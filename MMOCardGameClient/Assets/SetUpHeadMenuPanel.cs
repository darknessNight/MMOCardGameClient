﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SetUpHeadMenuPanel : MonoBehaviour 
{
	public GameObject Panel;
	public Button PlayButton;
	public Button StoreButton;
	public Button CollectionButton;
	public Button DeckButton;
	public Button PackagesButton;
	public Button LogoutButton;
	public Button SettingsButton;

	void Start () 
	{
		var activeSceneName = SceneManager.GetActiveScene().name;

		if (activeSceneName == "GameMenu")
			PlayButton.interactable = false;
		else if (activeSceneName == "Store")
			StoreButton.interactable = false;
		else if (activeSceneName == "CardCollection")
			CollectionButton.interactable = false;
		else if (activeSceneName == "DeckSelector" || activeSceneName == "DeckEditor")
			DeckButton.interactable = false;
		else if (activeSceneName == "Packages")
			PackagesButton.interactable = false;
		else if (activeSceneName == "LoginScreen")
			LogoutButton.interactable = false;

		if (activeSceneName == "MenuScreen")
			SettingsButton.interactable = true;
	}

	public void hideMenuPanel()
	{
		if(Panel.activeInHierarchy)
			Panel.SetActive(false);
	}

	public void displayMenuPanel()
	{
		if(!Panel.activeInHierarchy)
			Panel.SetActive(true);
	}
}
