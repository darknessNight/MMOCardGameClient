﻿public class Configuration
{
    public string authentionUrlTail;
    public string changeEmailTail;
    public string changePasswordTail;
    public string dataServerURL;
    public string language;
    public string resetPasswordTail;
    public string usersURLTail;
    public string cardsCollectionTail;
    public string gameTail;
    public string checkIfUserIsInGameTail;
    public string gameResultUrlTail;

    public string soloDoubleDuel;
    public string soloThreeDuel;
    public string soloFourDuel;
    public string teamDoubleDuel;

    public string battleServerIpAdress;
    public int nrOfBattleServerConnectionRetries;
}