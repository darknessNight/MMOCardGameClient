﻿using Newtonsoft.Json;

public static class JsonConfigSerialization
{
    public static Configuration getServerConfiguratoin(string serverConfigJsonString) => 
        JsonConvert.DeserializeObject<Configuration>(serverConfigJsonString);
}