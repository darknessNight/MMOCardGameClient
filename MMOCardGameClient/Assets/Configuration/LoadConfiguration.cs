﻿using System;
using System.IO;
using Localization;
using Newtonsoft.Json;
using UnityEngine;

public class LoadConfiguration : MonoBehaviour
{
    private string serverConfigFilePath;

    private void Start()
    {
        serverConfigFilePath = Application.dataPath + @"/StreamingAssets/serverConfig.json";
        loadConfiguration();
        loadLocalization();
    }

    private void loadConfiguration()
    {
        try
        {
            saveConfiguration();
        }
        catch (Exception e)
        {
            Debug.Log("Unable to load configuration - " + e.Message);
        }
    }

    private void saveConfiguration()
    {
        var configuration = JsonConvert.DeserializeObject<Configuration>(
            File.ReadAllText(serverConfigFilePath));
        UserInfo.Configuration = configuration;
        GameInfo.Configuration = configuration;
    }

    public void saveConfigurationToFile()
    {
        if (File.Exists(serverConfigFilePath))
            using (var writer = new StreamWriter(serverConfigFilePath))
            {
                writer.Write(buildServerConfigString());
            }
    }

    public string buildServerConfigString()
    {
        var configString =
            "{" +
            "\n\t\"dataServerURL\":\"" +
            UserInfo.DataServerURL +
            "\",\n\t\"authentionUrlTail\":\"/users/authenticated\"," +
            "\n\t\"usersURLTail\":\"/users\"," +
            "\n\t\"changeEmailTail\":\"/email\"," +
            "\n\t\"changePasswordTail\":\"/password\"," +
            "\n\t\"resetPasswordTail\":\"/password/reset\"" +
            "\n}";

        return configString;
    }

    private void loadLocalization()
    {
        var localizationLoader = new LocalizationLoader();
        UserInfo.LocalizationDictionary =
            localizationLoader.loadLocalizationFor(UserInfo.Language);
    }
}