﻿using System.Text.RegularExpressions;
using UnityEngine.UI;

public class FormDataValidator
{
    protected readonly Regex EmailRegex = new Regex(@"^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}$");
    protected readonly Regex LoginRegex = new Regex(@"^[A-Za-z0-9]{3,20}$");
    protected readonly Regex PasswordRegex = new Regex(@"^(?=\S+$).[A-Za-z0-9@#$%^&+=]{7,35}$");

    public bool validateEmail(string email)
    {
        return EmailRegex.IsMatch(email);
    }

    public bool validateEmail(InputField inputField)
    {
        return validateEmail(inputField.text);
    }

    public bool validateLogin(string login)
    {
        return LoginRegex.IsMatch(login);
    }

    public bool validateLogin(InputField inputField)
    {
        return validateLogin(inputField.text);
    }

    public bool validatePassword(string password)
    {
        return PasswordRegex.IsMatch(password);
    }

    public bool validatePassword(InputField inputField)
    {
        return validatePassword(inputField.text);
    }

    public bool checkIfFieldsAreEqual(string text1, string text2)
    {
        return text1.Equals(text2);
    }

    public bool checkIfFieldsAreEqual(InputField inputField1, InputField inputField2)
    {
        return checkIfFieldsAreEqual(inputField1.text, inputField2.text);
    }
}