﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Backend.GameInformation;
using ProtoBuffers;
using UnityEngine;

public class CallbackListeners
{
    private static CallbackListeners staticCallbackListeners;

    public delegate void PhaseEnded(GamePhase oldPhase, int oldPlayerId, GamePhase newPhase, int newPlayerId);
    public delegate void PlayerLifeChanged(int playerId, int newLife);
    public delegate void LinkPointsUpdated(int playerId, LinkPoints playerLinkPoints);
    public delegate void HandChanged(int playerId);
    public delegate void CardRevealed(Card revealedCard, int owner, CardPosition position);
    public delegate void ClientSevereException(string message, string title = null, bool shutDown = true);
    public delegate void PlayfieldInitialized();
    public delegate void CardStatisticsChanged(Card changedCard);
    public delegate void CardMoved(Card card, int oldOwner, CardPosition oldPosition, int newOnwer, CardPosition newPosition);
    public delegate void FinishedChoosing(int transactionId, int instanceId);
    public delegate void PickOptions(int transactionId, TargetList targetList, int instanceId);
    public delegate void ElementAddedToStack(int stackId, StackElement stackElement);
    public delegate void SimpleEvent();
    public delegate void FinishedChoosingTargets(int transactionId, int instanceId, TargetList targets);
    public delegate void AttackersSelected(Dictionary<int, List<TargetList>> args);
    public delegate void StackAction(int stackId, StackElement stackElement);
    public delegate void AttackExecuted(int attackerId, int defenderId, TargetType defenderType);
    public delegate void GameEnded(GameEndedResult result);
    public delegate void PlayerEndGame(int playerId);
    public delegate void CardCreatedFunc(Card card, int owner, CardPosition position);
    public delegate void TriggerActivatedFunc(int instanceId, TriggerType triggerType);
    public delegate void TriggerAddedToStack(int stackId, StackElement stackElement);

    public event PhaseEnded PhaseEndedEvent;
    public event PlayerLifeChanged PlayerLifeChangedEvent;
    public event LinkPointsUpdated LinkPointsUpdatedEvent;
    public event HandChanged HandChangedEvent;
    public event CardRevealed CardRevealedEvent;
    public event ClientSevereException SevereExceptionEvent;
    public event CardStatisticsChanged CardStatisticsChangedEvent;
    public event PlayfieldInitialized PlayfieldInitializedEvent;
    public event CardMoved CardMovedEvent;
    public event FinishedChoosing FinishedChoosingEvent;
    public event PickOptions PickOptionsEvent;
    public event ElementAddedToStack ElementAddedToStackEvent;
    public event SimpleEvent ReactToStackActionEvent;
    public event SimpleEvent WaitForReactionToStackActionEvent;
    public event StackAction StackActionCompletedEvent;
    public event StackAction StackActionRemovedEvent;
    public event FinishedChoosingTargets FinishedChoosingTargetsEvent;
    public event AttackersSelected AttackersSelectedEvent;
    public event AttackExecuted AttackExecutedEvent;
    public event GameEnded GameEndedEvent;
    public event PlayerEndGame PlayerWonEvent;
    public event PlayerEndGame PlayerLoseEvent;
    public event CardCreatedFunc CardCreatedEvent;
    public event TriggerAddedToStack TriggerAddedToStackEvent;
    public event TriggerActivatedFunc TriggerActivatedEvent;

    public static CallbackListeners get()
    {
        return staticCallbackListeners ?? (staticCallbackListeners = new CallbackListeners());
    }

    public void OnPlayfieldInitializedEvent()
    {
        PlayfieldInitializedEvent?.Invoke();
    }
    
    
    public void OnClientSevereExceptionEvent(string message, string title = null, bool shutDownAfterDisplaying = true)
    {
        Debug.LogError(title + message + (shutDownAfterDisplaying ? " Shutting down..." : string.Empty));
        SevereExceptionEvent?.Invoke(message, title, shutDownAfterDisplaying);
    }
    
    
    public void OnPhaseEndedEvent(GamePhase oldPhase, int oldPlayerId, GamePhase newPhase, int newPlayerId)
    {
        Debug.Log("Phase changed from " + oldPhase + " oldPlayerId: " + oldPlayerId + " to " + newPhase +
                  " newPlayerId: " + newPlayerId);
        PhaseEndedEvent?.Invoke(oldPhase, oldPlayerId, newPhase, newPlayerId);
    }

    public void OnPlayerLifeChangedEvent(int playerId, int newLife)
    {
        Debug.Log("Life of player with id: " + playerId + " changed to " + newLife);
        PlayerLifeChangedEvent?.Invoke(playerId, newLife);
    }
    
    public void OnLinkPointsUpdated(int playerId, LinkPoints playerLinkPoints)
    {
        var linkpoints = string.Concat("[P", playerLinkPoints.Psychos, "|R", playerLinkPoints.Raiders, "|S",
            playerLinkPoints.Scanners, "|W", playerLinkPoints.WhiteHats, "|N", playerLinkPoints.Neutral, "]");
        Debug.Log("LinkPoints of player with id: " + playerId + " changed to " + linkpoints);
        LinkPointsUpdatedEvent?.Invoke(playerId, playerLinkPoints);
    }

    public void OnHandChangeEvent(int playerId)
    {
        Debug.Log("OnHandChanged for playier with id: " + playerId + " " + HandChangedEvent);
        HandChangedEvent?.Invoke(playerId);
    }

    public void OnCardStatisticsChangedEvent(Card changedCard)
    {
        Debug.Log("Card with id: " + changedCard.CardId + " nad instanceId: " + changedCard.InstanceId +
                  " has been changed");
        CardStatisticsChangedEvent?.Invoke(changedCard);
    }

    public void OnCardRevealed(Card revealedCard, int cardRevealedOwner, CardPosition cardRevealedPosition)
    {
        Debug.Log("Card with id: " + revealedCard.CardId + " nad instanceId: " + revealedCard.InstanceId + 
                  " has been revealed");
        CardRevealedEvent?.Invoke(revealedCard, cardRevealedOwner, cardRevealedPosition);
    }

    public void OnCardMoved(Card card, int oldOwner, CardPosition oldPosition, int newOnwer, CardPosition newPosition)
    {
        CardMovedEvent?.Invoke(card, oldOwner, oldPosition, newOnwer, newPosition);
    }

    public void OnFinishedChoosing(int transactionId, int instanceId)
    {
        FinishedChoosingEvent?.Invoke(transactionId, instanceId);
    }

    public void OnPickOptions(int transactionId, TargetList targetList, int instanceId)
    {
        PickOptionsEvent?.Invoke(transactionId, targetList, instanceId);
    }

    public void OnElementAddedToStack(int stackId, StackElement stackElement)
    {
        ElementAddedToStackEvent?.Invoke(stackId, stackElement);
    }

    public void OnReactToStackAction()
    {
        ReactToStackActionEvent?.Invoke();
    }

    public void OnWaitForReactionToStackAction()
    {
        WaitForReactionToStackActionEvent?.Invoke();
    }

    public void OnStackActionCompleted(int stackId, StackElement stackElement)
    {
        StackActionCompletedEvent?.Invoke(stackId, stackElement);
    }

    public void OnStackActionRemoved(int stackId, StackElement stackElement)
    {
        StackActionRemovedEvent?.Invoke(stackId, stackElement);
    }

    public void OnFinishedChoosingTargets(int transactionId, int instanceId, TargetList choosenTargets)
    {
        FinishedChoosingTargetsEvent?.Invoke(transactionId, instanceId, choosenTargets);
    }

    public void OnAttackersSelected(Dictionary<int, List<TargetList>> attackersDefenders)
    {
        AttackersSelectedEvent?.Invoke(attackersDefenders);
    }

    public void OnAttackExecuted(int attackerId, int defenderId, TargetType defenderType)
    {
        AttackExecutedEvent?.Invoke(attackerId, defenderId, defenderType);
    }

    public void OnGameEnded(GameEndedResult result)
    {
        GameEndedEvent?.Invoke(result);
    }

    public void OnPlayerWon(int playerId)
    {
        PlayerWonEvent?.Invoke(playerId);
    }

    public void OnPlayerLose(int playerId)
    {
        PlayerLoseEvent?.Invoke(playerId);
    }

    public void OnCardCreated(Card createdCard, int owner, CardPosition cardPosition)
    {
        CardCreatedEvent?.Invoke(createdCard, owner, cardPosition);
    }

    internal void OnTriggerAddedToStack(int stackId, StackElement stackElement)
    {
        TriggerAddedToStackEvent?.Invoke(stackId, stackElement);
    }

    internal void OnTriggerActivated(int instanceId, TriggerType triggerType)
    {
        TriggerActivatedEvent?.Invoke(instanceId, triggerType);
    }

    public static void ResetEventListeners()
    {
        staticCallbackListeners = null;
    }
}