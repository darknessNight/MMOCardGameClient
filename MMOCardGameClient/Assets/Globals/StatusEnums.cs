﻿public enum LoginStatus
{
    SIGNED_OUT,
    SIGNED_IN,
    CONNECTION_LOST
}