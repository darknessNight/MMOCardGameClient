﻿using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SimpleHorizontalLayout : MonoBehaviour
{
	public Margin Margin = new Margin(){Top = 0, Bottom = 0, Left = 0, Right = 0};
	public HorizontalAlignment Alignment = HorizontalAlignment.CENTER;
	public float Spacing = 3;
	public Vector2 AspectRatio;
	public bool KeepAspectRatio;
	
	private RectTransform itemZone;

	private ZoneLayoutProperties properties;
	private ZoneLayoutProperties previousProperties;
	
	private HorizontalAlignment previousOriantation;
	private Vector2 previousAspectRatio;
	private bool previousKeepAspectRatio;
	
	private HorizontalItemPositioning positioning;

	private HorizontalPositioningBuilder _positioningBuilder;

	private List<RectTransform> childrensRectTransform;

	private bool forceRefresh = false;

	void Start()
	{
		itemZone = gameObject.GetComponent<RectTransform>();
		childrensRectTransform = new List<RectTransform>();
		properties = new ZoneLayoutProperties();
		updateProperties();
		previousProperties = new ZoneLayoutProperties();
		setPreviousValues();
		_positioningBuilder = new HorizontalPositioningBuilder();
	}
	
	private void updateProperties()
	{
		properties.Margin = Margin;
		properties.NumberOfItems = itemZone.childCount;
		properties.ZoneSize = new Vector2(itemZone.rect.width, itemZone.rect.height);
		properties.Spacing = Spacing;
	}
	

	void Update()
	{
		updateProperties();
		if (noChangesOccurred() && !forceRefresh) return;
		getListOfChildrensRectTransform();
		repositionItems();
		setPreviousValues();
		forceRefresh = false;
	}

	public void ForceRefresh()
	{
		forceRefresh = true;
	}

	private bool noChangesOccurred()
	{
		return previousProperties.Equals(properties) &&
		       Alignment == previousOriantation &&
		       AspectRatio == previousAspectRatio &&
		       KeepAspectRatio == previousKeepAspectRatio;
	}

	private void getListOfChildrensRectTransform()
	{
		childrensRectTransform.Clear();
		foreach (RectTransform item in itemZone)
			childrensRectTransform.Add(item);
	}

	private void repositionItems()
	{
		if (_positioningBuilder == null && Application.isEditor)
			_positioningBuilder = new HorizontalPositioningBuilder();
		(_positioningBuilder.SetItemZone(itemZone)
			.SetMargin(Margin)
			.SetAspectRatio(AspectRatio)
			.SetOrientation(Alignment)
			.SetSpacing(Spacing)
			.KeepAspectRatio(KeepAspectRatio)
			.Build() as HorizontalItemPositioning)
			.setPositions();
	}

	private void setPreviousValues()
	{
		previousProperties.replaceValues(properties);
		previousAspectRatio = AspectRatio;
		previousOriantation = Alignment;
		previousKeepAspectRatio = KeepAspectRatio;
	}
}
