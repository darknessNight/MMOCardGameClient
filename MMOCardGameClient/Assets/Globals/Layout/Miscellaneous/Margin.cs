﻿[System.Serializable]
public struct Margin
{
	public float Top;
	public float Bottom;
	public float Left;
	public float Right;

	public Margin(Margin original)
	{
		Top = original.Top;
		Bottom = original.Bottom;
		Left = original.Left;
		Right = original.Right;
	}

	public override bool Equals(object obj)
	{
		if (!(obj?.GetType() == typeof(Margin))) return false;
		var o = (Margin)obj;
		return this.Bottom.Equals(o.Bottom) &&
		       this.Left.Equals(o.Left) &&
		       this.Right.Equals(o.Right) &&
		       this.Top.Equals(o.Top);
	}

	public override int GetHashCode()
	{
		unchecked
		{
			var hashCode = Top.GetHashCode();
			hashCode = (hashCode * 397) ^ Bottom.GetHashCode();
			hashCode = (hashCode * 397) ^ Left.GetHashCode();
			hashCode = (hashCode * 397) ^ Right.GetHashCode();
			return hashCode;
		}
	}
}
