﻿using UnityEngine;

public struct ZoneLayoutProperties
{
	public Vector2 ZoneSize;
	public int NumberOfItems;
	public Margin Margin;
	public float Spacing;

	public void replaceValues(ZoneLayoutProperties original)
	{
		ZoneSize = original.ZoneSize;
		NumberOfItems = original.NumberOfItems;
		Margin = original.Margin;
		Spacing = original.Spacing;
	}

	public override bool Equals(object obj)
	{
		if (!(obj?.GetType() == typeof(ZoneLayoutProperties))) return false;
		var o = (ZoneLayoutProperties) obj;
		return ZoneSize.Equals(o.ZoneSize) &&
		       NumberOfItems.Equals(o.NumberOfItems) &&
		       Margin.Equals(o.Margin) &&
		       Spacing.Equals(o.Spacing);
	}

	public override int GetHashCode()
	{
		unchecked
		{
			var hashCode = ZoneSize.GetHashCode();
			hashCode = (hashCode * 397) ^ NumberOfItems.GetHashCode();
			hashCode = (hashCode * 397) ^ Margin.GetHashCode();
			hashCode = (hashCode * 397) ^ Spacing.GetHashCode();
			return hashCode;
		}
	}
}
