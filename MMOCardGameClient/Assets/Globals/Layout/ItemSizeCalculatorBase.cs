﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemSizeCalculatorBase
{
	protected Vector2 itemSize;
	
	public Vector2 ItemSize 
	{ 
		get{setItemSize();
			return itemSize;
		}
		protected set => itemSize = value;
	}
	
	protected Vector2 zoneSize;
	protected Margin margin;
	protected float spacing;
	protected int itemCount;
	protected Vector2 aspectRatio;

	protected Vector2 usableZoneSize;

	protected ItemSizeCalculatorBase(Vector2 zoneSize, Margin margin, float spacing, int itemCount, Vector2 aspectRatio)
	{
		this.zoneSize = zoneSize;
		this.margin = margin;
		this.spacing = spacing;
		this.itemCount = itemCount;
		this.aspectRatio = checkAndCorrectAspectRatio(aspectRatio);
	}
	
	private Vector2 checkAndCorrectAspectRatio(Vector2 aspectRatio)
	{
		var newAspectRatio = aspectRatio;
		if(aspectRatio.x.Equals(0) || aspectRatio.y.Equals(0))
			newAspectRatio = Vector2.one;
		return newAspectRatio;
	}

	protected abstract void setItemSize();

	protected void calculateUsableZoneSize()
	{
		usableZoneSize = new Vector2(
			zoneSize.x - (margin.Left + margin.Right),
			zoneSize.y - (margin.Top + margin.Bottom));
	}
}
