﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalCardHandPositioning : Positioning
{
    private HorizontalAlignment _alignment;
    private float preferredSpacing;
    
    protected float averageItemWidth;
    
    public HorizontalCardHandPositioning(RectTransform zone, Margin margin,
        HorizontalAlignment alignment,  float preferredSpacingPercent = 0)
    : base(zone, margin)
    {
        this._alignment = alignment;

        averageItemWidth = averageItemSize.x;
        preferredSpacing = (preferredSpacingPercent / 100) * averageItemWidth;
    }

    public List<float> getPositions()
    {
        var spacing = getSpacing();
        var beginX = getBeginningXCoordinate(spacing);
        var positionList = new List<float>(itemZone.childCount);
        
        for (var i = 0; i < itemZone.childCount; i++)
            positionList.Add(beginX + spacing * i + averageItemWidth / 2);

        return positionList;
    }

    private float getBeginningXCoordinate(float spacing)
    {
        if (_alignment == HorizontalAlignment.LEFT)
            return getLeftOrientedBeginningXCoordinate();
        if (_alignment == HorizontalAlignment.CENTER)
            return getCenterOrientedBeginningXCoordinate(spacing);
        throw new Exception("HorizontalCardHandPositioning: Unsupported orientation!");
    }

    private float getLeftOrientedBeginningXCoordinate()
    {
        var xCoordinate = zoneSize.x / 2 + averageItemWidth / 2;
        xCoordinate += alignLeft ? -margin.Right - averageItemWidth / 2 : 
            margin.Left + averageItemWidth / 2;
        xCoordinate *= alignLeft ? -1 : 1;
        return xCoordinate;
    }

    private float getCenterOrientedBeginningXCoordinate(float spacing)
    {
        var xCoordinate = ((itemZone.childCount - 1) * spacing) / 2 + averageItemWidth / 2;
        xCoordinate *= alignLeft ? -1 : 1;
        return xCoordinate;
    }

    private bool alignLeft => overlapping == Overlapping.FROM_LEFT;

    private float getSpacing()
    {
        var spacing = (zoneSize.x - (margin.Left + margin.Right + averageItemWidth)) / itemZone.childCount;
        return spacing < preferredSpacing || preferredSpacing.Equals(0.0f) ? spacing : preferredSpacing;
    }
}

