﻿using System;
using UnityEngine;

public class StatusBarLengthCalculator
{
    private readonly RectTransform _placeholder;
    private readonly Margin _margin;
    private readonly float _percentage;
    private HorizontalAlignment _alignment;

    private float horizontalMargin => _margin.Left + _margin.Right;
    private float verticarMargin => _margin.Top + _margin.Bottom;
    private float usableZoneWidth => _placeholder.sizeDelta.x - horizontalMargin;
    private float usableZoneHeight => _placeholder.sizeDelta.y - verticarMargin;

    private float centeredVerticalPosition => _placeholder.sizeDelta.y / 2 - _margin.Top - usableZoneHeight / 2;
    private float rightAlignedNewXCoordinate => (usableZoneWidth - newBarWidth) / 2;
    private float newBarWidth => usableZoneWidth * (_percentage / 100);
    
    public StatusBarLengthCalculator(RectTransform placeholder, Margin margin,
        float percentage, HorizontalAlignment alignment)
    {
        _placeholder = placeholder;
        _margin = margin;
        _percentage = percentage;
        _alignment = alignment;
    }

    public Vector2 getBarSize()
    {
        return new Vector2(newBarWidth, usableZoneHeight);
    }

    public Vector2 getPosition()
    {
        return calculatePosition() + _placeholder.position;
    }

    private Vector3 calculatePosition()
    {
        switch (_alignment)
        {
            case HorizontalAlignment.CENTER:
                return new Vector3(0, centeredVerticalPosition, 0);
            case HorizontalAlignment.LEFT:
                return new Vector3(-rightAlignedNewXCoordinate, centeredVerticalPosition, 0);
            case HorizontalAlignment.RIGHT:
                return new Vector3(rightAlignedNewXCoordinate, centeredVerticalPosition, 0);
            default:
                Debug.LogWarning("Unsupported alignment - " + _alignment.ToString() + ". Aligning to center.");
                _alignment = HorizontalAlignment.CENTER;
                return new Vector3(0, centeredVerticalPosition, 0);
        }
    }
}
