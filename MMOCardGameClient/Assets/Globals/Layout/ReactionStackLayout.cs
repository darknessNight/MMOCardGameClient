﻿using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ReactionStackLayout : MonoBehaviour
{
    public Margin Margin = new Margin(){Top = 0, Bottom = 0, Left = 10, Right = 10};
    public HorizontalAlignment Alignment = HorizontalAlignment.CENTER;
    public float PreferredSpacing = 10;
    public float MaxAdditionalHeight = 30;
    
    private RectTransform cardZone;

    private ZoneLayoutProperties properties;
    private ZoneLayoutProperties previousProperties;

    private HorizontalAlignment _previousAlignment;
    
    private HorizontalCardHandPositioning positioning;
    private Vector2 cardSize;
    
    private List<RectTransform> cardsRectTransformList;

    public HorizontalPositioningBuilder PositioningBuilder;

    
    void Start()
    {
        cardZone = gameObject.GetComponent<RectTransform>();
        cardsRectTransformList = new List<RectTransform>();
        properties = new ZoneLayoutProperties();
        updateProperties();
        previousProperties = new ZoneLayoutProperties();
        previousProperties.replaceValues(properties);
        
        PositioningBuilder = new HorizontalPositioningBuilder(); //Can be replaced after start
    }
    
    private void updateProperties()
    {
        properties.Margin = Margin;
        properties.NumberOfItems = cardZone.childCount;
        properties.Spacing = PreferredSpacing;
        properties.ZoneSize = new Vector2(cardZone.rect.width, cardZone.rect.height);
    }
    

    void Update()
    {
        updateProperties();
        if (noChangesOccurred()) return;
        getCards();
        repositionCards();
        setPreviousValues();
    }
    
    private bool noChangesOccurred()
    {
        return previousProperties.Equals(properties) &&
               Alignment == _previousAlignment;
    }
    
    private void getCards()
    {
        cardsRectTransformList.Clear();
        foreach (RectTransform card in cardZone)
            cardsRectTransformList.Add(card);
    }

    private void repositionCards()
    {
        if (cardZone.rect.width == 0 || cardZone.rect.height == 0) return;
        
        cardSize = new Vector2(cardZone.rect.width, cardZone.rect.height);
        setPosition();
    }

    private void setPosition()
    {
        float spacing = PreferredSpacing;
        if (properties.NumberOfItems * PreferredSpacing > MaxAdditionalHeight)
            spacing = MaxAdditionalHeight / properties.NumberOfItems;
        for (var i = 0; i < properties.NumberOfItems; i++)
        {
            var position = cardZone.position + new Vector3(cardZone.rect.x / 2, 0) +
                           new Vector3(0, -i * spacing);
            var child = cardZone.GetChild(i);
            var movableChild=child.GetComponent<IMovable>();
            if (movableChild != null)
            {
                movableChild.moveToPosition(position.x, position.y);
                movableChild.resizeToSize(cardSize.x, cardSize.y);
            }
            else
            {
                child.transform.position = position;
                cardsRectTransformList[i].sizeDelta = cardSize;
            }
        }
    }
    
    private void setPreviousValues()
    {
        previousProperties.replaceValues(properties);
        _previousAlignment = Alignment;
    }
}