﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FitToParentLayout : MonoBehaviour
{
	private RectTransform rectTransform;

	void Start ()
	{
		rectTransform = gameObject.GetComponent<RectTransform>();
	}
	
	void Update ()
	{
		var parentSize = transform.parent.GetComponent<RectTransform>().sizeDelta;
		rectTransform.sizeDelta = parentSize;
		rectTransform.position = transform.parent.position;
	}
}
