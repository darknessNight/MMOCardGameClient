﻿using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CardStackLayout : MonoBehaviour
{
    public Margin Margin = new Margin(){Top = 0, Bottom = 0, Left = 0, Right = 0};
    public HorizontalAlignment Alignment = HorizontalAlignment.CENTER;
    public float PreferredSpacing = 3;
    public float VisibleCards = 4;
    
    private RectTransform cardZone;

    private ZoneLayoutProperties properties;
    private ZoneLayoutProperties previousProperties;

    private HorizontalAlignment _previousAlignment;
    
    private HorizontalCardHandPositioning positioning;
    private Vector2 cardSize;
    
    private List<RectTransform> cardsRectTransformList;

    
    void Start()
    {
        cardZone = gameObject.GetComponent<RectTransform>();
        cardsRectTransformList = new List<RectTransform>();
        properties = new ZoneLayoutProperties();
        updateProperties();
        previousProperties = new ZoneLayoutProperties();
        previousProperties.replaceValues(properties);
    }
    
    private void updateProperties()
    {
        properties.Margin = Margin;
        properties.NumberOfItems = cardZone.childCount;
        properties.Spacing = PreferredSpacing;
        properties.ZoneSize = new Vector2(cardZone.rect.width, cardZone.rect.height);
    }
    

    void Update()
    {
        updateProperties();
        if (noChangesOccurred()) return;
        getCards();
        repositionCards();
        setPreviousValues();
    }
    
    private bool noChangesOccurred()
    {
        return previousProperties.Equals(properties) &&
               Alignment == _previousAlignment;
    }
    
    private void getCards()
    {
        cardsRectTransformList.Clear();
        foreach (RectTransform card in cardZone)
            cardsRectTransformList.Add(card);
    }

    private void repositionCards()
    {
        if (cardZone.rect.width == 0 || cardZone.rect.height == 0) return;
        
        cardSize = new Vector2(cardZone.rect.width, cardZone.rect.height-VisibleCards*PreferredSpacing);
        setPosition();
    }

    private void setPosition()
    {
        for (var i = properties.NumberOfItems-1; i >= 0 ; i--)
        {
            float yPos = 0;
            if (i >= properties.NumberOfItems-VisibleCards)
                yPos = (properties.NumberOfItems-i-1) * PreferredSpacing;

            var newPosition = cardZone.position + new Vector3(0, -yPos);
            
            var child=cardZone.GetChild(i);
            var movableChild = child.GetComponent<IMovable>();
            if (movableChild != null)
            {
                movableChild.moveToPosition(newPosition.x, newPosition.y);
                movableChild.resizeToSize(cardSize.x, cardSize.y);
            }
            else
            {
                child.transform.position = newPosition;
                cardsRectTransformList[i].sizeDelta = cardSize;
            }
        }
    }
    
    private void setPreviousValues()
    {
        previousProperties.replaceValues(properties);
        _previousAlignment = Alignment;
    }
}