﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class HorizontalPositioningBuilder
{
    private Positioning positionCalculator;

    public HorizontalPositioningBuilder()
    {
        _itemZoneInitialized = false;
        _aspectRatioInitialized = false;
        _spacingInitialized = false;

        _margin = new Margin() {Bottom = 0, Left = 0, Right = 0, Top = 0};
        _alignment = HorizontalAlignment.CENTER;
        _keepAspectRatio = true;
    }

    private RectTransform _itemZone;
    private Margin _margin;
    private Vector2 _aspectRatio;
    private HorizontalAlignment _alignment;
    private float _spacing;
    private bool _keepAspectRatio;

    private bool _itemZoneInitialized;
    private bool _aspectRatioInitialized;
    private bool _spacingInitialized;

    public HorizontalPositioningBuilder SetItemZone(RectTransform itemZone)
    {
        _itemZone = itemZone;
        _itemZoneInitialized = true;
        return this;
    }

    public HorizontalPositioningBuilder SetMargin(Margin margin)
    {
        _margin = margin;
        return this;
    }

    public HorizontalPositioningBuilder SetAspectRatio(Vector2 aspectRatio)
    {
        _aspectRatio = aspectRatio;
        _aspectRatioInitialized = true;
        return this;
    }

    public HorizontalPositioningBuilder SetAspectRatio(float w, float h)
    {
        _aspectRatio = new Vector2(w, h);
        _aspectRatioInitialized = true;
        return this;
    }

    public HorizontalPositioningBuilder SetOrientation(HorizontalAlignment alignment)
    {
        _alignment = alignment;
        return this;
    }

    public HorizontalPositioningBuilder SetSpacing(float spacing)
    {
        _spacing = spacing;
        _spacingInitialized = true;
        return this;
    }

    public HorizontalPositioningBuilder KeepAspectRatio(bool keepAspectRatio)
    {
        _keepAspectRatio = keepAspectRatio;
        return this;
    }

    public Positioning Build()
    {
        if(canBuildCardPostioner)
            return new HorizontalCardHandPositioning(_itemZone, _margin, _alignment, _spacing);
        if (canBuildUniversalItemPostioner)
            return new HorizontalItemPositioning(_itemZone, _margin, _aspectRatio, _alignment,
                _spacing, _keepAspectRatio);
        throw new NotSupportedException(getError());
    }

    private bool canBuildUniversalItemPostioner =>
        _itemZoneInitialized == _spacingInitialized == _aspectRatioInitialized;

    private bool canBuildCardPostioner =>
        _itemZoneInitialized == _spacingInitialized && _aspectRatioInitialized == false;

    private string getError()
    {
        var builder = new StringBuilder();
        builder.Append("Unable to build positioner - No ");
        if (!_itemZoneInitialized) builder.Append("item zone");
        if (!_spacingInitialized && _itemZoneInitialized) builder.Append(" and ");
        if (!_spacingInitialized) builder.Append("spacing");
        builder.Append(" provided.");
        return builder.ToString();
    }

}
