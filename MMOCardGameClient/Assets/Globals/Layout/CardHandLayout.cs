﻿using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CardHandLayout : MonoBehaviour
{
    public Margin Margin = new Margin(){Top = 0, Bottom = 0, Left = 10, Right = 10};
    public HorizontalAlignment Alignment = HorizontalAlignment.CENTER;
    public float PreferredSpacing = 0;
    
    private RectTransform cardZone;

    private ZoneLayoutProperties properties;
    private ZoneLayoutProperties previousProperties;

    private HorizontalAlignment _previousAlignment;
    
    private HorizontalCardHandPositioning positioning;
    private Vector2 cardSize;
    
    private List<RectTransform> cardsRectTransformList;

    public HorizontalPositioningBuilder PositioningBuilder;

    public bool ElementsAreDragable = false;

    
    void Start()
    {
        cardZone = gameObject.GetComponent<RectTransform>();
        cardsRectTransformList = new List<RectTransform>();
        properties = new ZoneLayoutProperties();
        updateProperties();
        previousProperties = new ZoneLayoutProperties();
        previousProperties.replaceValues(properties);
        
        PositioningBuilder = new HorizontalPositioningBuilder(); //Can be replaced after start
    }
    
    private void updateProperties()
    {
        properties.Margin = Margin;
        properties.NumberOfItems = cardZone.childCount;
        properties.Spacing = PreferredSpacing;
        properties.ZoneSize = new Vector2(cardZone.rect.width, cardZone.rect.height);
    }
    

    void Update()
    {
        updateProperties();
        if (noChangesOccurred()) return;
        getCardsInPlayersHand();
        repositionCards();
        setPreviousValues();
    }
    
    private bool noChangesOccurred()
    {
        return previousProperties.Equals(properties) &&
               Alignment == _previousAlignment;
    }
    
    private void getCardsInPlayersHand()
    {
        cardsRectTransformList.Clear();
        foreach (RectTransform card in cardZone)
            cardsRectTransformList.Add(card);
    }

    private void repositionCards()
    {
        if (cardZone.rect.width == 0 || cardZone.rect.height == 0) return;
        positioning = PositioningBuilder?.SetItemZone(cardZone)
            .SetMargin(Margin)
            .SetOrientation(Alignment)
            .SetSpacing(PreferredSpacing)
            .Build() as HorizontalCardHandPositioning; 
        
        resizeCards();
        setHorizontalPosition();
    }
    
    private void resizeCards()
    {
        if (Application.isEditor)
            positioning = new HorizontalCardHandPositioning(cardZone, Margin, Alignment, PreferredSpacing);
        
        cardSize = new CardHandSizeCalculator(cardZone, Margin, PreferredSpacing, cardsRectTransformList.Count, 
            positioning.averageItemSize).ItemSize;
    }

    private void setHorizontalPosition()
    {
        var xCoodinations = positioning.getPositions();
        for (var i = 0; i < properties.NumberOfItems; i++)
        {

            var newPosition = cardZone.position + new Vector3(xCoodinations[i], 0, 0);
            
            var child=cardZone.GetChild(i);
            var movableChild = child.GetComponent<IMovable>();
            if (movableChild != null)
            {
                movableChild.moveToPosition(newPosition.x, newPosition.y);
                movableChild.resizeToSize(cardSize.x, cardSize.y);
                movableChild.SetDragable(ElementsAreDragable);
            }
            else
            {
                child.transform.position = newPosition;
                cardsRectTransformList[i].sizeDelta = cardSize;
            }
        }
            
    }
    
    private void setPreviousValues()
    {
        previousProperties.replaceValues(properties);
        _previousAlignment = Alignment;
    }
}
