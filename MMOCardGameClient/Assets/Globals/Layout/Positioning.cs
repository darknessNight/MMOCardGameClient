﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Positioning
{
    protected RectTransform itemZone;
    protected Vector2 zoneSize;
    public Vector2 averageItemSize { get; }

    protected Margin margin;
    protected Overlapping overlapping;

    public Positioning(RectTransform zone, Margin margin)
    {
        this.margin = margin;
        itemZone = zone;
        zoneSize = new Vector2(zone.rect.width, zone.rect.height);

        averageItemSize = GetAverageSizeOfChildren();
    }
    
    private Vector2 GetAverageSizeOfChildren()
    {
        var avgSize = new Vector2();
        foreach (RectTransform card in itemZone)
        {
            var cardSize = card.sizeDelta;
            avgSize.x += cardSize.x;
            avgSize.y += cardSize.y;
        }

        avgSize.x /= itemZone.childCount;
        avgSize.y /= itemZone.childCount;

        return avgSize;
    }
}
