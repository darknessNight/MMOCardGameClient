﻿using UnityEngine;

[ExecuteInEditMode]
public class StatusBarLayout : MonoBehaviour
{
	[SerializeField] public float VisiblePercentage = 100;
	
	[SerializeField] private RectTransform Placeholder;
	[SerializeField] private RectTransform Bar;
	[SerializeField] private Margin Margin = new Margin(){Top = 0, Bottom = 0, Left = 0, Right = 0};
	[SerializeField] private HorizontalAlignment BarAlignment = HorizontalAlignment.CENTER;

	private Vector2 currentPlaceholderSize;
	private float previosPercentage;
	private Margin previousMargin;
	private HorizontalAlignment previousAlignment;

	private StatusBarLengthCalculatorBuilder lengthCalculatorBuilder;

	private bool valuesChanged => currentPlaceholderSize != Placeholder.sizeDelta ||
	                              !VisiblePercentage.Equals(previosPercentage) ||
	                              !Margin.Equals(previousMargin) ||
	                              BarAlignment != previousAlignment;

	private bool necessaryFieldsInitialized => Placeholder != null && Bar != null;
	private bool percentageValueCorrect => VisiblePercentage >= 0 && VisiblePercentage != float.NaN &&
	                                       VisiblePercentage <= 100;

	private bool fieldsValid => necessaryFieldsInitialized && percentageValueCorrect;
	
	void Start ()
	{
		setPreviousValues();
		lengthCalculatorBuilder = new StatusBarLengthCalculatorBuilder();
		lengthCalculatorBuilder.SetPlaceholderRectTransform(Placeholder);
		lengthCalculatorBuilder.SetBarRectTransform();
		lengthCalculatorBuilder.SetVisiblePrecentage(VisiblePercentage);
		lengthCalculatorBuilder.SetAlignment(BarAlignment);
		lengthCalculatorBuilder.SetMargin(Margin);
	}
	
	void Update ()
	{
		if (!valuesChanged || !fieldsValid) return;
		var barLengtCalculator = rebuildCalculator();
		Bar.sizeDelta = barLengtCalculator.getBarSize();
		Bar.transform.position = barLengtCalculator.getPosition();
		setPreviousValues();
	}

	private StatusBarLengthCalculator rebuildCalculator()
	{
		initializeBarLengthCalculatorForEditMode();
		lengthCalculatorBuilder.SetPlaceholderRectTransform(Placeholder);
		lengthCalculatorBuilder.SetBarRectTransform();
		lengthCalculatorBuilder.SetVisiblePrecentage(VisiblePercentage);
		lengthCalculatorBuilder.SetAlignment(BarAlignment);
		lengthCalculatorBuilder.SetMargin(Margin);
		return lengthCalculatorBuilder.Build();
	}

	private void initializeBarLengthCalculatorForEditMode()
	{
		if (!Application.isEditor && lengthCalculatorBuilder != null) return;
		lengthCalculatorBuilder = new StatusBarLengthCalculatorBuilder();
	}

	private void setPreviousValues()
	{
		previosPercentage = VisiblePercentage;
		currentPlaceholderSize = Placeholder.sizeDelta;
		previousMargin = new Margin(Margin);
		previousAlignment = BarAlignment;
	}
}
