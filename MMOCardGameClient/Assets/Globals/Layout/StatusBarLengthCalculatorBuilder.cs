﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class StatusBarLengthCalculatorBuilder
{
    private StatusBarLengthCalculator _builtCalculator;

    private RectTransform _placeholder;
    private Margin _margin;
    private float _percentage;
    private HorizontalAlignment _alignment;

    private bool _barInitialized;
    private bool _placeholderInitialized;
    private bool _percentageInitialized;

    private bool _changesMade;
    private bool _firstInitializationDone;

    private bool allNecessaryVariablesProvided => _barInitialized && _placeholderInitialized && _percentageInitialized;
    
    public StatusBarLengthCalculatorBuilder()
    {
        _builtCalculator = null;
        _margin = new Margin(){Top = 0, Bottom = 0, Left = 0, Right = 0};
        _alignment = HorizontalAlignment.CENTER;
        
        _changesMade = false;
        _firstInitializationDone = false;
    }

    public StatusBarLengthCalculatorBuilder SetBarRectTransform()
    {
        _barInitialized = true;
        _changesMade = true;
        return this;
    }

    public StatusBarLengthCalculatorBuilder SetPlaceholderRectTransform(RectTransform placeholder)
    {
        _placeholder = placeholder;
        _placeholderInitialized = true;
        _changesMade = true;
        return this;
    }

    public StatusBarLengthCalculatorBuilder SetVisiblePrecentage(float percentage)
    {
        _percentage = percentage;
        _percentageInitialized = true;
        _changesMade = true;
        return this;
    }

    public StatusBarLengthCalculatorBuilder SetMargin(Margin margin)
    {
        _margin = margin;
        _changesMade = true;
        return this;
    }

    public StatusBarLengthCalculatorBuilder SetAlignment(HorizontalAlignment alignment)
    {
        _alignment = alignment;
        _changesMade = true;
        return this;
    }

    public StatusBarLengthCalculator Build(bool forceRebuild = false)
    {
        _firstInitializationDone = allNecessaryVariablesProvided || _firstInitializationDone;
        if(!allNecessaryVariablesProvided && !_firstInitializationDone) throw new Exception(getError());
        if (!_changesMade ^ forceRebuild) return _builtCalculator;
        _changesMade = false;
        _builtCalculator = new StatusBarLengthCalculator(_placeholder, _margin, _percentage, _alignment);
        return _builtCalculator;
    }

    private string getError()
    {
        var builder = new StringBuilder();
        builder.Append("StatusBarLengthCalculatorBuilder: Not all necessary variables have been provided - missing ");
        if (!_barInitialized) builder.Append("\n\tStatusbar");
        if (!_placeholderInitialized) builder.Append("\n\tPlaceholder");
        if (!_percentageInitialized) builder.Append("Percentage");
        return builder.ToString();
    }

}
