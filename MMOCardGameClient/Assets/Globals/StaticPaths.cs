﻿using UnityEngine;

public static class StaticPaths
{
    public static string CardsJsonDirectory { get; } = Application.dataPath + "/StreamingAssets/CardsJson/cards.json";
    public static string LogsDirectory { get; } = Application.dataPath + "/StreamingAssets/Logs/";
    public static string NamedAbilitiesJsonDirectory { get; } = Application.dataPath + "/StreamingAssets/CardsJson/effects.json";
    public static string ErrorLogLocation { get; } = LogsDirectory + "errors.log";
    public static string MessageLogLocation { get; } = LogsDirectory + "messages.log";
}