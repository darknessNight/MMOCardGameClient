﻿using System;
using System.Collections.Generic;
using System.Text;
using Backend.GameInformation;
using Backend.GameInformation.Enums;
using ProtoBuffers;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class CardDisplay : MonoBehaviour, IPlayfieldItemConfiguration, IAmountChanger
{
    [SerializeField] private CardCostDisplay costDisplay;
    [SerializeField] private InactiveObjectHiding objectHiding;
    [SerializeField] private CardClassAndTypeDisplay classAndType;
    [SerializeField] private SimpleCardTextObjectsDisplay textObjects;

    private IPlayfieldItemConfiguration[] itemConfigurators;

    public GameObject PartialCard;
    public GameObject FullCard;

    public Image artworkImage;
    public Image BattlefieldArtworkImage;

    public int InstanceId { get; private set; }
    public int? CardId { get; private set; }

    public GameObject visibilityLayer;

    
    private Card changedCard;

    public CardDisplay()
    {
        CallbackListeners.get().CardRevealedEvent += OnCardRevealedEvent;
        
    }
    
    private void Start()
    {
        itemConfigurators = new IPlayfieldItemConfiguration[] {costDisplay, objectHiding, classAndType, textObjects};  
        
        var callbackListeners = CallbackListeners.get();
        callbackListeners.CardStatisticsChangedEvent += StatisticsChanged;
        callbackListeners.CardMovedEvent += CallbackListenersOnCardMovedEvent;
    }

    private void Update()
    {
        if (changedCard != null)
        {
            var card = changedCard;
            changedCard = null;
            var infoCard = CardCreator.createNewCard(card);
            CardId = card.CardId;
            setUpVisibleCardObjects(infoCard);
            Destroy(infoCard);
        }
    }

    public ScriptableObject Original { get; private set; }

    public void setUp(ScriptableObject item)
    {
        var card = item as CardScriptable;
        if (card == null)
            throw new Exception("CardDisplay: Unable to configure card - the given Scriptable object is not a Card!");
        
        Original = card;
        InstanceId = card.InstanceId;
        CardId = card.CardId;
        try
        {
            setUpVisibleCardObjects(card);
        }
        catch (Exception e)
        {
            throw new Exception("CardDisplay: Error in item configuration - " + e.Message);
        }
    }

    public void ChangeToFullCard()
    {
        if (FullCard != null)
            FullCard.SetActive(true);
        if (PartialCard != null)
            PartialCard.SetActive(false);
    }

    public void ChangeToPlayfieldCard()
    {
        if (FullCard != null)
            FullCard.SetActive(false);
        if (PartialCard != null)
            PartialCard.SetActive(true);
    }

    private void CallbackListenersOnCardMovedEvent(Card card, int oldowner, CardPosition oldposition, int newonwer,
        CardPosition newposition)
    {
        if (card.InstanceId == InstanceId)
        {
            if (newposition == CardPosition.BattleField)
            {
                if(FullCard != null)
                    FullCard.SetActive(false);
                if(PartialCard != null)
                    PartialCard.SetActive(true);
            }
            else
            {
                if(FullCard != null)
                    FullCard.SetActive(true);
                if(PartialCard != null)
                    PartialCard.SetActive(false);
            }
        }
    }

    private void StatisticsChanged(Card changedcard)
    {
        OnCardRevealedEvent(changedcard, 0, CardPosition.Void);
    }

    private void OnCardRevealedEvent(Card revealedcard, int owner, CardPosition position)
    {
        if (revealedcard.InstanceId == InstanceId)
            changedCard = revealedcard;
    }

    private void setUpVisibleCardObjects(CardScriptable card)
    {
        setUpCardVisibility(card);
        setUpArtwork(card);
        if (itemConfigurators == null)
            itemConfigurators = new IPlayfieldItemConfiguration[]
                {costDisplay, objectHiding, textObjects, classAndType};
        foreach (var configurator in itemConfigurators)
        {
            configurator.setUp(card);
        }
    }

    private void setUpCardVisibility(CardScriptable card)
    {
        if (visibilityLayer == null) return;
        if (card.CardId == null)
            visibilityLayer.SetActive(false);
        else visibilityLayer.SetActive(true);
    }

    private void setUpArtwork(CardScriptable card)
    {
        artworkImage.sprite = card.Artwork;
        if (BattlefieldArtworkImage != null)
            BattlefieldArtworkImage.sprite = card.Artwork;
    }

    public int Increase(int amount = 1)
    {
        var card = (Original as CardScriptable);
        card.Amount += amount;
        objectHiding.setUp(card);
        return card.Amount;
    }

    public int Decrease(int amount = 1)
    {
        var card = (Original as CardScriptable);
        if (card.Amount - amount >= 0)
        {
            card.Amount -= amount;
            objectHiding.setUp(card);
            return card.Amount;
        }
        card.Amount = 0;
        objectHiding.setUp(card);
        return -1;
    }

    public void Set(int amount)
    {
        var card = (Original as CardScriptable);
        card.Amount = amount;
        objectHiding.setUp(card);
    }

    public int Get()
    {
        return (Original as CardScriptable).Amount;
    }
}
