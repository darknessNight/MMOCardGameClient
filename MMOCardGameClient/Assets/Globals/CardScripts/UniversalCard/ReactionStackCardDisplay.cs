﻿using System;
using System.Collections.Generic;
using System.Text;
using Backend.GameInformation;
using Backend.GameInformation.Enums;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class ReactionStackCardDisplay : MonoBehaviour, IPlayfieldItemConfiguration, IPointerEnterHandler,
    IPointerExitHandler
{
    private Transform previousParent = null;
    private Color32 previousColor = new Color32();
    private bool moveForeground = false;
    private bool isForeground = false;
    private int counter = -1;
    private List<TargetList> targetLists = null;

    public float HorizontalMove = 20;
    public Transform cardObject;
    public Transform foreground = null;
    public Transform cardSafeContainer = null;
    public Text nameText;
    public Text descriptionText;
    public Text TypeInfoText;
    public Text ClassInfoText;

    public Image cardShapeImage;
    public Image artworkImage;
    private readonly string cardAbilityString = "CardAbility.";
    private readonly string namedAbilityString = "NamedAbility.";
    private readonly string cardDescriptionString = "CardDescription";

    public int InstanceId { get; private set; }
    public int? CardId { get; private set; }

    
    public PlayersCollection playersCollection;
    public PlayfieldCardsCollection cardsCollection;

    void Start()
    {
        if(cardsCollection == null)
            Debug.Log("ReactionStackCardDisplay: PlayfieldCardsCollection not initlialized!");
    }

    public void setDescriptionText(String description)
    {
        Debug.Log("Card description <"+InstanceId+">: "+description);
        if (description.StartsWith(namedAbilityString))
            SetNamedAbilityDescription(description);
        else if (description.StartsWith(cardAbilityString))
            setCardAbilityDescription(description);
        else if (description.Equals(cardDescriptionString))
            setCardDescription();
        else
            descriptionText.text = UserInfo.LocalizationDictionary.getLocalizedTextIfExists(description);
    }

    private void setCardAbilityDescription(string description)
    {
        int abilityId = int.Parse(description.Substring(cardAbilityString.Length));

        if(CardId!=null)
        foreach (var ability in GameInfo.CardsCollection.getCard((int) CardId).activatedAbilities)
        {
            if (ability.id == abilityId)
            {
                descriptionText.text = ability.description;
                break;
            }
        }
    }

    private void setCardDescription()
    {
        if(CardId!=null)
            descriptionText.text = GameInfo.CardsCollection.getCard((int) CardId).description;
    }

    private void SetNamedAbilityDescription(string description)
    {
        int abilityId = int.Parse(description.Substring(namedAbilityString.Length));
        descriptionText.text = GameInfo.NamedAbilities.getNamedAbility(abilityId)?.description;
    }

    public ScriptableObject Original { get; private set; }

    public void setUp(ScriptableObject item)
    {
        var card = item as CardScriptable;
        if (card == null)
            throw new Exception("CardDisplay: Unable to configure card - the given Scriptable object is not a Card!");

        Original = card;
        InstanceId = card.InstanceId;
        CardId = card.CardId;
        if (card.CardId != null)
            setUpTextObjects(card);
        setUpSprites(card);
    }

    private void setUpSprites(CardScriptable cardScriptable)
    {
        artworkImage.sprite = cardScriptable.Artwork;
    }

    private void setUpTextObjects(CardScriptable cardScriptable)
    {
        if (cardScriptable.cardType == CardType.Unknown) return;
        nameText.text = cardScriptable.Name;

        TypeInfoText.text = UserInfo.getLocalizedString("Type: ") + cardScriptable.cardType;
        ClassInfoText.text = buildSubtypeAndClassInfoText(cardScriptable);
    }

    private string buildSubtypeAndClassInfoText(CardScriptable cardScriptable)
    {
        var builder = new StringBuilder();
        appendCardClassInfo(cardScriptable, builder);
        builder.Append('\t');
        appendCardSubtypeInfo(cardScriptable, builder);
        return builder.ToString();
    }

    private static void appendCardSubtypeInfo(CardScriptable cardScriptable, StringBuilder builder)
    {
        if (cardScriptable.subType == CardSubType.Unknown) return;
        builder.Append(UserInfo.getLocalizedString("Subtype: "));
        builder.Append(UserInfo.getLocalizedString(cardScriptable.subType.ToString()));
    }

    private static void appendCardClassInfo(CardScriptable cardScriptable, StringBuilder builder)
    {
        if (!string.IsNullOrEmpty(cardScriptable.cardClass))
        {
            builder.Append(UserInfo.getLocalizedString("Class: "));
            builder.Append(UserInfo.getLocalizedString(cardScriptable.cardClass));
            builder.Append("\t");
        }

        if (string.IsNullOrEmpty(cardScriptable.subClass)) return;
        builder.Append(UserInfo.getLocalizedString("Subclass: "));
        builder.Append(UserInfo.getLocalizedString(cardScriptable.subClass));
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        moveForeground = true;
        counter = -1;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        counter = 1;
    }

    public void Update()
    {
        if (counter >= 0)
        {
            if (counter == 0)
                moveForeground = false;

            counter--;
        }

        if (isForeground == moveForeground) return;

        if (moveForeground)
        {
            if (foreground != null)
            {
                isForeground = true;
                previousParent = cardObject.parent;
                cardObject.SetParent(foreground);
                cardObject.Translate(-HorizontalMove, 0, 0);
                cardSafeContainer?.Translate(-(HorizontalMove+2), 0, 0);
                previousColor = cardShapeImage.color;
                cardShapeImage.color = CardShapeColors.ReactionStackHoverCardColor;
            }
            iluminateTargets();
        }
        else
        {
            if (previousParent != null)
            {
                isForeground = false;
                cardObject.SetParent(previousParent);
                cardObject.Translate(HorizontalMove, 0, 0);
                cardSafeContainer?.Translate(HorizontalMove+2, 0, 0);
                previousParent = null;
                cardShapeImage.color = previousColor;
            }
            deiluminateTargets();
        }
    }

    private void iluminateTargets()
    {
        int i = -1;
        foreach(var targetList in targetLists){
            i++;
            if(targetList.OptionsType==ProtoBuffers.TargetSelectingTransaction.Types.OptionsType.Target ||
               targetList.OptionsType == ProtoBuffers.TargetSelectingTransaction.Types.OptionsType.CardInHand)
            foreach(var target in targetList.Targets){
                    if(target.TargetType==ProtoBuffers.TargetType.Player){
                        foreach(var playerInfos in playersCollection.GetListOfPlayers()){
                            if (playerInfos.playerId == target.Id)
                            {
                                playerInfos.playerIluminationOverlay.SetColor(i);
                                break;
                            }
                        }
                    }else{
                        cardsCollection.getCard(target.Id)?.GetComponent<MovableCardColorOverlay>().SetColor(i);
                    }
            }
        }
    }

    private void deiluminateTargets()
    {
        int i = -1;
        foreach (var targetList in targetLists)
        {
            i++;
            if (targetList.OptionsType == ProtoBuffers.TargetSelectingTransaction.Types.OptionsType.Target ||
               targetList.OptionsType == ProtoBuffers.TargetSelectingTransaction.Types.OptionsType.CardInHand)
                foreach (var target in targetList.Targets)
                {
                    if (target.TargetType == ProtoBuffers.TargetType.Player)
                    {
                        foreach (var playerInfos in playersCollection.GetListOfPlayers())
                        {
                            if (playerInfos.playerId == target.Id)
                            {
                            playerInfos.playerIluminationOverlay.Reset();
                                break;
                            }
                        }
                    }
                    else
                    {
                        cardsCollection.getCard(target.Id)?.GetComponent<MovableCardColorOverlay>().Reset();
                    }
                }
        }
    }

    public void setTargetLists(List<TargetList> stackElementTargetLists)
    {
        targetLists = stackElementTargetLists;
    }
}