﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using Backend.GameInformation.Enums;
using UnityEngine;
using UnityEngine.UI;

public class CardClassAndTypeDisplay : MonoBehaviour, IPlayfieldItemConfiguration 
{
	public Text TypeInfoText;
	public Text ClassInfoText;

	private CardScriptable cardScriptable;

	public ScriptableObject Original => cardScriptable;

	public void setUp(ScriptableObject item)
	{
		cardScriptable = item as CardScriptable;
		ClassInfoText.text = buildSubtypeAndClassInfoText();
		TypeInfoText.text = UserInfo.getLocalizedString("Type: ") + cardScriptable.cardType;
	}
	
	private string buildSubtypeAndClassInfoText()
	{
		var builder = new StringBuilder();
		appendCardClassInfo(builder);
		builder.Append(' ');
		appendCardSubtypeInfo(builder);
		return builder.ToString();
	}

	private void appendCardClassInfo(StringBuilder builder)
	{
		if (!string.IsNullOrEmpty(cardScriptable.cardClass))
		{
			builder.Append(UserInfo.getLocalizedString("Class: "));
			builder.Append(UserInfo.getLocalizedString(cardScriptable.cardClass));
			builder.Append(" ");
		}

		if (string.IsNullOrEmpty(cardScriptable.subClass)) return;
		builder.Append(UserInfo.getLocalizedString("Subclass: "));
		builder.Append(UserInfo.getLocalizedString(cardScriptable.subClass));
	}

	private void appendCardSubtypeInfo(StringBuilder builder)
	{
		if (cardScriptable.subType == CardSubType.Unknown) return;
		builder.Append(UserInfo.getLocalizedString("Subtype: "));
		builder.Append(UserInfo.getLocalizedString(cardScriptable.subType.ToString()));
	}
}
