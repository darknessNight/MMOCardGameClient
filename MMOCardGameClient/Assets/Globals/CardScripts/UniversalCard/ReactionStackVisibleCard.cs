﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ReactionStackVisibleCard : MonoBehaviour, IPointerExitHandler
{

	[SerializeField] private ReactionStackCardDisplay parentCard;

	public void OnPointerExit(PointerEventData eventData)
	{
		parentCard.OnPointerExit(eventData);
	}
}
