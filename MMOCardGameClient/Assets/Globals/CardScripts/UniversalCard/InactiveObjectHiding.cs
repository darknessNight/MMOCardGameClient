﻿using System;
using System.Collections;
using System.Collections.Generic;
using Backend.GameInformation.Enums;
using UnityEngine;
using UnityEngine.UI;

public class InactiveObjectHiding : MonoBehaviour, IPlayfieldItemConfiguration
{
	public GameObject[] ObjectsVisibleOnlyWithUnit = new GameObject[0];
	public GameObject OverloadOverlay;
	public GameObject AmountOverlay;
	public Text AmountText;
	
	private CardScriptable cardScriptable;
	private Text attackAndDefenceText;

	public ScriptableObject Original => cardScriptable;

	public void setUp(ScriptableObject item)
	{
		cardScriptable = item as CardScriptable;

		if (OverloadOverlay != null)
		{
			if (cardScriptable.overloadPoints > 0)
				OverloadOverlay.SetActive(true);
			else OverloadOverlay.SetActive(false);
		}

		bool shouldObjectBeVisible = cardScriptable.cardType == CardType.Unit;
		foreach (var gameObject in ObjectsVisibleOnlyWithUnit)
		{
			gameObject.SetActive(shouldObjectBeVisible);
		}

		if (AmountText != null && cardScriptable.Amount > 0)
		{
			AmountOverlay.SetActive(true);
			AmountText.text = cardScriptable.Amount.ToString();
		}	
		else if(AmountOverlay != null)
			AmountOverlay.SetActive(false);
	}
}
