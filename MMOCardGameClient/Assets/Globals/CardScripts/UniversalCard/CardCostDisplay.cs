﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardCostDisplay : MonoBehaviour, IPlayfieldItemConfiguration
{
	[SerializeField] private RectTransform CardCostsZone;
	[SerializeField] private List<GameObject> CardCostPrefabs;

	private CardScriptable currentCard;

	public ScriptableObject Original => currentCard;

	public void setUp(ScriptableObject item)
	{
		currentCard = item as CardScriptable;
		
		clearCostZone();
		addCostsToCard(currentCard.Cost.Neutral, currentCard.Cost.Psychos, currentCard.Cost.Raiders,
			currentCard.Cost.Scanners, currentCard.Cost.WhiteHats);
		CardCostsZone.GetComponent<SimpleHorizontalLayout>()?.ForceRefresh();
	}

	private void clearCostZone()
	{
		foreach (RectTransform item in CardCostsZone)
		{
			Destroy(item.gameObject);
		}
	}

	private void addCostsToCard(params int[] costs)
	{
		for (var i = 0; i < costs.Length; i++)
		{
			if(costs[i] <= 0) continue;
			var newCostItem = Instantiate(CardCostPrefabs[i]);
			newCostItem.GetComponentInChildren<Text>().text = costs[i].ToString();
			newCostItem.transform.SetParent(CardCostsZone);
		}
	}
}
