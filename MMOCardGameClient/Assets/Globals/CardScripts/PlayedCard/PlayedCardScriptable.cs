﻿using System.Text;
using Backend.GameInformation;
using Backend.GameInformation.Enums;
using UnityEngine;

[CreateAssetMenu(fileName = "New played card", menuName = "Cards/Played card")]
[System.Serializable]
public class PlayedCardScriptable : ScriptableObject
{
    public Card BackendCard;
    
    public string Name;

    public Sprite CardShape;
    public Sprite AttackShape;
    public Sprite DefenceShape;
    public Sprite OverloadShape;

    public Sprite Artwork;

    public Color CardShapeColor;
    public Color AttackShapeColor;
    public Color DefenceShapeColor;
    public Color OverloadShapeColor;

    public Color AttackTextColor;
    public Color DefenceTextColor;
    public Color OverloadTextColor;

    public int InstanceId => BackendCard.InstanceId;
    
    public int Attack
    {
        get => BackendCard.Attack ?? -1;
        set => BackendCard.Attack = value;
    }
    
    public int Defence
    {
        get => BackendCard.Defence ?? -1;
        set => BackendCard.Defence = value;
    }

    public int Overload
    {
        get => BackendCard.OverloadedPoints ?? 0;
        set => BackendCard.OverloadedPoints = value;
    }

    public CardType CardType;

    public void setUpDefaultSprites()
    {
        CardShape = Resources.Load<Sprite>("Sprites/Cards/PlayedCard");
        AttackShape = Resources.Load<Sprite>("Sprites/Cards/PlayedCardAttack");
        DefenceShape = Resources.Load<Sprite>("Sprites/Cards/PlayedCardDefence");
        OverloadShape = Resources.Load<Sprite>("Sprites/Cards/PlayedCardOverload");
    }

    public void setUpDefaultColors()
    {
        CardShapeColor = CardShapeColors.PlayedCardColor;
        AttackShapeColor = CardShapeColors.AttackShapeColor;
        DefenceShapeColor = CardShapeColors.DefenceShapeColor;
        OverloadShapeColor = CardShapeColors.OverloadShapeColor;
    }

    public override string ToString()
    {
        var builder = new StringBuilder();
        builder.Append("Name: ");
        builder.Append(Name);
        builder.Append(", Id: ");
        builder.Append(InstanceId);
        return builder.ToString();
    }
}
