﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayedCardDisplay : MonoBehaviour
{
    private readonly Color TRANSPARENT = new Color(0,0,0,0);

    public Image CardImage;
    public Image AttackImage;
    public Image DefenceImage;
    public Image OverloadImage;

    public Text AttackText;
    public Text DefenceText;
    public Text OverloadText;

    public Image ArtworkImage;


    public void SetupPlayedCard(PlayedCardScriptable playedCardScriptable)
    {
        setupSprites(playedCardScriptable);
        setupTextObjects(playedCardScriptable);
    }

    private void setupSprites(PlayedCardScriptable playedCardScriptable)
    {
        ArtworkImage.sprite = playedCardScriptable.Artwork;

        AttackImage.sprite = playedCardScriptable.AttackShape;
        DefenceImage.sprite = playedCardScriptable.DefenceShape;
        OverloadImage.sprite = playedCardScriptable.OverloadShape;

        AttackImage.color = playedCardScriptable.AttackShapeColor;
        DefenceText.color = playedCardScriptable.DefenceShapeColor;
        OverloadImage.color = playedCardScriptable.Overload > 0 ? playedCardScriptable.OverloadShapeColor : TRANSPARENT;
    }

    private void setupTextObjects(PlayedCardScriptable playedCardScriptable)
    {
        AttackText.text = playedCardScriptable.Attack.ToString();
        DefenceText.text = playedCardScriptable.Defence.ToString();
        OverloadText.text = playedCardScriptable.Overload.ToString();

        AttackText.color = playedCardScriptable.AttackTextColor;
        DefenceText.color = playedCardScriptable.DefenceTextColor;
        OverloadText.color = playedCardScriptable.Overload > 0 ? playedCardScriptable.OverloadTextColor : TRANSPARENT;
    }
}
