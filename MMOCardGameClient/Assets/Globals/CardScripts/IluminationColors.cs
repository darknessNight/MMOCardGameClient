﻿using UnityEngine;

public static class IluminationColors
{
    public static readonly Color32[] colors = {
        new Color32(0,255,0,128),
        new Color32(255,255,0,128),
        new Color32(200,155,0,128),
        new Color32(255,0,255,128),
        new Color32(0,255,255,128),
        new Color32(255,255,255,128),
    };
    public static readonly Color32 transparentColor = new Color32(0, 0, 0, 0);
}
