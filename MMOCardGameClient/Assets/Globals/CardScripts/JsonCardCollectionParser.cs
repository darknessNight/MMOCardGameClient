﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class JsonCardCollectionParser
{
    public JsonCardCollectionParser()
    {

    }

    public List<UserCardCollection> deserialize(string collectionString)
    {
        return JsonConvert.DeserializeObject<List<UserCardCollection>>(collectionString);
    }


}

public class UserCardCollection
{
    public long cardId;
    public string amount;
}
