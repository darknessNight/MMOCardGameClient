﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

public class ClickData
{
	public CardDisplay Card { get; set; }
	public PointerEventData.InputButton Button { get; set; }
	public Vector2 Position { get; set; }

	public ClickData(CardDisplay card, PointerEventData.InputButton button, Vector2 position)
	{
		Card = card;
		Button = button;
		Position = position;
	}
}

[Serializable]
public class ClickEvent : UnityEvent<ClickData>
{
}

public class MovableCard : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler, IPointerClickHandler, IMovable
{
	[SerializeField] private RectTransform visibleCard;
	[SerializeField] private RectTransform invisibleCard;
	public ClickEvent Click=new ClickEvent();
	public ClickEvent DoubleClick=new ClickEvent();
	[SerializeField] private float doubleClickDelayMs=100;
	private float lastClick = -1;
	private CardDisplay CardDisplay;
	public float AnimationMoveSpeed = 400;
	public float AnimationResizeSpeed = 1;
	public bool Dragable = false;
	
	
	public delegate void MovableEvent();
	public event MovableEvent MoveEnded;//it's cleared on occur
	public event MovableEvent ResizeEnded;//it's cleared on occur
	
	private bool mouseDrag;
	public Vector2 offset;
	private bool resizeStarted=false;
	private bool moveStarted=false;


	void Start()
	{
		CardDisplay = GetComponent<CardDisplay>();
	}
	
	void Update ()
	{
		if (!mouseDrag)
			MoveToInvisible();
		ResizeToInvisible();
	}

	private void ResizeToInvisible()
	{
		if (AnimationResizeSpeed < 0)
		{
			visibleCard.sizeDelta = invisibleCard.sizeDelta;
		}
		else
		{
			var destinationSize = invisibleCard.rect.size;
			var visibleDestination = visibleCard.sizeDelta;
			var deltaSize = destinationSize - visibleDestination;
			var animationResize = deltaSize.normalized * AnimationResizeSpeed;
			var afterAnimationSize = visibleDestination + animationResize;

			if (deltaSize.magnitude < animationResize.magnitude)
			{
				visibleCard.sizeDelta = destinationSize;
				if (resizeStarted)
				{
					ResizeEnded?.Invoke();
					ResizeEnded = null;
					resizeStarted = false;
				}
			}
			else visibleCard.sizeDelta = afterAnimationSize;
		}
	}

	private void MoveToInvisible()
	{
		var visiblePosition = visibleCard.position;
		var destination = invisibleCard.position;

		var deltaPosition = destination - visiblePosition;

		var move = deltaPosition.normalized * AnimationMoveSpeed * Time.deltaTime;

		if (move.magnitude > deltaPosition.magnitude)
		{
			move = deltaPosition;
		}
		
		if (moveStarted && Math.Abs(move.magnitude) < 0.1)
		{
			MoveEnded?.Invoke();
			MoveEnded = null;
			moveStarted = false;
		}
		
		visibleCard.position+=(move);
	}

	public void SetParent(Transform gameObject)
	{
		var prevPos=transform.position;
		transform.SetParent(gameObject);
		visibleCard.position = prevPos;
		moveStarted = true;
	}

	public void moveToPosition(float x, float y)
	{
		var visibleCardPos=transform.position;
		transform.position=new Vector3(x,y,0);
		visibleCard.position = visibleCardPos;
		moveStarted = true;
	}

	public void resizeToSize(float width, float height)
	{
		var visibleCardSize = visibleCard.sizeDelta;
		transform.GetComponent<RectTransform>().sizeDelta=new Vector2(width, height);
		visibleCard.sizeDelta = visibleCardSize;
		resizeStarted = true;
	}

	public void SetDragable(bool enable)
	{
		Dragable = enable;
	}

	public void OnDrag(PointerEventData eventData)
	{
		if (!Dragable) return;
		var curPosition = eventData.position + offset;
		visibleCard.position = curPosition;
		GetComponent<CanvasGroup>().blocksRaycasts = false;
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		if (!Dragable) return;
		mouseDrag = true;
		offset = visibleCard.position - new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 0);
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		if (!Dragable) return;
		mouseDrag = false;
		GetComponent<CanvasGroup>().blocksRaycasts = true;
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		var clickData=new ClickData(CardDisplay, eventData.button, eventData.position);
		if (eventData.clickCount % 2 == 0 && (eventData.clickTime - lastClick) <= doubleClickDelayMs)
			DoubleClick?.Invoke(clickData);
		else
			Click?.Invoke(clickData);
		lastClick = eventData.clickTime;
	}
}
