﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CardShapeColors 
{
	public static Color32 Transpartent => new Color32(0, 0, 0, 0);
	public static Color32 ReactionStackHoverCardColor => new Color32(25, 25, 25, 255);
	
	public static Color32 PlayedCardColor => new Color32(255, 255, 255, 200);
	public static Color32 AttackShapeColor => new Color32(255, 0, 0, 200);
	public static Color32 DefenceShapeColor => new Color32(255, 160, 0, 200);
	public static Color32 OverloadShapeColor => new Color32(200, 50, 50, 200);
}
