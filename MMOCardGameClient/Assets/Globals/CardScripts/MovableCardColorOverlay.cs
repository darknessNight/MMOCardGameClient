﻿using System;
using System.Collections;
using System.Collections.Generic;
using JSON.Entities.CardReader;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MovableCardColorOverlay : MonoBehaviour, IIluminationOverlay, IPointerClickHandler
{
	[SerializeField] private RectTransform visibleCard;
	[SerializeField] private RectTransform overlayBack;
	[SerializeField] private RectTransform overlayFront;
	private Image overlayBackImage;
	private Image overlayFrontImage;
	
	public float frontAlpha = 75.0f/255.0f;
	public float biggerPercent = 10;

    public IluminationClickEvent ClickEvent= new IluminationClickEvent();

    private int currentColor = -1;

	void Start()
	{
		if(overlayBack != null)
			overlayBackImage=overlayBack.GetComponent<Image>();
		overlayFrontImage=overlayFront.GetComponent<Image>();
	}

	public void SetColor(int i)
	{
		if(i>= IluminationColors.colors.Length)
			throw new ArgumentOutOfRangeException();
		if (overlayBackImage != null)
			overlayBackImage.color = IluminationColors.colors[i];
		overlayFrontImage.color = IluminationColors.colors[i];
		var color = overlayFrontImage.color;
		color.a = frontAlpha;
		overlayFrontImage.color = color;
        currentColor = i;
	}

	public void ToggleSelection(int i)
	{
		if(currentColor != -1)
			Reset();
		else
			SetColor(i);
	}

	public void Reset()
	{
        currentColor = -1;
		if(overlayBackImage != null)
			overlayBackImage.color = IluminationColors.transparentColor;
		overlayFrontImage.color = IluminationColors.transparentColor;
	}
	
	
	void Update ()
	{
		if (overlayBack != null)
		{
			overlayBack.sizeDelta = visibleCard.sizeDelta * (1 + biggerPercent / 100);
			overlayBack.position = visibleCard.position;
		}
		
		overlayFront.sizeDelta = visibleCard.sizeDelta;
		overlayFront.position = visibleCard.position;
	}

    public int GetColor()
    {
        return currentColor;
    }

    public IluminationClickEvent GetClickEvent()
    {
        return ClickEvent;
    }

	public void InvokeClick()
	{
		ClickEvent?.Invoke(this);
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		InvokeClick();
	}
}
