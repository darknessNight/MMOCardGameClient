﻿using System.Collections.Generic;
using Backend.Constants;
using JSON.Entities.CardReader;
using JSON.Readers.CardReader;

public class CardsCollection
{
    private Dictionary<int, CardJSON> cards=new Dictionary<int, CardJSON>();
    
    public CardsCollection()
    {
        foreach (var card in CardJSONReader.GetCards(StaticPaths.CardsJsonDirectory))
        {
            cards.Add(card.id,card);
        }
    }

    public CardJSON getCard(int id)
    {
        return cards[id];
    }
}