﻿using Backend.Main;
using UnityEngine;

public class BackendController : MonoBehaviour
{
    private IMainController mainController = null;

    public IBackendBuilder BackendBuilder;
    
    
    void Awake()
    {
        DontDestroyOnLoad(this);
    }
    
    public IMainController Controller
    {
        get
        {
            if (BackendBuilder != null && (BackendBuilder.ChangesOccurred() || mainController == null))  
                mainController = BackendBuilder.Build();
            return mainController;
        } 

        private set => mainController = value;
    }

    public void ResetController()
    {
        mainController = null;
        
    }
}
