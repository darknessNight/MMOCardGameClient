﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LineBehaviour : MonoBehaviour
{
    [SerializeField] private RectTransform Panel;
    [SerializeField] private Image Image;
    [SerializeField] private TimeSpan? DestroyAfterTime = null;
    [SerializeField] private float DestroyAfterSeconds = -1;

    public float X1
    {
        get => x1;
        set
        {
            updated = true;
            x1 = value;
        }
    }

    public float Y1
    {
        get => y1;
        set
        {
            updated = true;
            y1 = value;
        }
    }

    public float X2
    {
        get => x2;
        set
        {
            updated = true;
            x2 = value;
        }
    }

    public float Y2
    {
        get => y2;
        set
        {
            updated = true;
            y2 = value;
        }
    }

    public float Width
    {
        get => width;
        set
        {
            updated = true;
            width = value;
        }
    }

    public Color Color
    {
        get => color;
        set
        {
            updated = true;
            color = value;
        }
    }

    private float x1, y1, x2, y2, width = 2;
    private Color color = new Color32(0, 0, 0, 0);
    private bool updated;

    private void Start()
    {
        if (DestroyAfterSeconds > 0)
            DestroyAfterTime = new TimeSpan(0, 0, 0, 0, (int) (DestroyAfterSeconds * 1000));
    }

    void Update()
    {
        destroyIfTimesUp();
        updatePosAndSize();
    }

    private void destroyIfTimesUp()
    {
        if (DestroyAfterTime != null)
        {
            var timeLeft = DestroyAfterTime.Value.Add(new TimeSpan(0, 0, 0, 0, -(int) (Time.deltaTime * 1000)));
            if (timeLeft.Milliseconds < 0)
                Destroy(gameObject);
            DestroyAfterTime = timeLeft;
        }
    }

    private void updatePosAndSize()
    {
        if (!updated) return;
        updated = false;
        var vector = new Vector2(X2 - X1, Y2 - Y1);
        Panel.sizeDelta = new Vector2(Width, vector.magnitude);

        var position = new Vector3();
        position.x = X1;
        position.y = Y1;

        var result = Mathf.Atan2(vector.x, vector.y) * Mathf.Rad2Deg;
        Panel.rotation = Quaternion.Euler(0, 0, -result);

        Panel.position = position;

        Image.color = Color;
    }

    public void SetStartPosition(Vector3 pos)
    {
        X1 = pos.x;
        Y1 = pos.y;
    }

    public void SetStopPosition(Vector3 pos)
    {
        X2 = pos.x;
        Y2 = pos.y;
    }

    public void DestroyAfter(TimeSpan destroyAfterTime)
    {
        DestroyAfterTime = destroyAfterTime;
    }
}