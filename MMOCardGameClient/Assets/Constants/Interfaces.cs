﻿using System;
using System.Collections;

public interface ISessionManaging
{
    IEnumerator sessionStart();
    IEnumerator sessionDestroy();
}

public interface ILoginCredentialChecker
{
    bool validateLoginData();
    string getErrorMessage();
}

public interface ILoginDataConverter
{
    string convert();
}

public interface ILiteral
{
    void ILiteral(Enum @enum);
}

public interface IValidableCredentialStruct
{
    string Email();
    string Login();
    string Password();
    string OldPassword();
    string NewPassword();
    string RepeatedPassword();
    string RepeatedNewPassword();
}