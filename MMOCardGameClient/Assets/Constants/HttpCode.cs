﻿public enum HttpCode
{
    NetworkError = 0,

    Ok = 200,
    Created = 201,

    BadRequest = 400,
    Unauthorized = 401,
    Forbidden = 403,
    NotFound = 404,
    NotAcceptable = 406,
    RequestTimeout = 409,
    PreconditionFailed = 412,

    InternalServerError = 500
}
