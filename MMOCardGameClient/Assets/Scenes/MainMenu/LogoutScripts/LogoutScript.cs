﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LogoutScript : MonoBehaviour
{
    public Button LogoutButton;

    public void logout()
    {
        disableButtons();
        logoutAsync();
    }

    private void disableButtons()
    {
        LogoutButton.interactable = false;
    }

    private void logoutAsync()
    {
        var request = UserInfo.LoggedClient.logout();

        request.SuccessCallback += (v) =>
        {
            SceneManager.LoadSceneAsync("LoginScreen");
            enableButtons();
        };
        request.Send();
    }

    private void enableButtons()
    {
        LogoutButton.interactable = true;
    }
}