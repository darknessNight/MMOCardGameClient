﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class DeleteLogoutRequestBuilder : RestRequestBuilder
{
    public DeleteLogoutRequestBuilder() : base (UnityWebRequest.kHttpVerbDELETE)
    { 
        WebRequest.url = UserInfo.DataServerUrlAuthenticated;
        WebRequest.SetRequestHeader("X-Auth-Token", UserInfo.AuthenticationToken);
    }
}