﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CollectionButtonBehaviour : MonoBehaviour {

	void Start () {
		GetComponentInChildren<Text>().text = 
			UserInfo.getLocalizedString("HeadMenuCollectionButton");
	}
	
	public void changeSceneToCollectionScene()
	{
		try
		{
			SceneManager.LoadSceneAsync("CardCollection");
		} catch(Exception e)
		{
			Debug.Log(e.Message);
		}
	}	
}
