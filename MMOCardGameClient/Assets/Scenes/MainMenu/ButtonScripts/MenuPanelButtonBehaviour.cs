﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPanelButtonBehaviour : MonoBehaviour {

	public void ChangeSceneToDeckEditor()
	{
		if (SceneManager.GetActiveScene().name != @"DeckSelector")
			SceneManager.LoadSceneAsync(@"DeckSelector");
	}
}
