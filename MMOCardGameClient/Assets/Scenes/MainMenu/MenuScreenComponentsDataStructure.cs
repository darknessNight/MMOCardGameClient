﻿using UnityEngine;
using UnityEngine.UI;

public class MenuScreenComponentsDataStructure : MonoBehaviour
{
    public Button CollectionButton;
    public Button DeckButton;
    public Button LogoutButton;
    public Button PackagesButton;
    public Button PlayButton;
    public Button SettingsButton;
    public Button StoreButton;

    public GameObject SettingsPanel;
}