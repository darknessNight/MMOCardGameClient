﻿using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class OnLoadScript : MonoBehaviour
{
    public Text MenuGreetingText;

    private void Start()
    {
        MenuGreetingText.text = buildGreetingString();
    }

    private string buildGreetingString()
    {
        var greetingStringBuilder = new StringBuilder();
        greetingStringBuilder.Append(UserInfo.getLocalizedString("MenuTitleHelloPart"));
        greetingStringBuilder.Append(UserInfo.UserName);
        greetingStringBuilder.Append("!");
        return greetingStringBuilder.ToString();
    }
}