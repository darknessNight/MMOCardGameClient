﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class ChangeAccountSettingsRequestBuildertBase : RestRequestBuilder
{
    private readonly string settingUrlTail;

    public ChangeAccountSettingsRequestBuildertBase(string settingUrlTail, string requestMethod) : base(requestMethod)
    {
        this.settingUrlTail = settingUrlTail;
    }

    protected void setUpRequest(string requestBodyJsonString)
    {
        var adressBuilder = buildDataServerUserUrl();
        adressBuilder.Append(settingUrlTail);
        WebRequest.url = adressBuilder.ToString();
        WebRequest.SetRequestHeader("X-Auth-Token", UserInfo.AuthenticationToken);
        addUploadHandler(requestBodyJsonString);
    }
}
