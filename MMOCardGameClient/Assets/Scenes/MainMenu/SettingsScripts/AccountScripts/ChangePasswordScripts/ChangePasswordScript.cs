﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class ChangePasswordScript : MonoBehaviour
{
    private bool buttonsLocked;

    private ChangePasswordFormValidator FormValidator;
    private bool passwordChanged;
    public ChangePasswordScreenComponents PasswordScreenComponents;

    private void Start()
    {
        buttonsLocked = false;
        passwordChanged = false;
        FormValidator = new ChangePasswordFormValidator(PasswordScreenComponents);
    }

    private void Update()
    {
        if (PasswordScreenComponents.Panel.activeInHierarchy)
            enableChangePasswordButtonIfPossible();
    }

    private void enableChangePasswordButtonIfPossible()
    {
        if (formDataIsValidPasswordHasntChangedAndButtonsArentLocked())
            PasswordScreenComponents.ChangePasswordButton.interactable = true;
        else if (!FormValidator.validateForm())
            PasswordScreenComponents.ChangePasswordButton.interactable = false;
        else if (passwordChanged)
            PasswordScreenComponents.ChangePasswordButton.interactable = false;
    }

    private bool formDataIsValidPasswordHasntChangedAndButtonsArentLocked()
    {
        return !buttonsLocked &&
               !passwordChanged &&
               FormValidator.validateForm();
    }

    public void changePassword()
    {
        disableButtons();
        if (FormValidator.validateForm())
            StartCoroutine(changePasswordAsync());
    }

    private void disableButtons()
    {
        PasswordScreenComponents.ChangePasswordButton.interactable = false;

        PasswordScreenComponents.OldPasswordInputField.interactable = false;
        PasswordScreenComponents.NewPasswordInputField.interactable = false;
        PasswordScreenComponents.RepeatNewPasswordInputField.interactable = false;
        buttonsLocked = true;
    }

    public IEnumerator changePasswordAsync()
    {
        var changePasswordRequestBuilder = 
            new ChangePasswordRequestBuilder(getFormData(), UnityWebRequest.kHttpVerbPUT);
        yield return changePasswordRequestBuilder.WebRequest.SendWebRequest();

        getResponse(changePasswordRequestBuilder.WebRequest).getMessageForUser(ref PasswordScreenComponents.ServerInfoText);
        enableButtons();
    }

    private DataServerResponse getResponse(UnityWebRequest webRequest)
    {
        return new DataServerResponse.DataServerResponseBuilder(webRequest, "Change Email")
            .addResponseCodeMessage(HttpCode.Ok, UserInfo.getLocalizedString("PasswordChanged"), true)
            .addResponseCodeMessage(HttpCode.NotFound, UserInfo.getLocalizedString("PasswordChangingError"), false)
            .addResponseCodeMessage(HttpCode.Forbidden, UserInfo.getLocalizedString("UserDoesNotExist"), false)
            .build();
    }

    private ChangePasswordFormBody getFormData()
    {
        return new ChangePasswordFormBody()
        {
            password = PasswordScreenComponents.OldPasswordInputField.text,
            newPassword = PasswordScreenComponents.NewPasswordInputField.text,
            repeatedNewPassword = PasswordScreenComponents.RepeatNewPasswordInputField.text
        };
    }

    private void enableButtons()
    {
        PasswordScreenComponents.ChangePasswordButton.interactable = true;

        PasswordScreenComponents.OldPasswordInputField.interactable = true;
        PasswordScreenComponents.NewPasswordInputField.interactable = true;
        PasswordScreenComponents.RepeatNewPasswordInputField.interactable = true;
        buttonsLocked = false;
    }
}