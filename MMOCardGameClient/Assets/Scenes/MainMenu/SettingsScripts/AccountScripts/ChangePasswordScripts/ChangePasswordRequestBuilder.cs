﻿using UnityEngine.Networking;

public class ChangePasswordRequestBuilder : ChangeAccountSettingsRequestBuildertBase
{
    public ChangePasswordRequestBuilder(string oldPassword, string newPassword, string repeatNewPassword, 
        string requestMethod) :
        base(UserInfo.ChangePasswordTail, requestMethod)
    {
        setUpRequest(new ChangePasswordFormDataSerialization(
            new ChangePasswordFormBody()
            {
                newPassword = newPassword,
                password = oldPassword,
                repeatedNewPassword = repeatNewPassword
            }).setializeFormDataBody());
    }

    public ChangePasswordRequestBuilder(ChangePasswordFormBody data, string requestMethod) : 
        base(UserInfo.ChangePasswordTail, requestMethod)
    {
        setUpRequest(new ChangePasswordFormDataSerialization(data).setializeFormDataBody());
    }


}