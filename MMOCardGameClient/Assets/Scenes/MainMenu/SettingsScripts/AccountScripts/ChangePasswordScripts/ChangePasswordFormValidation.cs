﻿public class ChangePasswordFormValidator : FormDataValidator
{
    private readonly ChangePasswordScreenComponents Data;

    public ChangePasswordFormValidator(ChangePasswordScreenComponents dataStructure)
    {
        Data = dataStructure;
    }

    public bool validateForm()
    {
        return validatePassword(Data.OldPasswordInputField) &&
               validatePassword(Data.NewPasswordInputField) &&
               validatePassword(Data.RepeatNewPasswordInputField) &&
               checkIfFieldsAreEqual(Data.NewPasswordInputField, Data.RepeatNewPasswordInputField);
    }
}