﻿using Newtonsoft.Json;

public class ChangePasswordFormDataSerialization
{
    private readonly ChangePasswordFormBody Body;

    public ChangePasswordFormDataSerialization(ChangePasswordFormBody body)
    {
        Body = body;
    }

    public ChangePasswordFormDataSerialization(string oldPassword, string newPassword,
        string repeatedNewPassword)
    {
        Body = new ChangePasswordFormBody
        {
            password = oldPassword,
            newPassword = newPassword,
            repeatedNewPassword = repeatedNewPassword
        };
    }

    public string setializeFormDataBody()
    {
        return JsonConvert.SerializeObject(Body);
    }
}

public class ChangePasswordFormBody
{
    public string newPassword;
    public string password;
    public string repeatedNewPassword;
}