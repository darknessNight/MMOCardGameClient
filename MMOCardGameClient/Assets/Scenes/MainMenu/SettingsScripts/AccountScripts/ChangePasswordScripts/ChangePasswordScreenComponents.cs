﻿using UnityEngine;
using UnityEngine.UI;

public class ChangePasswordScreenComponents : MonoBehaviour
{
    public Button ChangePasswordButton;
    public Text NewPasswordInfo;
    public InputField NewPasswordInputField;

    public Text OldPasswordInfo;

    public InputField OldPasswordInputField;

    public GameObject Panel;
    public Text RepeatNewPasswordInfo;
    public InputField RepeatNewPasswordInputField;
    public Text TitleText;
    public Text ServerInfoText;
}