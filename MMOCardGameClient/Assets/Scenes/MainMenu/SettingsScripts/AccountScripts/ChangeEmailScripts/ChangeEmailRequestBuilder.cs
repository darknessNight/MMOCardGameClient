﻿using UnityEngine.Networking;

public class ChangeEmailRequestBuilder : ChangeAccountSettingsRequestBuildertBase
{
    public ChangeEmailRequestBuilder(string email, string repeatedEmail, string password, string requestMethod) : 
        base(UserInfo.EmailUrlTail, requestMethod)
    {
       setUpRequest(new ChangeEmailFormDataSerialization(
            new ChangeEmailFormBody()
            {
                newEmail = email,
                repeatedEmail = repeatedEmail,
                password = password
            }).setializeFormDataBody());
    }

    public ChangeEmailRequestBuilder(ChangeEmailFormBody data, string requestMethod) : 
        base(UserInfo.EmailUrlTail, requestMethod)
    {
        setUpRequest(new ChangeEmailFormDataSerialization(data).setializeFormDataBody());
    }
}