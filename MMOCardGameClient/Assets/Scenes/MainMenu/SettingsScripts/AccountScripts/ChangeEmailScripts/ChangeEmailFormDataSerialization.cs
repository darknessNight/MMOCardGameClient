﻿using Newtonsoft.Json;

public class ChangeEmailFormDataSerialization
{
    private readonly ChangeEmailFormBody Body;

    public ChangeEmailFormDataSerialization(ChangeEmailFormBody body)
    {
        Body = body;
    }

    public ChangeEmailFormDataSerialization(string newEmail, string repeatedEmail,
        string password)
    {
        Body = new ChangeEmailFormBody
        {
            newEmail = newEmail,
            repeatedEmail = repeatedEmail,
            password = password
        };
    }

    public string setializeFormDataBody()
    {
        return JsonConvert.SerializeObject(Body);
    }
}

public class ChangeEmailFormBody
{
    public string newEmail;
    public string password;
    public string repeatedEmail;
}