﻿using UnityEngine;
using UnityEngine.UI;

public class SettingsPanelComponentsDataStructure : MonoBehaviour 
{
    public GameObject CategoryPanel;

    public Button GraphicsSettingsButton;
    public Button AudioSettingsButton;
    public Button PersonalizationSettingsButton;
    public Button SocialSettingsButton;
    public Button AccountSettingsButton;

    public GameObject currentlyOpenSubCategoryPanel { get; set; }
    public GameObject currentlyOpenSettingPanel { get; set; }

    //public GameObject GraphicsSettingsCategoryPanel;
    //public GameObject AudioSettingsCategoryPanel;
    //public GameObject PersonalizationSettingsCategoryPanel;
    //public GameObject SocialSettingsCategoryPanel;
    public GameObject AccountSettingsCategoryPanel;
    
}
