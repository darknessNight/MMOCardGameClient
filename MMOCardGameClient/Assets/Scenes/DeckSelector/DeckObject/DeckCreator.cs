﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DeckCreator 
{
	public static DeckScriptable CreateDeck(string deckName, int amountOfCards)
	{
		var deckScriptable = ScriptableObject.CreateInstance<DeckScriptable>();
		deckScriptable.DeckName = deckName;
		deckScriptable.NumberOfCards = amountOfCards.ToString();
		return deckScriptable;
	}
}
