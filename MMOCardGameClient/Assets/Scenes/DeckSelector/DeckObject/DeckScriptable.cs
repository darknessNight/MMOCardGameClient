﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Deck", menuName = "Cards/Deck")]
[System.Serializable]
public class DeckScriptable : ScriptableObject
{
    public string DeckName;
    public string NumberOfCards;
}
