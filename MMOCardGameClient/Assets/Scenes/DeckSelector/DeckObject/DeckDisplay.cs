﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeckDisplay : MonoBehaviour, IPlayfieldItemConfiguration
{
	public Text DeckNameText;
	public Text AmountOfCardsText;

	public ScriptableObject Original { get; private set; }

	public void setUp(ScriptableObject item)
	{
		var deckScriptable = item as DeckScriptable;
		Original = deckScriptable;
		if(deckScriptable == null)
			throw new Exception("Unable to configure deck. Provided item is not of DeckScriptable type!");
		
		if(DeckNameText == null || AmountOfCardsText == null)
			Debug.LogWarning("DeckDisplay: Not all fields have been properly initialized!");

		DeckNameText.text = DeckNameText != null ? deckScriptable.DeckName : "Deck name not available";
		AmountOfCardsText.text = AmountOfCardsText != null ? deckScriptable.NumberOfCards : "-1";
	}
}
