﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using ExtensionMethods;
using UnityEngine;

public class UserDeckDeletion : MonoBehaviour
{
	public SelectionBehaviour SelectionBehaviour;
	public UserDecks UserDecks;

	private bool requestFinished = false;

	private bool allNecessaryComponentsAreInitialized => SelectionBehaviour != null && UserDecks != null;
	
	void Start()
	{
		if(!allNecessaryComponentsAreInitialized)
			Debug.LogError("UserDecksOperations in \"" + gameObject.GetComponent<RectTransform>().name + 
			               "\": Not all components are initialized!");
	}

	private void errorCallback(Exception e)
	{
		if(e != null)
			Debug.LogError(e.Message + "\n" + e.StackTrace);
	}

	public void DeleteDeck(string name)
	{
		sendNewDeleteDeckRequest(name);
	}

	public void DeleteSelectedDecks()
	{
		this.StartCoroutine(deleteDecksAsync(), errorCallback);
	}

	private IEnumerator deleteDecksAsync()
	{
		SelectionBehaviour.BlockSelectionUpdate = true;
		foreach (var deck in SelectionBehaviour.SelectedDecks)
		{
			sendNewDeleteDeckRequest(deck.DeckName);
			while (!requestFinished)
			{
				yield return new WaitForFixedUpdate();
			}
		}
		UserDecks.GetUserDecks();
		SelectionBehaviour.BlockSelectionUpdate = false;
		SelectionBehaviour.ClearSelection();
		yield return new WaitForFixedUpdate();
		SelectionBehaviour.CheckSelection();
		
	}

	private void sendNewDeleteDeckRequest(string deckName)
	{
		requestFinished = false;
		var request = UserInfo.LoggedClient.deleteDeck(deckName);
		request.ErrorCallback += obj =>
		{
			foreach (var error in obj)
			{
				Debug.Log(error);
			}

			StopAllCoroutines();
		};
		request.SuccessCallback += obj => { requestFinished = true; };
		request.Send();
	}
}
