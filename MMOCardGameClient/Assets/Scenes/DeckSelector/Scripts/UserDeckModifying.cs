﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UserDeckModifying : MonoBehaviour 
{
	public SelectionBehaviour SelectionBehaviour;

	
	void Start () {
		if(SelectionBehaviour == null)
			Debug.LogError("UserDeckModifying in \"" + gameObject.GetComponent<RectTransform>().name + 
			               "\": SelectionBehaviour is not initialized!");
	}

	public void ModifyDeck()
	{
		UserInfo.DeckToEdit = SelectionBehaviour.SelectedDecks[0].DeckName;
		SceneManager.LoadSceneAsync("DeckEditor");
	}
}
