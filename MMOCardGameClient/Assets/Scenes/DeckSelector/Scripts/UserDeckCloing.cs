﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ExtensionMethods;
using UnityEngine;

public class UserDeckCloing : MonoBehaviour
{

	public SelectionBehaviour SelectionBehaviour;
	public CreateNewDeckScript NewDeckScript;
	public UserDecks UserDecks;
	public UserDeckDeletion DeckDeletion;
	public LoadingPanelBehaviour LoadingPanel;

	private bool requestFinished = false;

	private bool allNecessaryComponentsAreInitialized => SelectionBehaviour != null && UserDecks != null &&
	                                                     NewDeckScript != null && LoadingPanel != null &&
	                                                     DeckDeletion != null;

	void Start()
	{
		if (!allNecessaryComponentsAreInitialized)
			Debug.LogError("UserDecksCloning in \"" + gameObject.GetComponent<RectTransform>().name +
			               "\": Not all components are initialized!");
	}

	public void CloneSelectedDeck()
	{
		NewDeckScript.CreateNewDeck();
	}

	public void AddCardsToClonedDeckCallback(string clonedDeckName, string newDeckName)
	{
		LoadingPanel.disable = false;
		LoadingPanel.LoadingPanel.SetActive(true);

		cloneCards(UserDecks.GetDeck(clonedDeckName), newDeckName);
	}

	private void cloneCards(UserDeckJSON deck, string newDeckName)
	{
		var request = UserInfo.LoggedClient.addCardsToDeck(newDeckName, deck.cardsWithAmounts);
		request.SuccessCallback = obj =>
		{
			UserDecks.GetUserDecks();
			SelectionBehaviour.ClearSelection();
			SelectionBehaviour.CheckSelection();
			LoadingPanel.disable = true;
		};
		request.ErrorCallback = obj =>
		{
			foreach (var error in obj)
			{
				Debug.Log(error);
			}

			Debug.LogError("Error during deck cloning! Deleting deck.");
			DeckDeletion.DeleteDeck(newDeckName);
			LoadingPanel.disable = true;
		};
		request.Send();
	}

}
