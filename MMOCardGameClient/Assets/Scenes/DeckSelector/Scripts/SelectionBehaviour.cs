﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SelectionBehaviour : MonoBehaviour
{
	public Button CreateDeckButton;
	public Button ModifyButton;
	public Button DeleteButton;
	public Button CloneButton;

	public RectTransform DeckZone;

	private bool allNecessaryComponentsInitialized => CreateDeckButton != null && ModifyButton != null &&
	                                                  DeleteButton != null && CloneButton != null &&
	                                                  DeckZone != null;
	
	public List<DeckScriptable> SelectedDecks { get; private set; } = new List<DeckScriptable>();
	public bool BlockSelectionUpdate { private get; set; } = false;

	void Start()
	{
		if(!allNecessaryComponentsInitialized)
			Debug.LogError("SelectionBehavior in \"" + gameObject.GetComponent<RectTransform>().name + 
			               "\": Not all components are initialized!");
		ModifyButton.interactable = DeleteButton.interactable = CloneButton.interactable = false;
	}

	void Update()
	{
		if(!BlockSelectionUpdate && LeftOrRightMB)
			CheckSelection();
	}

	private static bool LeftOrRightMB => (Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1));

	public void CheckSelection()
	{
		SelectedDecks = getSelectedDecks();

		if (SelectedDecks.Count == 0)
		{
			ModifyButton.interactable = DeleteButton.interactable = CloneButton.interactable = false;
		}
		else if (SelectedDecks.Count == 1)
		{
			ModifyButton.interactable = DeleteButton.interactable = CloneButton.interactable = true;
		}
		else if (SelectedDecks.Count > 1)
		{
			ModifyButton.interactable = CloneButton.interactable = false;
			DeleteButton.interactable = true;
		}
	}

	private List<DeckScriptable> getSelectedDecks()
	{
		var list = new List<DeckScriptable>();
		foreach (RectTransform deckRectTransform in DeckZone)
		{
			if(!deckRectTransform.GetComponent<CardSelect>().IsSelected) continue;
			var deck = deckRectTransform.GetComponent<IPlayfieldItemConfiguration>().Original as DeckScriptable;
			if (deck != null) list.Add(deck);
		}

		return list;
	}

	public void ClearSelection()
	{
		ModifyButton.interactable = DeleteButton.interactable = CloneButton.interactable = false;
		SelectedDecks.Clear();
	}
	
}
