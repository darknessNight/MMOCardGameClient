﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Backend.GameInformation;
using UnityEngine;
using Backend.Main;
using Google.Protobuf.WellKnownTypes;
using ProtoBuffers;
using UnityEngine.SceneManagement;

public class FirstHandChoices : MonoBehaviour
{
	[SerializeField] private SimpleItemZoneController FirstHandZone;
	[SerializeField] private RectTransform cardZone;
	[SerializeField] private SimpleHorizontalLayout layoutController;
	[SerializeField] private MulliganAndExchangeButtonControl buttonControl;
	
	private string sceneName = "FirstHandChoiceOverlay";
	private IMainController Controller;
	private bool updateHand = false;
	private bool iDidntCallNotifyYet = true;
	private bool changeToPlayfieldScene = false;
	private bool exchangeRequestSent = false;
	private bool mulliganRequestSent = false;
	
	private System.Object updateHandLock = new System.Object();
	

	void Start()
	{
		try
		{
			Debug.Log("Initializing FirstHandChoices");
			CallbackListeners.get().HandChangedEvent += OnHandChangeEvent;
			CallbackListeners.get().PhaseEndedEvent += OnPhaseEndedEvent;
			Controller = GameInfo.BackendController;
			CallbackListeners.get().OnPlayfieldInitializedEvent();
		}
		catch (Exception e)
		{
			Debug.Log(e.Message);
			CallbackListeners.get().OnClientSevereExceptionEvent(e.Message, "FirstHandChoice Start()");
		}
	}

	private void OnPhaseEndedEvent(GamePhase oldphase, int oldplayerid, GamePhase newphase, int newplayerid)
	{
		if (oldphase.Equals(GamePhase.FirstHandDraw))
			changeToPlayfieldScene = true;
	}


	void Update()
	{
		if (handUpdateUnlocked())
		{
			tryToUpdateFirstHand();
		}
			
		if (iDidntCallNotifyYet)
		{
			GameInfo.BackendController.NotifyThatPreparedForGame();
			iDidntCallNotifyYet = false;
		}
		if(changeToPlayfieldScene)
			SceneManager.UnloadSceneAsync(sceneName);
	}

	private bool handUpdateUnlocked()
	{
		lock(updateHandLock)
		{
			if (updateHand)
			{
				updateHand = false;
				return true;
			}
			return false;
		}
	}

	private void tryToUpdateFirstHand()
	{
		try
		{
			updateFirstHand();
		}
		catch (Exception e)
		{
			Debug.LogError(e.Message);
			CallbackListeners.get().OnClientSevereExceptionEvent(e.Message, "UpdateFirstHand", false);
		}
		layoutController.ForceRefresh();
	}

	private void updateFirstHand()
	{
		var userId = GameInfo.GameState.GetMyUserId();
		FirstHandZone.ClearZone();
		var currentHand = new Dictionary<int, Card>(GameInfo.GameState.GetPlayer((int) userId).Hand);
		//var tmp =GameInfo.GameState.GetPlayer((int) userId).Hand;
			
		if (currentHand.Count == 0)
		{
			buttonControl.DisableButtons();
			buttonControl.DisplayWaitingText();
		}
			
		foreach (var handCard in currentHand)
		{
			var addedGameObject = 
				FirstHandZone.AddItemToScene(CardCreator.createNewCard(handCard.Value), handCard.Value.InstanceId);
			if(mulliganRequestSent)
				addedGameObject.GetComponent<CardSelect>()?.DisableSelection();
		}
	}

	
	private void OnHandChangeEvent(int playerId)
	{
		if (playerId != GameInfo.GameState.GetMyUserId()) return;
		lock (updateHandLock)
		{
			updateHand = true;
		}
	}
	
	public void Muligan()
	{
		Controller.Mulligan();
		mulliganRequestSent = true;
	}

	public void AcceptHand()
	{
		Controller.AcceptHand();
	}

	public void ReplaceCards()
	{
		var rejectedCardInstanceIds = new List<int>();
		for (var i = 0; i < cardZone.childCount; i++)
		{
			var child = cardZone.GetChild(i);
			if (child.GetComponent<CardSelect>().IsSelected)
			{
				var childCardDisplay = child.GetComponent<CardDisplay>();
				rejectedCardInstanceIds.Add(childCardDisplay.InstanceId);
			}
				
		}

		if (rejectedCardInstanceIds.Count != 0)
		{
			Controller.ReplaceCards(rejectedCardInstanceIds);
			exchangeRequestSent = true;	
		}
	}
}
