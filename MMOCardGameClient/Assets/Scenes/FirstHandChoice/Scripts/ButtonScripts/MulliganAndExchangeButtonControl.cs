﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class MulliganAndExchangeButtonControl : MonoBehaviour
{
	[SerializeField] private RectTransform _cardZone;
	[SerializeField] private Button _exchangeButton;
	[SerializeField] private Button _mulliganButton;
	[SerializeField] private Button _acceptHandButton;

	[SerializeField] private Text _infoText;

	private bool _mulliganButtonClicked;
	private bool _atLeastOneCardHasBeenSelected;
	
	void Start () {
		checkIfGameObjectsAreInitialized();
		_exchangeButton.onClick.AddListener(OnExchangeButtonClick);
		_mulliganButton.onClick.AddListener(OnMulliganButtonClick);
		_acceptHandButton.onClick.AddListener(OnAcceptHandButtonClick);
	}

	private void checkIfGameObjectsAreInitialized()
	{
		var errorMessage = new StringBuilder();
		if (_cardZone == null) errorMessage.Append("\ncardZone");
		if (_exchangeButton == null) errorMessage.Append("\n>exchange button");
		if (_mulliganButton == null) errorMessage.Append("\n>muligan button");
		if (_acceptHandButton == null) errorMessage.Append("\n>keep hand button");
		if (_infoText == null) errorMessage.Append("\n>info text");
		if (errorMessage.Length == 0) return;
		Debug.LogError("MulliganAndExchangeButtonControl: The following item(s) aren't initialized - " + 
		               errorMessage);
		CallbackListeners.get().OnClientSevereExceptionEvent("The following item(s) aren't initialized - " +
			errorMessage, "MulliganAndExchangeButtonControl");
	}

	public void OnAcceptHandButtonClick()
	{
		DisableButtons();
		DisplayWaitingText();
	}

	public void OnMulliganButtonClick()
	{
		_exchangeButton.interactable = false;
		_mulliganButtonClicked = true;
		_acceptHandButton.GetComponentInChildren<Text>().text = UserInfo.getLocalizedString("Finish choosing");
	}

	public void OnExchangeButtonClick()
	{
		DisableButtons();
		DisplayWaitingText();
	}

	public void DisableButtons()
	{
		_acceptHandButton.interactable = false;
		_mulliganButton.interactable = false;
		_exchangeButton.interactable = false;
	}

	public void DisplayWaitingText()
	{
		_infoText.text = UserInfo.getLocalizedString("FirstHandChoiceIngoWaitingForOpponents");
	}

	public void ActivateExchangeButtonIfSelectionWasMade()
	{
		_exchangeButton.interactable = !_mulliganButtonClicked && checkIfAtLeastOneCardIsSelected();
	}

	private bool checkIfAtLeastOneCardIsSelected()
	{
		_atLeastOneCardHasBeenSelected = false;
		for (var i = 0; i < _cardZone.childCount; i++)
		{
			_atLeastOneCardHasBeenSelected = _atLeastOneCardHasBeenSelected ||
			                                 _cardZone.GetChild(i).GetComponent<CardSelect>().IsSelected;
		}

		return _atLeastOneCardHasBeenSelected;
	}
}
