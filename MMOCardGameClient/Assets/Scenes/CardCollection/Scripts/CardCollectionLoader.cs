﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Localization;
using UnityEngine.Networking;

public class CardCollectionLoader : MonoBehaviour 
{
	public CollectionComponentsDataStructure CollectionComponents;
    public string cardCollectionJson { get; private set; }

	void Start ()
	{
	    cardCollectionJson = string.Empty;
	}

	public IEnumerator getCardCollectionAsync()
	{
	    var getCardCollectionBuilder = new GetCardCollectionRequestBuilder(UnityWebRequest.kHttpVerbGET);
	    yield return getCardCollectionBuilder.WebRequest.SendWebRequest();

	    var response = getResponse(getCardCollectionBuilder.WebRequest);
        response.getMessageForUser(ref CollectionComponents.InfoText);
	    cardCollectionJson = response.DownloadedText;
	}

    private DataServerResponse getResponse(UnityWebRequest webRequest)
    {
        return new DataServerResponse.DataServerResponseBuilder(webRequest, "Get CardCollecion")
            .addResponseCodeMessage(HttpCode.Ok, string.Empty, true)
            .addResponseCodeMessage(HttpCode.Forbidden, UserInfo.getLocalizedString("UserDoesNotExist"), false)
            .setDefaultMessage(UserInfo.getLocalizedString("ErrorWhileDownloadingCollection"))
            .build();
    }
}
