﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class CardCollectionDisplay : MonoBehaviour 
{
	public List<CardScriptable> cardsList;
	public Transform contentPanel;
	public GameObject prefabCard;

	private Dictionary<int, GameObject> createdCards;

    void Start()
    {
        createdCards = new Dictionary<int, GameObject>();
    }

    public void Setup(List<CardScriptable> cards)
    {
        cardsList = cards;
        addCards();
    }

	private void addCards()
	{
		foreach(var card in cardsList)
		{
			var newCard = createNewCard(card);
			newCard.GetComponent<CardSelect>()?.DisableSelection();
			newCard.transform.SetParent(contentPanel);
			newCard.SetActive(true);
			createdCards.Add(card.CardId ?? -1, newCard);
		}
	}

	private GameObject createNewCard(CardScriptable cardScriptable)
	{
		var instantiatedPrefabCard = Instantiate(prefabCard);
		tryToConfigureCard(cardScriptable, instantiatedPrefabCard);
		return instantiatedPrefabCard;
	}

	private static void tryToConfigureCard(CardScriptable cardScriptable, GameObject instantiatedPrefabCard)
	{
		try
		{
			instantiatedPrefabCard.GetComponent<CardDisplay>().setUp(cardScriptable);
		}
		catch (Exception e)
		{
			var errorMessage = string.Concat("Unable to configure card with id: ", cardScriptable.InstanceId);
			Debug.LogWarning(errorMessage);
			CallbackListeners.get().OnClientSevereExceptionEvent(errorMessage, 
				"CardCollectionDisplay, tryToConfigureCard()");
		}
	}
}
