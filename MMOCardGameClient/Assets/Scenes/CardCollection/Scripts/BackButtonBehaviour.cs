﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BackButtonBehaviour : MonoBehaviour
{
    public Button ThisButton;
    public GameObject WaitingPanel;

	void Start ()
	{
		if (SceneManager.GetActiveScene().name == "MainMenu")
			ThisButton.interactable = false;

	}

    void Update()
    {
        if (ThisButton.interactable && WaitingPanel != null && WaitingPanel.activeInHierarchy)
            ThisButton.interactable = false;
        else if (WaitingPanel == null || !ThisButton.interactable && !WaitingPanel.activeInHierarchy)
            ThisButton.interactable = true;
    }

	public void changeSceneToMainMenuScene()
	{
		try
		{
            SceneManager.LoadSceneAsync("MenuScreen");
		} catch(Exception e)
		{
			Debug.Log(e.Message);
		}
	}

	public void changeSceneToDeckSelector()
	{
		try
		{
			SceneManager.LoadSceneAsync("DeckSelector");
		}
		catch (Exception e)
		{
			Debug.Log(e.Message);
		}
	}
	
}
