﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JSON.Entities.CardReader;
using JSON.Readers.CardReader;
using ExtensionMethods;

public class CardCollectionDisplayManager : MonoBehaviour
{
    public CollectionComponentsDataStructure CollectionComponents;

    public CardCollectionLoader CollectionLoader;
    public CardCollectionDisplay CollectionDisplay;

    private List<UserCardCollection> UsersCardCollection;


    private void displayExceptionMessage(Exception e)
    {
        if (e == null) return;
        Debug.LogError("CardCollectionDisplayManager, uncaughtException: " + e.Message);
    }

	void Start ()
	{
        this.StartCoroutine(LoadAndDisplayCollection(), displayExceptionMessage);
	}

    public IEnumerator LoadAndDisplayCollection()
    {
        CollectionComponents.LoadingCircle.SetActive(true);

        yield return CollectionLoader.getCardCollectionAsync();

        if (!string.IsNullOrEmpty(CollectionLoader.cardCollectionJson))
            createAndDisplayCards();
        
        CollectionComponents.LoadingCircle.SetActive(false);
    }

    private void createAndDisplayCards()
    {
        CollectionComponents.InfoText.text = UserInfo.getLocalizedString("PreparingCardCollection");
        UsersCardCollection = new JsonCardCollectionParser().deserialize(CollectionLoader.cardCollectionJson);
        CollectionDisplay.Setup(createCardCollectionList());

        CollectionComponents.LoadingPanel.GetComponent<LoadingPanelBehaviour>().disable = true;
        CollectionComponents.LoadingCircle.SetActive(false);
    }

    private List<CardScriptable> createCardCollectionList()
    {
        var allCardsList = new List<CardJSON>(CardJSONReader.GetCards(StaticPaths.CardsJsonDirectory));
        var cardCollectionToDisplay = new List<CardScriptable>();

        foreach (var cardJson in allCardsList)
        {
            var newCard = CardCreator.createNewCard(cardJson);
            newCard.Amount = getAmountOfCardsInUsersCollection(cardJson);
            cardCollectionToDisplay.Add(newCard);
        }

        return cardCollectionToDisplay;
    }

    private int getAmountOfCardsInUsersCollection(CardJSON currentlyLoadedCard)
    {
        try
        {
            foreach (var userCard in UsersCardCollection)
                if (userCard.cardId == currentlyLoadedCard.id)
                    return int.Parse(userCard.amount);
        }
        catch (Exception e)
        {
            Debug.Log("Unable to determine amount of given cardId " + currentlyLoadedCard.id + 
                      " - " + e.Message + ". Setting amount to \'0\'.");
        }

        return 0;
    }


}
