﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingPanelBehaviour : MonoBehaviour 
{
	public GameObject LoadingPanel;
	public Text InfoText;
	public Image BlinderImage;

	public bool disable = false;
	private float panelAlpha;
	public float animationSpeed = 0.5f;

	void Start()
	{
		LoadingPanel.SetActive(true);
		disable = false;
		panelAlpha = 1f;
	}

	void Update () {
		if(disable && LoadingPanel.activeInHierarchy)
			fadeOutAndDisable();
		else if(!disable)
			fadeInAndEnable();
	}

	private void fadeOutAndDisable()
	{
		if((panelAlpha -= Time.deltaTime * animationSpeed) >= 0)
		{
			BlinderImage.color = new Color(
				0f, 0f, 0f, panelAlpha
			);
			InfoText.color = new Color(
				1f, 1f, 1f, panelAlpha
			);
		}
		else
		{
			LoadingPanel.SetActive(false);
		}
	}

	private void fadeInAndEnable()
	{
		if(!LoadingPanel.activeInHierarchy) LoadingPanel.SetActive(true);
		if((panelAlpha += Time.deltaTime * animationSpeed) < 1)
		{
			BlinderImage.color = new Color(
				0f, 0f, 0f, panelAlpha
			);
			InfoText.color = new Color(
				1f, 1f, 1f, panelAlpha
			);
		}
	}
}
