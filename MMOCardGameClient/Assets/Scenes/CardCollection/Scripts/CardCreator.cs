﻿using System.Collections;
using System.Collections.Generic;
using JSON.Entities.CardReader;
using UnityEngine;
using System;
using System.Text;
using Backend.GameInformation;
using Backend.GameInformation.Enums;
using JetBrains.Annotations;

public static class CardCreator
{
    public static CardScriptable createNewCard(Card card)
    {
        var newCardScriptable = ScriptableObject.CreateInstance<CardScriptable>();
        newCardScriptable.BackendCard = card;
        if(card.CardId != null)
            initializeCardParameters(newCardScriptable, GameInfo.CardsCollection.getCard((int) card.CardId), card);
        loadSprites(newCardScriptable);
        return newCardScriptable;
    }
    
    public static CardScriptable createNewCard(int? id)
    {
        if (id != null)
            return createNewCard(GameInfo.CardsCollection.getCard((int) id));
        else return createNewCard(new Card(-1));
    }
    
    public static CardScriptable createNewCard(CardJSON cardJson)
    {
        var newCard = ScriptableObject.CreateInstance<CardScriptable>();
        initializeCardParameters(newCard, cardJson);
        loadSprites(newCard);
        return newCard;
    }

    private static void initializeCardParameters(CardScriptable newCardScriptable, CardJSON cardJson, Card card = null)
    {
        newCardScriptable.BackendCard = card ?? cardJSONToCard(cardJson, newCardScriptable.BackendCard?.InstanceId ?? -1);
        newCardScriptable.JsonCard = cardJson;
        if (card != null)
            newCardScriptable.Description = buildDescription(newCardScriptable, card, cardJson);
        else
            newCardScriptable.Description = cardJson.description;
    }

    public static Card cardJSONToCard(CardJSON cardJson, int instanceId)
    {
        CardCosts costs;
        if (cardJson.cost == null)
            costs = new CardCosts(0,0,0,0,0);
        else costs = new CardCosts(cardJson.cost.neutral, cardJson.cost.psychotronnics, 
            cardJson.cost.raiders, cardJson.cost.scanners, cardJson.cost.whiteHats);
        int attack = 0;
        int defence = 0;
        if (cardJson.statistics != null)
        {
            attack = cardJson.statistics.attack;
            defence = cardJson.statistics.defence;
        }
        
        return new Card(cardJson.id, instanceId, -1, null, attack, defence, costs, 
            getEnumFromString(cardJson.type, CardType.Unknown),
            getEnumFromString(cardJson.subtype, CardSubType.Unknown),
            cardJson.classValue, cardJson.subClass);
    }

    private static T getEnumFromString<T>(string value, T defaultValue) where T : struct
    {
        if (string.IsNullOrEmpty(value)) return defaultValue;
        T result;
        return Enum.TryParse<T>(value, out result) ? result : defaultValue;
    }

    private static void loadSprites(CardScriptable newCardScriptable)
    {
        if(newCardScriptable.cardType != CardType.Unknown) loadArtwork(newCardScriptable);
    }

    private static void loadArtwork(CardScriptable newCardScriptable)
    {
        try
        {
            newCardScriptable.Artwork = CardImagesLoader.loadImage(newCardScriptable.BackendCard.CardId ?? 0);
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
            newCardScriptable.Artwork = CardImagesLoader.loadDefaultImage();
        }
    }

    private static string buildDescription(CardScriptable newCardScriptable, Card card, CardJSON cardJson)
    {
        var builder = new StringBuilder();
        if (!string.IsNullOrEmpty(cardJson.description)) builder.Append(cardJson.description);
        foreach (var item in card.ActivatedAbilities)
        {
            builder.Append("\n");
            builder.Append(item.Value.Description);
        }

        return builder.ToString();
    }
}
