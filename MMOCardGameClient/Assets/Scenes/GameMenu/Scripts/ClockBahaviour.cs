﻿using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class ClockBahaviour : MonoBehaviour
{
    public Text TimeText;
    public bool RunTimer;

    private long waitingTimeSeconds;
    private double elapsedTime;

    void Start()
    {
        RunTimer = true;
        resetTime();
    }

    void OnEnable()
    {
        resetTime();
    }

    public void resetTime()
    {
        waitingTimeSeconds = 0;
        elapsedTime = 0;
        TimeText.text = @"00:00";
    }


    void Update()
    {
        if (waitingTimeSeconds < (waitingTimeSeconds += increaseTimer()))
            TimeText.text = buildTimeString();
    }

    private int increaseTimer()
    {
        var elapsedSeconds = 0;
        elapsedTime += Time.deltaTime;

        while (elapsedTime >= 1)
        {
            elapsedSeconds++;
            elapsedTime--;
        }

        return elapsedSeconds;
    }

    private string buildTimeString()
    {
        var timeStringBuilder = new StringBuilder();
        timeStringBuilder.Append((waitingTimeSeconds / 60).ToString("D2"));
        timeStringBuilder.Append(":");
        timeStringBuilder.Append((waitingTimeSeconds % 60).ToString("D2"));
        return timeStringBuilder.ToString();
    }
}
