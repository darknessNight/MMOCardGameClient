﻿using System;
using System.Collections;
using ExtensionMethods;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class SearchForGameScript : MonoBehaviour
{
    public GameObject WaitingPanel;
    public Text UserInfoText;
    public Button SearchButton;
    public Button CancelButton;

    public WaitForGameScript WaitForGame;

    private string adressTail = string.Empty;

    public string GameModeAdressTail
    {
        get { return adressTail; }
        set
        {
            adressTail = value;
            SearchButton.interactable = !string.IsNullOrEmpty(value);
        }
    }

    void Start()
    {
        UserInfo.SearchState = SearchState.NOT_SEARCHING;
    }

    void Update()
    {
        if (!string.IsNullOrEmpty(GameModeAdressTail) && !SearchButton.interactable)
            SearchButton.interactable = true;
    }
    
    private void displayExceptionMessage(Exception e)
    {
        UserInfoText.text = e.Message;
        UserInfoText.color = Color.red;
    }

    public void search()
    {
        WaitingPanel.SetActive(true);
        if (!string.IsNullOrEmpty(GameModeAdressTail))
        {
            UserInfoText.text = UserInfo.getLocalizedString("SearchingForMatch");
            MonoBehaviourExt.StartCoroutine(this, searchForGameAsync(), displayExceptionMessage);
        }
        else UserInfoText.text = UserInfo.getLocalizedString("GameModeNotSupported");

        setupCancelButton();
    }

    private void setupCancelButton()
    {
        CancelButton.GetComponentInChildren<Text>().text = UserInfo.getLocalizedString("CancelButton");
        CancelButton.onClick.RemoveAllListeners();
        CancelButton.onClick.AddListener(cancelSearch);
    }

    public void cancelSearch()
    {
        UserInfoText.text = UserInfo.getLocalizedString("CancellingSearch");
        StopCoroutine(searchForGameAsync());
        this.StartCoroutine(deleteSearchRequestAsync(), displayExceptionMessage);
    }


    private IEnumerator searchForGameAsync()
    {
        UserInfo.SearchState = SearchState.WAITING_FOR_QUEUE;
        var gameRequestBuilder = new GameQueuingRequestBuilder(GameModeAdressTail, UnityWebRequest.kHttpVerbPOST);   
        yield return gameRequestBuilder.WebRequest.SendWebRequest();

        getSearchResponse(gameRequestBuilder.WebRequest).getMessageForUser(ref UserInfoText);
        WaitForGame.waitForGame();
    }

    private DataServerResponse getSearchResponse(UnityWebRequest webRequest)
    {
        return new DataServerResponse.DataServerResponseBuilder(webRequest, GameModeAdressTail)
            .addResponseCodeMessage(HttpCode.Ok, string.Empty, true)
            .addResponseCodeMessage(HttpCode.Forbidden, string.Empty, false)
            .addResponseCodeMessage(HttpCode.PreconditionFailed, UserInfo.getLocalizedString("UserAlreadyInQueue"), 
                false)
            .build();
    }


    private IEnumerator deleteSearchRequestAsync()
    {
        var deleteGameRequestBuilder = new GameQueuingRequestBuilder(GameModeAdressTail, UnityWebRequest.kHttpVerbDELETE);
        yield return deleteGameRequestBuilder.WebRequest.SendWebRequest();

        var response =
            getDeleteResponse(deleteGameRequestBuilder.WebRequest);
        if (response.Success)
            UserInfo.SearchState = SearchState.NOT_SEARCHING;
        WaitingPanel.SetActive(false);
        Debug.Log("Cancelled search.");
    }

    private DataServerResponse getDeleteResponse(UnityWebRequest webRequest)
    {
        return new DataServerResponse.DataServerResponseBuilder(webRequest,
                "Delete " + GameModeAdressTail)
            .addResponseCodeMessage(HttpCode.Ok, string.Empty, true)
            .addResponseCodeMessage(HttpCode.Forbidden, UserInfo.getLocalizedString("UserDoesNotExist"), false)
            .addResponseCodeMessage(HttpCode.PreconditionFailed, UserInfo.getLocalizedString("UserAlreadyInQueue"),
                false)
            .build();
    }
}



public enum SearchState
{
    NOT_SEARCHING,
    WAITING_FOR_QUEUE,
    WAITING_FOR_SERVER_ADRESS,
    JOINING
}
