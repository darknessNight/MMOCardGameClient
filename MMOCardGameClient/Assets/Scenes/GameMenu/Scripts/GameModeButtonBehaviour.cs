﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameModeButtonBehaviour : MonoBehaviour
{
    public SearchForGameScript SearchForGame;
    public List<Button> ButtonList;

    public string GameModeAddressTail;

    void Start () {
		
	}
	
	void Update () {
		
	}

    public void setGameModeAdressTail()
    {
        SearchForGame.GameModeAdressTail = GameModeAddressTail;
    }

    public void enableAllButtons()
    {
        setAllButtons();
    }

    public void disableAllButtons()
    {
        setAllButtons(false);
    }

    public void disableAllButCurrentButton()
    {
        enableAllButtons();
        ButtonList[0].interactable = false;
    }

    private void setAllButtons(bool value = true)
    {
        foreach (var button in ButtonList)
        {
            button.interactable = value;
        }
    }
}
