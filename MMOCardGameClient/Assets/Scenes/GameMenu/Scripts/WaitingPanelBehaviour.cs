﻿using UnityEngine;
using UnityEngine.UI;

public class WaitingPanelBehaviour : MonoBehaviour
{
    public Text InfoText;
    public Text ClockText;
    public Image WaitingPanelImage;

    public bool displayWaitingPanel;

    private readonly Color INVISIBLE_WHITE = new Color(1.0f, 1.0f, 1.0f, 0.0f);
    private readonly Color INVISIBLE_BLACK = new Color(0.0f, 0.0f, 0.0f, 0.0f);
    private float textVisibility;
    private float panelVisibility;
    private float animationSpeed;

    void Start()
    {
        InfoText.color = INVISIBLE_WHITE;
        ClockText.color = INVISIBLE_WHITE;
        WaitingPanelImage.color= INVISIBLE_BLACK;
        textVisibility = 0.0f;
        animationSpeed = 1.0f;
        panelVisibility = 0.0f;
        displayWaitingPanel = true;
    }

    void Update()
    {
        if (displayWaitingPanel)
            fadeIn();
        else
            fadeOut();
    }

    private void fadeIn()
    {
        var deltaTime = Time.deltaTime * animationSpeed;
        if (textVisibility < 1.0f)
            setTextColors(new Color(1.0f, 1.0f, 1.0f, textVisibility += deltaTime));

        if (panelVisibility < 0.4f)
            WaitingPanelImage.color = new Color(0.0f, 0.0f, 0.0f, panelVisibility += deltaTime);
    }

    private void fadeOut()
    {
        var deltaTime = Time.deltaTime * animationSpeed;
        if (textVisibility > 0.0f)
            setTextColors(new Color(1.0f, 1.0f, 1.0f, textVisibility -= deltaTime));

        if (panelVisibility > 0.0f)
            WaitingPanelImage.color = new Color(0.0f, 0.0f, 0.0f, panelVisibility -= deltaTime);

    }

    private void setTextColors(Color textColor)
    {
        InfoText.color = textColor;
        ClockText.color = textColor;

    }
}
