﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ExtensionMethods;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class ConnectToBattleServerScript : MonoBehaviour
{
	public Text UserInfoText;
	public Button RetryButton;
	public Button AbortGameButton;

	public GameObject WaitingPanel;
	
	void Start()
	{
		checkCriticalClassVariables();
		AbortGameButton.gameObject.SetActive(false);
		AbortGameButton.interactable = false;
	}

	[Conditional("DEBUG")]
	private void checkCriticalClassVariables()
	{
		if(AbortGameButton == null) Debug.LogWarning("ConnectToBattleServerScript: Abort game button not initialized!");
		if(RetryButton == null) Debug.LogWarning("ConnectToBattleServerScript: Retry button not initialized!");
		if(UserInfoText == null) Debug.LogWarning("ConnectToBattleServerScript: User info text not initialized");
	}

	public void ConnectToBattleServer()
	{
		this.StartCoroutine(connectToBattleServerAsync(), displayExceptionMessage);
		setUpAbortGameButton();
		setUpRetryConnectingButton();
	}

	private void setUpRetryConnectingButton()
	{
		RetryButton.GetComponentInChildren<Text>().text = UserInfo.getLocalizedString("TryAgainToConnectToBattleServer");
		RetryButton.onClick.RemoveAllListeners();
		RetryButton.onClick.AddListener(
			() => this.StartCoroutine(connectToBattleServerAsync(), displayExceptionMessage)
		);
	}

	private void setUpAbortGameButton()
	{
		AbortGameButton.gameObject.SetActive(true);
		AbortGameButton.interactable = false;
		AbortGameButton.onClick.RemoveAllListeners();
		AbortGameButton.onClick.AddListener(abortGame);
	}

	private void abortGame()
	{
		WaitingPanel.SetActive(false);
		StopAllCoroutines();
		UserInfo.SearchState = SearchState.NOT_SEARCHING;
		AbortGameButton.gameObject.SetActive(false);
	}

	private void displayExceptionMessage(Exception e)
	{
		if (e == null) return;
		UserInfoText.text = e.Message;
		UserInfoText.color = Color.red;
	}

	private IEnumerator connectToBattleServerAsync()
	{
		RetryButton.interactable = false;
		initializeBackend();
		yield return new WaitForSeconds(1);

		for (var i = 0; i < UserInfo.BattleServerConnectionRetries; i++)
		{
			if(tryToConnect(i))
				yield break;
			else
				yield return new WaitForSeconds(1);
		}
		
		AbortGameButton.interactable = true;
		RetryButton.interactable = true;
		throw new Exception(UserInfo.getLocalizedString("PlayerIsNotInGame"));
	}

	private bool tryToConnect(int i)
	{
		try
		{
			GameInfo.BackendController.MakeConnection(GameInfo.ServerAdress1, GameInfo.Token);
			return true;
		}
		catch (Exception e)
		{
			Debug.LogError(e.Message + "\nStack trace:\n" + e.StackTrace + "\n\n");
			UserInfoText.text = string.Concat(UserInfo.getLocalizedString("PlayerIsNotInGameRetry"),
				(i + 1).ToString());
		}

		return false;
	}

	private void initializeBackend()
	{
		var backendController = GameObject.Find("Globals").GetComponentInChildren<BackendController>();
		if (backendController == null)
		{
			Debug.LogWarning("Couldn't find global object: BackendController!");
			return;
		}

		backendController.BackendBuilder.SetUserName(UserInfo.UserName);
		GameInfo.BackendController = backendController.Controller;
	}
}
