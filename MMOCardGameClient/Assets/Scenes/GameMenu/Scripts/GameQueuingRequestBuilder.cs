﻿using System.Text;
using UnityEngine.Networking;

public class GameQueuingRequestBuilder : RestRequestBuilder
{  
    public GameQueuingRequestBuilder(string gameModeAdressTail, string requestMethod) : base(requestMethod)
    {
        var adressBuilder = buildDataServerUserUrl(UserInfo.UserName);
        adressBuilder.Append(UserInfo.GameUrlTail);
        if (requestMethod != UnityWebRequest.kHttpVerbDELETE)
        {
            adressBuilder.Append("/");
            adressBuilder.Append(gameModeAdressTail);
        }
        WebRequest.url = adressBuilder.ToString();
        addAuthX_TokenHeader();
    }
}
