﻿using Backend.Main;
using ProtoBuffers;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class NextPhaseButtonScript : MonoBehaviour
{
    public GamePhase phase;
    public int phasePlayer;
    [SerializeField] private AttackDefenderTargetSelector TargetSelector;
    [SerializeField] private Button button;
    [SerializeField] private Text text;
    private IMainController backendController;
    private PlayerInfos playerInfos;
    private bool changed = true;

    public void Start()
    {
        phasePlayer = GameInfo.PlayerId;
        playerInfos = GetComponentInParent<PlayerInfos>();
        CallbackListeners.get().PhaseEndedEvent += OnPhaseEndedEvent;
        CallbackListeners.get().ReactToStackActionEvent+=OnReactToStackActionEvent;
        CallbackListeners.get().WaitForReactionToStackActionEvent+=OnWaitForReactionToStackActionEvent;
        backendController = GameInfo.BackendController;
    }

    private void OnWaitForReactionToStackActionEvent()
    {
        OnPhaseEndedEvent(GamePhase.PlayerRoundPhasesStart, -1, GamePhase.ReactionPhase, -2);
    }

    private void OnReactToStackActionEvent()
    {
        OnPhaseEndedEvent(GamePhase.PlayerRoundPhasesStart, -1, GamePhase.ReactionPhase, -1);
    }

    public void setBackendController(IMainController controller)
    {
        backendController = controller;
    }

    private void OnPhaseEndedEvent(GamePhase oldPhase, int oldPlayerId, GamePhase phase, int phasePlayer)
    {
        this.phase = phase;
        this.phasePlayer = phasePlayer;
        changed = true;
    }

    public void Update()
    {
        if (!changed) return;
        changed = false;
        button.interactable = shouldBeInteractable();
        text.text = "EndPhase." + phase;
    }

    private bool shouldBeInteractable()
    {
        if (playerInfos != null)
            return (playerInfos.playerId.Equals(phasePlayer) || phasePlayer == -1) &&
                   !phase.Equals(GamePhase.AttackExecutionPhase);
        else return false;
    }

    public void EndPhase()
    {
        if (phase == GamePhase.Attack || phase == GamePhase.Defence)
            endAttackPhase();
        else backendController?.EndPhase();
    }

    private void endAttackPhase()
    {
        var attackers = TargetSelector.CollectAttackersAndClean();
        backendController?.AttackDefend(attackers);
    }
}