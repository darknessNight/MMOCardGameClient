﻿using System.Collections;
using System.Collections.Generic;
using ProtoBuffers;
using UnityEngine;

public class PlayerInfos : MonoBehaviour
{
    public int playerId;
    public Dictionary<int, PlayerCard> playerCards;
    public PlayerIluminationOverlay playerIluminationOverlay;

    private void Start()
    {
        GetComponentInParent<PlayersCollection>()?.AddPlayer(this);
    }
}
