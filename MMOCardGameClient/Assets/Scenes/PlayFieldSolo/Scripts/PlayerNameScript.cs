﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerNameScript : MonoBehaviour
{
    void Start()
    {
        CallbackListeners.get().PlayfieldInitializedEvent += OnPlayfieldInitializedEvent;
    }

    private void OnPlayfieldInitializedEvent()
    {
        var text=GetComponentInChildren<Text>();
        var playersInfo = GetComponentInParent<PlayerInfos>();
        if(text!=null && playersInfo!=null)
            text.text = GameInfo.GameState.GetPlayer(playersInfo.playerId).Name;
    }
}
