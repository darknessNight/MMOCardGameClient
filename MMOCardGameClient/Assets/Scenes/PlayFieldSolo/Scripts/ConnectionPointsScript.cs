﻿using System;
using System.Collections;
using System.Collections.Generic;
using Backend.GameInformation;
using UnityEngine;
using UnityEngine.UI;

public class ConnectionPointsScript : MonoBehaviour
{
    [SerializeField] private Text raiders;
    [SerializeField] private Text scanners;
    [SerializeField] private Text psychos;
    [SerializeField] private Text whiteHats;
    [SerializeField] private Text neutral;
    private PlayerInfos playerInfos;
    private LinkPoints linkPoints = new LinkPoints(0, 0, 0, 0, 0);

    public bool displayPrefixes = true;

    private bool changed = true;

    void Start()
    {
        try
        {
            playerInfos = GetComponentInParent<PlayerInfos>();//search for nearest component
            CallbackListeners.get().LinkPointsUpdatedEvent += OnLinkPointsUpdatedEvent;
        }
        catch (NullReferenceException)
        {
            Debug.Log("Something is strange on this Scene");
        }
    }

    private void OnLinkPointsUpdatedEvent(int playerId, LinkPoints playerLinkPoints)
    {
        if (playerId == playerInfos.playerId)
        {
            linkPoints = playerLinkPoints;
            changed = true;
        }
    }

    void Update()
    {
        if (!changed) return;
        changed = false;
        raiders.text = (displayPrefixes ? "R:" : "") + linkPoints.Raiders;
        whiteHats.text = (displayPrefixes ? "W:" : "") + linkPoints.WhiteHats;
        scanners.text = (displayPrefixes ? "S:" : "") + linkPoints.Scanners;
        psychos.text = (displayPrefixes ? "P:" : "") + linkPoints.Psychos;
        neutral.text = (displayPrefixes ? "N:" : "") + linkPoints.Neutral;
    }
}