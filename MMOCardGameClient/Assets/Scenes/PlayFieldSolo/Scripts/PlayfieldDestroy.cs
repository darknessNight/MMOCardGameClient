﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayfieldDestroy : MonoBehaviour {
	private void OnDestroy()
	{
		Debug.LogWarning("Playfield destroyed");
		GameInfo.BackendController?.CancelConnection();
		var backendController = GameObject.Find("Globals").GetComponentInChildren<BackendController>();
		if (backendController != null)
		{
			backendController.ResetController();
		}
		else
			Debug.LogError("Eh");
		
		CallbackListeners.ResetEventListeners();
	}
}
