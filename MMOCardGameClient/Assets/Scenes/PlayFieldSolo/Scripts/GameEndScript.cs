﻿using System.Collections;
using System.Collections.Generic;
using ExtensionMethods;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEndScript : MonoBehaviour {

	void Start () {
		CallbackListeners.get().GameEndedEvent+=OnGameEndedEvent;
		CallbackListeners.get().PlayerWonEvent += id => OnGameEndedEvent(GameEndedResult.Lost);
	}

	private void OnGameEndedEvent(GameEndedResult result)
	{
		this.StartCoroutine(endGameAsync().GetEnumerator());
	}

	private IEnumerable endGameAsync()
	{
		yield return new WaitForSecondsRealtime(5);
//		SceneManager.UnloadSceneAsync("SoloPlayfield");
		SceneManager.LoadSceneAsync("GameEndScreen");
	}
}
