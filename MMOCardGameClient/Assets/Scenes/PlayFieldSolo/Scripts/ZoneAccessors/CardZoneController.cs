﻿using System;
using System.Collections.Generic;
using Backend.GameInformation;
using Backend.GameInformation.Enums;
using ProtoBuffers;
using UnityEngine;

public class CardZoneController : SimpleItemZoneController
{
    public CardType cardTypeLimit = CardType.Unknown;
    public CardPosition zonePosition;
    public PlayfieldCardsCollection CardsCollection;
    public bool getUndefined = false;
    private PlayerInfos PlayerInfos;
    
    public void Start()
    {
        FindZoneIfNotSet();
        if(CardsCollection == null)
            Debug.Log("CardZoneController in "+ gameObject.GetComponent<RectTransform>().name + 
                      ": PlayfieldCardsCollection not initlialized!");
        CallbackListeners.get().CardMovedEvent+=OnCardMovedEvent;
        CallbackListeners.get().CardRevealedEvent+=OnCardRevealedEvent;
        PlayerInfos = GetComponentInParent<PlayerInfos>();
    }

    private void OnCardRevealedEvent(Card card, int owner, CardPosition position)
    {
        if (!zoneShouldContainCard(card)) return;
        if (HasItem(card.InstanceId))
            RemoveItem(card.InstanceId);
        if (owner == PlayerInfos.playerId && position == zonePosition)
        {
            if (!CardsCollection.HasCard(card.InstanceId))
                AddItemToScene(CardsCollection.getCard(card.InstanceId), card.InstanceId);
        }
    }

    private void OnCardCreatedEvent(Card card, int owner, CardPosition position)
    {
        if (!zoneShouldContainCard(card)) return;
        if (owner == PlayerInfos.playerId && position == zonePosition)
        {
            if (!HasItem(card.InstanceId))
                AddNewCardToScene(card);
        }
    }

    private void OnCardMovedEvent(Card card, int oldOwner, CardPosition oldPosition, int newOwner, CardPosition newPosition)
    {
        if (!zoneShouldContainCard(card)) return;
        if (oldOwner == PlayerInfos.playerId && oldPosition == zonePosition)
        {
            if (HasItem(card.InstanceId))
                RemoveItem(card.InstanceId);
        }
        if (newOwner == PlayerInfos.playerId && newPosition == zonePosition)
        {
            if (!HasItem(card.InstanceId))
                AddItemToScene(CardsCollection.getCard(card.InstanceId), card.InstanceId);
        }
    }

    private bool zoneShouldContainCard(Card card)
    {
        return card.Type == cardTypeLimit || cardTypeLimit== CardType.Unknown ||
                    (getUndefined && card.Type == CardType.Unknown);
    }
}