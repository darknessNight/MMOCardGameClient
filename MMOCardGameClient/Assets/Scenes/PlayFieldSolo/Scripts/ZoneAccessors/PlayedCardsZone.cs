﻿using System;
using System.Collections.Generic;
using Backend.GameInformation;
using Backend.GameInformation.Enums;
using ProtoBuffers;
using UnityEngine;
using UnityEngine.UI;

public class PlayedCardsZone : MonoBehaviour, IPlayedCardsZone
{
    public RectTransform ProgramServiceCardsZone;
    public RectTransform OtherCardsZone;
    public RectTransform ConnectionCardsZone;
    public RectTransform ComputingUnitCardsZone;

    public GameObject PrefabPlayedCard;
    
    private PlayerInfos PlayerInfos;

    private Dictionary<int, GameObject> _createdPlayedCards;

    void Start()
    {
        _createdPlayedCards = new Dictionary<int, GameObject>();
    }

    public GameObject AddPlayedCard(PlayedCardScriptable playedCardScriptable)
    {
        if(_createdPlayedCards.ContainsKey(playedCardScriptable.InstanceId)) 
            throw new Exception("The card [" + playedCardScriptable.ToString() +"] already exists!");
        
        var newPlayedCard = createNewPlayedCard(playedCardScriptable);

        var layoutElement = newPlayedCard.AddComponent<LayoutElement>();
        layoutElement.flexibleHeight = 0;

        newPlayedCard.transform.SetParent(getContentZoneTransform(playedCardScriptable));
        newPlayedCard.SetActive(true);

        _createdPlayedCards.Add(playedCardScriptable.InstanceId, newPlayedCard);
        return newPlayedCard;
    }

    private Transform getContentZoneTransform(PlayedCardScriptable playedCardScriptable)
    {
        if (playedCardScriptable.BackendCard.CardId == 0) return ComputingUnitCardsZone;
        if (playedCardScriptable.CardType == CardType.Link) return ConnectionCardsZone;
        if (playedCardScriptable.CardType == CardType.ServiceProgram) return ProgramServiceCardsZone;
        return OtherCardsZone;
    }

    private GameObject createNewPlayedCard(PlayedCardScriptable playedCardScriptable)
    {
        var instantiatedPrefabPlayedCard = Instantiate(PrefabPlayedCard);
        instantiatedPrefabPlayedCard.GetComponent<PlayedCardDisplay>().SetupPlayedCard(playedCardScriptable);

        return instantiatedPrefabPlayedCard;
    }

    public void DestroyCard(int cardId)
    {
        if (!_createdPlayedCards.ContainsKey(cardId))
        {
            Debug.LogWarning("PlayedCardsAdder: Tried to remove non existing card with id: " + cardId.ToString());
            return;
        }
        
        Destroy(_createdPlayedCards[cardId]);
        _createdPlayedCards.Remove(cardId);
    }
}
