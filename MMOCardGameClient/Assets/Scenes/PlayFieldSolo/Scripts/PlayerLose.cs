﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLose : MonoBehaviour
{
	[SerializeField] private PlayerInfos playerInfos;
	[SerializeField] private RectTransform PlayerField;
	[SerializeField] private Transform ElementsOverlay;
	[SerializeField] private Color DarkeningColor = new Color32(0, 0, 0, 128);
	
	void Start () {
		CallbackListeners.get().PlayerLoseEvent+=OnPlayerLoseEvent;
	}

	private void OnPlayerLoseEvent(int playerid)
	{
		if (playerInfos.playerId == playerid)
		{
			GameObject panel = new GameObject("DarkSide");
			panel.AddComponent<Image>().color = DarkeningColor;
			var transform = panel.GetComponent<RectTransform>();
			transform.sizeDelta = PlayerField.sizeDelta;
			transform.position = PlayerField.position;
			transform.SetParent(ElementsOverlay);
		}
	}
}
