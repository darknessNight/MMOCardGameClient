﻿using System.Collections;
using System.Collections.Generic;
using ProtoBuffers;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FirstHandChoiceDisplayer : MonoBehaviour 
{
	void Start ()
	{
		if (GameInfo.BackendController == null && Application.isEditor) return;
		SceneManager.LoadScene("FirstHandChoiceOverlay", LoadSceneMode.Additive);
	}
}
