﻿using System.Collections;
using System.Collections.Generic;
using Backend.GameInformation;
using Backend.GameInformation.Enums;
using ProtoBuffers;
using UnityEngine;

public class ReactionStackController : MonoBehaviour
{
    private GameState GameState;
    private int playerId;
    [SerializeField] private SimpleItemZoneController reactionStackItemController;
    [SerializeField] private PlayfieldCardsCollection CardsCollection;
    [SerializeField] private Transform ReactionStackCardSafe;
    [SerializeField] private PlayfieldCardsCollection cardsCollection;
    [SerializeField] private PlayersCollection playersCollection;
    private bool playStarted = false;
    private bool triggerStarted = false;
    private TriggerType triggerType = TriggerType.UndefinedTriggerType;

    void Start()
    {
        GameState = GameInfo.GameState;
        playerId = (int) GameState.GetMyUserId();
        var listeners = CallbackListeners.get();
        listeners.FinishedChoosingEvent += OnFinishedChoosingEvent;
        listeners.ElementAddedToStackEvent += OnElementAddedToStackEvent;
        listeners.StackActionCompletedEvent+=OnStackActionCompletedEvent;
        listeners.StackActionRemovedEvent += OnStackActionCompletedEvent;
        listeners.TriggerActivatedEvent += OnTriggerActivatedEvent;
        listeners.TriggerAddedToStackEvent += OnElementAddedToStackEvent;
    }

    void OnTriggerActivatedEvent(int instanceId, TriggerType triggerType)
    {
        triggerStarted = true;
        this.triggerType = triggerType;
    }

    private void OnStackActionCompletedEvent(int stackId, StackElement stackElement)
    {
        reactionStackItemController.DestroyItem(stackId);
    }

    public void CardPlay(GameObject gameObject)
    {
        var card = gameObject.GetComponent<CardDisplay>();
        if (card != null)
            CardPlay(card);
    }

    public void CardPlay(ClickData clickData)
    {
        CardPlay(clickData.Card);
    }

    public void CardPlay(CardDisplay card)
    {
        if (GameState == null) return;
        if (GameState.GetPlayer(playerId).Hand.ContainsKey(card.InstanceId))
        {
            GameInfo.BackendController?.PlayCard(card.InstanceId);
            playStarted = true;
        }
        else if (GameState.GetPlayer(playerId).BattleField.ContainsKey(card.InstanceId)
            && (GameState.CurrentPhase!= GamePhase.Attack && GameState.CurrentPhase!=GamePhase.Defence || GameState.GetPlayer(playerId).BattleField[card.InstanceId].Type!=CardType.Unit ))
        {
            GameInfo.BackendController?.UseCardAbility(card.InstanceId);
            playStarted = true;
        }
    }

    private void OnFinishedChoosingEvent(int transactionid, int instanceid)
    {
        if (playStarted)
        {
            GameInfo.BackendController?.FinishPlay(transactionid, instanceid);
            playStarted = false;
        }
        if (triggerStarted){
            GameInfo.BackendController?.AcceptTrigger(transactionid, instanceid, triggerType);
            triggerStarted = false;
        }
    }

    private void OnElementAddedToStackEvent(int stackId, StackElement stackElement)
    {
        if (stackId == -1)
            return;

        var cardObject = CardsCollection.getCard(stackElement.InstanceId);
        
        var movable = cardObject.GetComponent<MovableCard>();
        
        var players = GameInfo.GameState.GetPlayers();
        var isOnBattlefield = false;
        foreach (var player in players)
        {
            if (player.Value.BattleField.ContainsKey(stackElement.InstanceId))
                isOnBattlefield = true;
        }
        
        if (movable != null)
        {
            if (!isOnBattlefield)
            {
                movable.SetParent(ReactionStackCardSafe);
                movable.MoveEnded += () =>
                    AddCardToReactionStack(stackId, stackElement, cardObject);
            }
            else
            {
                AddCardToReactionStack(stackId, stackElement, cardObject);
            }
        }
        else 
        {
            if (!isOnBattlefield)
                cardObject.transform.SetParent(ReactionStackCardSafe);
            AddCardToReactionStack(stackId, stackElement, cardObject);
        }
    }

    private ReactionStackCardDisplay AddCardToReactionStack(int stackId, StackElement stackElement, GameObject cardObject)
    {
        var card = cardObject.GetComponent<CardDisplay>();
        var cardScriptable = CardCreator.createNewCard(card.CardId);
        var stackCard = reactionStackItemController.AddItemToScene(cardScriptable, stackId)
            .GetComponent<ReactionStackCardDisplay>();
        if (stackElement.AbilityId == -1 || stackElement.TargetLists.Count <= 0)
            stackCard.setDescriptionText("CardDescription");
        else stackCard.setDescriptionText(stackElement.TargetLists[0].Description);

        stackCard.setTargetLists(stackElement.TargetLists);
        stackCard.cardsCollection = cardsCollection;
        stackCard.playersCollection = playersCollection;

        return stackCard;
    }

    public bool IsPlayInProgress(){
        return playStarted;
    }
}