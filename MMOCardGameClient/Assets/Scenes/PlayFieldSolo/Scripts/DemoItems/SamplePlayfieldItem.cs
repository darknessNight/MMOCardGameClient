﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Sample Item", menuName = "Cards/SampleItem")]
[System.Serializable]
public class SamplePlayfieldItem : ScriptableObject
{
    public Sprite sampleShape;
}
