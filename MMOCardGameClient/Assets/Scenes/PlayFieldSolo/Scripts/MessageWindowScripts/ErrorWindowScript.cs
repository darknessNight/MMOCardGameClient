﻿

using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class ErrorWindowScript : MonoBehaviour
{
	[SerializeField] private Text OkButton;
	[SerializeField] private Text ErrorMessageText;
	[SerializeField] private Text WindowTitleText;
	[SerializeField] private GameObject MainPanel;

	private bool shutDownAfterClosing = false;
	
	void Start ()
	{
		checkCriticalClassVariables();
	}

	[Conditional("DEBUG")]
	private void checkCriticalClassVariables()
	{
		if (!Application.isEditor) return;
		if (OkButton == null) Debug.LogWarning("Error window: OkButton not initialized!");
		if (ErrorMessageText == null) Debug.LogWarning("Error window: Error message text not initialized!");
		if (WindowTitleText == null) Debug.LogWarning("Error window: Window title text not initialized!");
		if (MainPanel == null) Debug.LogWarning("Error window: Main panel not initialized!");
	}

	public void DisplayErrorMessage(string message, string title = null)
	{
		ErrorMessageText.text = string.IsNullOrEmpty(message) ? "No message provided!" : message;
		WindowTitleText.text = string.IsNullOrEmpty(title) ? UserInfo.getLocalizedString("Error") : title;
	}

	public void ShutDownApplicationAfterClosing(bool shutDown = true)
	{
		shutDownAfterClosing = shutDown;
		if (shutDown) OkButton.text = UserInfo.getLocalizedString("ExitButton");
	}

	public void DestroyWindow()
	{
		GameObject.Destroy(MainPanel);
		if(shutDownAfterClosing) Application.Quit();
	}
}
