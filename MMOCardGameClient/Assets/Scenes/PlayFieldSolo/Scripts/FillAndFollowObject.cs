﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class FillAndFollowObject : MonoBehaviour
{
	public RectTransform parentObject;
	[SerializeField] private RectTransform childObject;
	
	void Update ()
	{
		var parentPosition = parentObject.position;
		var childPosition = childObject.position;
		var parentSize = parentObject.rect.size;
		var childSize = childObject.rect.size;

		if (parentPosition != childPosition)
			childObject.position = parentPosition;
		if (parentSize != childSize)
			childObject.sizeDelta = parentSize;
	}
}
