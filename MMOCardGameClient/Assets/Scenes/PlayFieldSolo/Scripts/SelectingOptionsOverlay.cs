﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Backend.GameInformation;
using ProtoBuffers;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

class TargetId
{
	public int id;
	public TargetType Type;

	public TargetId(int id, TargetType type)
	{
		this.id = id;
		Type = type;
	}
}

public class SelectingOptionsOverlay : MonoBehaviour
{
	[SerializeField] private GameObject MainPanel;
	[SerializeField] private GameObject BlockingOverlay;
	[SerializeField] private GameObject Contener;
	[SerializeField] private SelectableOption SelectableOptionPrefab;
	[SerializeField] private PlayfieldCardsCollection CardsCollection;
	[SerializeField] private PlayersCollection PlayersCollection;
	private int transactionId;
	private int targetId;
	private TargetList targetList;
	private List<SelectableOption> options;
	private Dictionary<TargetId, IIluminationOverlay> illuminatedTargets;
	private Dictionary<TargetId, bool> illuminatedTargetsSelected;
	private Dictionary<TargetId, UnityAction<IIluminationOverlay>> illuminatedTargetsActions;

	void Start ()
	{
		CallbackListeners.get().PickOptionsEvent+=OnPickOptionsEvent;
		CallbackListeners.get().PhaseEndedEvent+=OnPhaseEndedEvent;
		MainPanel.SetActive(false);
		gameObject.GetComponent<Canvas>().sortingOrder = 1;
	}

	private void OnPhaseEndedEvent(GamePhase oldphase, int oldplayerid, GamePhase newphase, int newplayerid)
	{
		if(MainPanel.activeSelf)
			CancelSelecting();
	}

	private void OnPickOptionsEvent(int transactionId, TargetList targetList, int instanceid)
	{
		if (targetList.Targets.Count == 0)
			return;
		
		this.targetId = instanceid;
		this.targetList = targetList;
		this.transactionId = transactionId;
		
		MainPanel.SetActive(true);
		if (targetList.OptionsType == TargetSelectingTransaction.Types.OptionsType.Option)
			displayOptionsToSelect(targetList);
		else illuminateTargets(targetList);
	}

	private void illuminateTargets(TargetList targetList)
	{
		BlockingOverlay.SetActive(false);
		var illuminatedTargetsLocal=new Dictionary<TargetId, IIluminationOverlay>();
		var illuminatedTargetsSelectedLocal=new Dictionary<TargetId, bool>();
		var illuminatedTargetsActionsLocal=new Dictionary<TargetId, UnityAction<IIluminationOverlay>>();
		for (int i = 0; i < targetList.Targets.Count; i++)
		{
			try
			{
				var target = targetList.Targets[i];
				IIluminationOverlay illuminatedTarget;
				if (target.TargetType == TargetType.Card)
					illuminatedTarget = CardsCollection.getCard(target.Id).GetComponent<IIluminationOverlay>();
				else illuminatedTarget = PlayersCollection.GetPlayer(target.Id).playerIluminationOverlay;

				var targetId = new TargetId(target.Id, target.TargetType);
				illuminatedTargetsSelectedLocal.Add(targetId, false);
				illuminatedTarget.SetColor(1);
				UnityAction<IIluminationOverlay> lambda = arg =>
				{
					illuminatedTargetsSelectedLocal[targetId] = !illuminatedTargetsSelectedLocal[targetId];
					if (illuminatedTargetsSelectedLocal[targetId])
						illuminatedTarget.SetColor(2);
					else illuminatedTarget.SetColor(1);
				};
				illuminatedTargetsActionsLocal.Add(targetId, lambda);
				illuminatedTargetsLocal.Add(targetId, illuminatedTarget);
				illuminatedTarget.GetClickEvent().AddListener(lambda);
			}
			catch (ArgumentException e)
			{
				Debug.LogError("Failed to iluminate object: "+targetList.Targets[i]);
			}

			illuminatedTargets = illuminatedTargetsLocal;
			illuminatedTargetsSelected = illuminatedTargetsSelectedLocal;
			illuminatedTargetsActions = illuminatedTargetsActionsLocal;
		}
	}

	private void displayOptionsToSelect(TargetList targetList)
	{
		BlockingOverlay.SetActive(true);
		options=new List<SelectableOption>();
		for (int i = 0; i < targetList.Targets.Count; i++)
		{
			var option = Instantiate(SelectableOptionPrefab);
			option.SetText(targetList.Targets[i].Description);
			option.SetColor(IluminationColors.colors[0]);
			option.id = targetList.Targets[i].Id;
			option.transform.SetParent(Contener.transform);
			options.Add(option);
		}
	}

	public void EndSelecting()
	{
		if (targetList.OptionsType == TargetSelectingTransaction.Types.OptionsType.Option)
			sendSelectedOptions();
		else sendSelectedTargets();
		MainPanel.SetActive(false);
	}

	public void CancelSelecting()
	{
		GameInfo.BackendController.AbortPlay(transactionId, targetId);
		MainPanel.SetActive(false);
		if (targetList.OptionsType == TargetSelectingTransaction.Types.OptionsType.Option)
			cleanupSelectedOptions();
		else cleanupSelectedTargets();
	}

	private void sendSelectedTargets()
	{
		var selectedTargets = new List<Target>();
		foreach (var target in illuminatedTargetsSelected)
		{
			if (target.Value)
				selectedTargets.Add(new Target { Id = target.Key.id, TargetType = target.Key.Type});
		}
		
		GameInfo.BackendController.SelectOptions(transactionId,targetId,selectedTargets);
		
		cleanupSelectedTargets();
	}

	private void cleanupSelectedTargets()
	{
		foreach (var target in illuminatedTargetsSelected)
		{
			illuminatedTargets[target.Key].Reset();
			illuminatedTargets[target.Key].GetClickEvent().RemoveListener(illuminatedTargetsActions[target.Key]);
		}

		illuminatedTargetsSelected = null;
		illuminatedTargetsActions = null;
		illuminatedTargets = null;
	}

	private void sendSelectedOptions()
	{
		var selectedOptions=new List<Target>();
		foreach (var option in options)
		{
			if(option.Selected)
				selectedOptions.Add(new Target
				{
					Id = option.id
				});
		}
		
		cleanupSelectedOptions();
		GameInfo.BackendController.SelectOptions(transactionId,targetId,selectedOptions);
	}

	private void cleanupSelectedOptions()
	{
		foreach (var option in options)
			Destroy(option.gameObject);

		options = null;
	}
}
