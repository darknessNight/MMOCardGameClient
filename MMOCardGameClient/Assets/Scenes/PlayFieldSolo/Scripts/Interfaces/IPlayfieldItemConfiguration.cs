﻿using UnityEngine;

public interface IPlayfieldItemConfiguration
{
	ScriptableObject Original { get; }
	void setUp(ScriptableObject item);
}
