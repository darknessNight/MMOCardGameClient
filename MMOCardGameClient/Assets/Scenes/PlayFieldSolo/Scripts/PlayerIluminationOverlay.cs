﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerIluminationOverlay : MonoBehaviour, IIluminationOverlay, IPointerClickHandler
{
    public Image overlayBackImage;
    public Image overlayFrontImage;

    public float frontAlpha = 75.0f / 255.0f;
    int currentColor = -1;

    public IluminationClickEvent ClickEvent = new IluminationClickEvent();


    public void SetColor(int i)
    {
        if (i >= IluminationColors.colors.Length)
            throw new ArgumentOutOfRangeException();
        overlayBackImage.color = IluminationColors.colors[i];
        overlayFrontImage.color = IluminationColors.colors[i];
        var color = overlayFrontImage.color;
        color.a = frontAlpha;
        overlayFrontImage.color = color;
        currentColor = i;
    }

    public int GetColor(){
        return currentColor;
    }

    public void Reset()
    {
        overlayBackImage.color = IluminationColors.transparentColor;
        overlayFrontImage.color = IluminationColors.transparentColor;
        currentColor = -1;
    }

    public IluminationClickEvent GetClickEvent()
    {
        return ClickEvent;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        ClickEvent?.Invoke(this);
    }
}
