﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateIsolatedLinkedElement : MonoBehaviour
{
	[SerializeField] private Transform elementsLayer;
	[SerializeField] private GameObject prefab;

	void Start ()
	{
		var element=Instantiate(prefab);
		element.GetComponent<FillAndFollowObject>().parentObject = GetComponent<RectTransform>();
		element.transform.SetParent(elementsLayer);
	}
}
