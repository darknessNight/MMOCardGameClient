﻿using System;
using UnityEngine.Events;

[Serializable]
public class IluminationClickEvent : UnityEvent<IIluminationOverlay>
{
}

public interface IIluminationOverlay
{
    int GetColor();
    void Reset();
    void SetColor(int i);
    IluminationClickEvent GetClickEvent();
}