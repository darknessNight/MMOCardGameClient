﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SelectableOption : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private Text text;
    [SerializeField] private Image Image;
    public int id;
    public bool Selected { get; private set; } = false;
    private Color defaultColor=new Color(255,255,255,255);
    [SerializeField] private Color SelectedColor=new Color(255,0,0,255); 

    public void SetText(string text)
    {
        this.text.text = text;
    }

    public void SetColor(Color color)
    {
        Image.color = color;
        defaultColor = color;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Selected = !Selected;
        if (Selected)
            Image.color = SelectedColor;
        else Image.color = defaultColor;
    }
}
