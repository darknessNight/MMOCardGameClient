﻿using UnityEngine;

public class InitPlayerZone : MonoBehaviour
{
	[SerializeField] private PlayerInfos playerInfos;
	void Start () {
		if (GameInfo.GameState != null)
		{
			playerInfos.playerId = (int) GameInfo.GameState.GetMyUserId();
		}
	}
}
