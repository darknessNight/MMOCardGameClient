﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AddedObjectEvent: UnityEvent<GameObject>{}

public class PlayfieldCardsCollection : MonoBehaviour
{
	public AddedObjectEvent AddedCard=new AddedObjectEvent();
	private Dictionary<int, GameObject> cardsInstances=new Dictionary<int, GameObject>();

	void Start()
	{
		Debug.Log("PlayfieldCardsCollection created");
	}
	
	public GameObject getCard(int id)
	{
		if (!cardsInstances.ContainsKey(id))
		{
			Debug.LogError("Wanted to get card with id: "+id);			
			throw new ArgumentException();
		}
		return cardsInstances[id];
	}

	public void addCard(int id, GameObject card)
	{
		Debug.Log("Added card with id to collection: "+id);
		cardsInstances.Add(id, card);
		AddedCard?.Invoke(card);
	}

	public bool HasCard(int id)
	{
		return cardsInstances.ContainsKey(id);
	}
}
