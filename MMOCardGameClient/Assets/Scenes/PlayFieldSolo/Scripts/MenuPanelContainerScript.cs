﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class MenuPanelContainerScript : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private GameObject panel;
    private bool hide = true;
    private float width;
    public float hiddenSize = 10;
    public float animationSpeed = 2.0f;

    void Start()
    {
        panel = gameObject;
        width = ((RectTransform) panel.transform).rect.width;
        panel.transform.Translate(-(width - hiddenSize),0,0);
    }

    void Update()
    {
        float animationDirection;
        float stopPoint;
        if (hide)
        {
            animationDirection = -1;
            stopPoint = -(width / 2 - hiddenSize);
        }
        else
        {
            animationDirection = 1;
            stopPoint = width / 2 - 1;
        }

        if (animationDirection > 0 && stopPoint > panel.transform.position.x
            || animationDirection < 0 && stopPoint < panel.transform.position.x)
            MovePanelAnimation(stopPoint, animationDirection);
    }

    private void MovePanelAnimation(float stopPoint, float animationDirection)
    {
        float animationMove = animationSpeed * Time.deltaTime;
        float realMove = Math.Min(Math.Abs(stopPoint - panel.transform.position.x), animationMove);
        panel.transform.Translate(animationDirection * realMove, 0, 0);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        hide = false;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        hide = true;
    }
}