﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[Serializable]
public class DropEvent : UnityEvent<GameObject>
{
}

public class DropZone : MonoBehaviour, IDropHandler
{
	public DropEvent Drop=new DropEvent();

	public void OnDrop(PointerEventData eventData)
	{
		Drop?.Invoke(eventData.pointerDrag);
	}
}
