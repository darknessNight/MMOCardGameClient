﻿using UnityEngine;

public class SurrenderButtonScript : MonoBehaviour {

	public void OnButtonClick()
	{
		GameInfo.BackendController?.Surrender();
		Debug.Log("Surrender");
	}
}
