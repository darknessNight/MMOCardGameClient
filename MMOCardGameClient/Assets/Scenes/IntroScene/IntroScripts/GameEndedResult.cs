public enum GameEndedResult
{
    Won,
    Lost
}