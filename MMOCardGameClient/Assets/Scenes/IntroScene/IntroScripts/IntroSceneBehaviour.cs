﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class IntroSceneBehaviour : MonoBehaviour
{
    private float animationSpeed;
    public Text IntroText;
    public Image IntroImage;

    private Color TextColor;
    private float textColorRGB;
    private float timeBeforeSceneChange;

    private void Start()
    {
        animationSpeed = 0.3f;
        timeBeforeSceneChange = 1;
    }

    private void Update()
    {
        changeTextColor();
        checkIfUserWantsToSkip();
        changeSceneWithDelayAfterAnimationFinished();
    }

    private void checkIfUserWantsToSkip()
    {
        if (Input.anyKey) changeScene();
    }

    private void changeTextColor()
    {
        if ((textColorRGB += Time.deltaTime * animationSpeed) < 1)
        {
            var color = new Color(
                textColorRGB, textColorRGB, textColorRGB,
                1.0f
            );
            
            IntroImage.color = color;
            IntroText.color = color;
        }
    }

    private void changeSceneWithDelayAfterAnimationFinished()
    {
        if (textColorRGB > 1.0f)
        {
            timeBeforeSceneChange -= Time.deltaTime;
            if (timeBeforeSceneChange <= 0) changeScene();
        }
    }

    private void changeScene()
    {
        SceneManager.LoadSceneAsync("LoginScreen");
    }
}