﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
using UnityEngine;

public class DeckEditorCardClick : MonoBehaviour, IEditorCardClick
{
	public MinimizedDeckCardDisplay CardDisplay;
	private NewDeckZoneController ZoneController;

	void Start()
	{
		if(ZoneController == null)
			ZoneController = gameObject.GetComponentInParent<NewDeckZoneController>();
		if (CardDisplay == null)
			CardDisplay = gameObject.GetComponent<MinimizedDeckCardDisplay>();
	}
	
	public void RemoveOne()
	{
		ZoneController.RemoveCard(getCardId());
	}

	public void AddOne()
	{
		ZoneController.AddCard(getCardId());
	}

	private int getCardId()
	{
		var cardScriptable = CardDisplay.Original as CardScriptable;
		if (cardScriptable != null && cardScriptable.CardId != null)
			return cardScriptable.CardId ?? -1;	// -1 will never be returned
		throw new Exception("EditorCard " + gameObject.name + " @[" + gameObject.transform.parent.name +
		                    "] has no id!");
	}
}
