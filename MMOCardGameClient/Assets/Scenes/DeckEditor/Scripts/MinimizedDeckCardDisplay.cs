﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.UI;

public class MinimizedDeckCardDisplay : MonoBehaviour, IPlayfieldItemConfiguration, IAmountChanger
{
	public Text AmountText;
	public Text CardNameText;

	private int amountOfCards = 0;
	
	void Start () {
		if(AmountText == null || CardNameText == null)
			Debug.LogError("MinimizedCardDisplay - not all fields have been initialized");
	}
	

	public ScriptableObject Original { get; private set; }
	
	
	public void setUp(ScriptableObject item)
	{
		var card = item as CardScriptable;
		if(card == null)
			throw new Exception("Can't configure item \"" + item.name + "\" - item is not of CardScriptable type!");
		Original = card;

		amountOfCards = card.Amount;
		AmountText.text = card.Amount.ToString();
		CardNameText.text = card.Name;
	}

	public int Increase(int amount = 1)
	{
		AmountText.text = (amountOfCards += amount).ToString();
		return amount;
	}

	public int Decrease(int amount = 1)
	{
		AmountText.text = (amountOfCards -= amount).ToString();
		return amountOfCards;
	}

	public void Set(int amount)
	{
		amountOfCards = amount;
		AmountText.text = amount.ToString();
	}

	public int Get()
	{
		return amountOfCards;
	}
}

public interface IAmountChanger
{
	int Increase(int amount = 1);
	int Decrease(int amount = 1);
	void Set(int amount);
	int Get();
}
