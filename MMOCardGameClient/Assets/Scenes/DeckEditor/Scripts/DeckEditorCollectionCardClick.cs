﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckEditorCollectionCardClick : MonoBehaviour, IEditorCardClick
{
    public CardDisplay CardDisplay;
    private CollectionZoneController ZoneController;

    void Start()
    {
        if(ZoneController == null)
            ZoneController = gameObject.GetComponentInParent<CollectionZoneController>();
        if (CardDisplay == null)
            Debug.LogError("CardDisplay in " + gameObject.name + " in " + gameObject.transform.parent.name + 
                           " is not initialized!");
    }
	
    public void RemoveOne()
    {
        ZoneController.RemoveCard(getCardId());
    }

    public void AddOne()
    {
        ZoneController.AddCard(getCardId());
    }

    private int getCardId()
    {
        var cardScriptable = CardDisplay.Original as CardScriptable;
        if (cardScriptable != null && cardScriptable.CardId != null)
            return cardScriptable.CardId ?? -1;	// -1 will never be returned
        throw new Exception("CollectionCard " + gameObject.name + " @[" + gameObject.transform.parent.name +
                            "] has no id!");
    }
}
