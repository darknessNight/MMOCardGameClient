﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEditorCardClick
{
    void RemoveOne();
    void AddOne();
}
