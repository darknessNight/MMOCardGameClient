﻿using System;
using System.Collections;
using System.Collections.Generic;
using ExtensionMethods;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EditorCollectionDisplay : MonoBehaviour 
{
	public List<CardScriptable> cardsList;
	public Transform contentPanel;
	public GameObject prefabCard;
	public GameObject LoadingPanel;

	public bool DoneLoading { get; private set; } = false;
	
	private bool waitForUserCardCollection = true;

	public Dictionary<int, GameObject> availableCards { get; private set; }

	void Start()
	{
		availableCards = new Dictionary<int, GameObject>();

		this.StartCoroutine(setupAvailableCards(), errorCallback);
	}

	public void errorCallback(Exception e)
	{
		Debug.LogError(e.Message + "\n" + e.StackTrace);
	}

	private IEnumerator setupAvailableCards()
	{
		getUsersCardCollection();
		while (waitForUserCardCollection)
		{
			yield return new WaitForEndOfFrame();
		}
		displayUsersAvailableCollection();
		DoneLoading = true;
	}

	private void displayUsersAvailableCollection()
	{
		foreach (var card in cardsList)
		{
			var newCard = createNewCard(card);
			newCard.GetComponent<CardSelect>()?.DisableSelection();
			newCard.transform.SetParent(contentPanel);
			newCard.SetActive(true);

			availableCards.Add(card.CardId ?? -1, newCard);
		}
	}

	private void getUsersCardCollection()
	{
		var request = UserInfo.LoggedClient.getUserCardCollection();
		request.SuccessCallback = saveCollection;
		request.ErrorCallback = obj =>
		{
			LoadingPanel.GetComponentInChildren<Text>().text =
				UserInfo.getLocalizedString("ErrorWhileDownloadingCollection");
		};
		request.Send();
	}

	private void saveCollection(UserCardCollection[] collection)
	{
		cardsList.Clear();
		cardsList = getAvailableCards(collection);
		waitForUserCardCollection = false;
	}

	private List<CardScriptable> getAvailableCards(UserCardCollection[] cards)
	{
		var list = new List<CardScriptable>();
		foreach (var card in cards)
		{
			var cardScriptable = CardCreator.createNewCard((int) card.cardId);
			cardScriptable.Amount = int.Parse(card.amount);
			if(cardScriptable.Amount > 0)
				list.Add(cardScriptable);
		}

		return list;
	}

	private GameObject createNewCard(CardScriptable cardScriptable)
	{
		var instantiatedPrefabCard = Instantiate(prefabCard);
		tryToConfigureCard(cardScriptable, instantiatedPrefabCard);
		return instantiatedPrefabCard;
	}

	private static void tryToConfigureCard(CardScriptable cardScriptable, GameObject instantiatedPrefabCard)
	{
		try
		{
			instantiatedPrefabCard.GetComponent<CardDisplay>().setUp(cardScriptable);
		}
		catch (Exception e)
		{
			var errorMessage = string.Concat("Unable to configure card with id: ", cardScriptable.InstanceId);
			Debug.LogWarning(errorMessage);
			CallbackListeners.get().OnClientSevereExceptionEvent(errorMessage, 
				"CardCollectionDisplay, tryToConfigureCard()");
		}
	}
}
