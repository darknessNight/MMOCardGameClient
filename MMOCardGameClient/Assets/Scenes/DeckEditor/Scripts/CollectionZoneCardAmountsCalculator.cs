﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionZoneCardAmountsCalculator : MonoBehaviour
{
	public EditorCollectionDisplay EditorCollection;
	public LoadDeckFromServerScript DeckCollection;
	public RectTransform CollectionZone;
	public RectTransform DeckCollectionZone;

	private bool amountsSet = false;
	
	void Start () {
		if (DeckCollection == null || EditorCollection == null || DeckCollectionZone == null)
		{
			Debug.LogError("CollectionZoneCardAmountsCalculator in " + gameObject.GetComponentInParent<RectTransform>().name + 
			               " is not properly initialized!");
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (amountsSet || !EditorCollection.DoneLoading || !DeckCollection.DoneLoading) return;
		foreach (RectTransform obj in DeckCollectionZone)
		{
			var cardObjectAmount = obj.GetComponent<IAmountChanger>();
			var cardObjectDisplay = obj.GetComponent<IPlayfieldItemConfiguration>();
			var cardScriptable = cardObjectDisplay.Original as CardScriptable;
			if(cardScriptable != null && !EditorCollection.availableCards.ContainsKey(cardScriptable.CardId ?? -1))
				continue;
			EditorCollection.availableCards[cardScriptable.CardId ?? -1].GetComponent<IAmountChanger>()
				.Decrease(cardObjectAmount.Get());
		}

		amountsSet = true;
	}
}
