﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadDeckFromServerScript : MonoBehaviour
{
	public NewDeckZoneController NewDeckZoneController;
	public CollectionZoneController CollectionZoneController;
	public GameObject LoadingPanel;
	
	public bool DoneLoading { get; private set; }
	public Dictionary<int, int> DeckOnServer { get; private set; } = null;
	
	private bool waitForUserCardCollection = true;
	
	// Use this for initialization
	void Start ()
	{
		StartCoroutine(getCardsFromDeckToEditAsync());
	}

	private IEnumerator getCardsFromDeckToEditAsync()
	{
		getCardsFromDeck();
		while (waitForUserCardCollection)
		{
			yield return new WaitForEndOfFrame();
		}

		DoneLoading = true;
	}

	private void getCardsFromDeck()
	{
		var request = UserInfo.LoggedClient.getCardsInDeck(UserInfo.DeckToEdit);
		request.SuccessCallback = getDeck;
		request.ErrorCallback = obj =>
		{
			foreach (var error in obj)
			{
				Debug.LogError(error);
			}
			StopAllCoroutines();
		};
		request.Send();
	}

	private void getDeck(UserCardCollection[] deck)
	{
		DeckOnServer = new Dictionary<int, int>();
		foreach (var card in deck)
		{
			var amount = int.Parse(card.amount);
			if(amount <= 0) 
				continue;
			var cardId = (int) card.cardId;
			NewDeckZoneController.AddCard(cardId, amount);
			DeckOnServer.Add(cardId, amount);
			CollectionZoneController.RemoveCard(cardId, amount);
		}
		waitForUserCardCollection = false;
	}
	
	
	
}
