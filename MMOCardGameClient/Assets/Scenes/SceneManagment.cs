﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagment : MonoBehaviour
{
    private Scenes currentScene;

    void Start()
    {
        DontDestroyOnLoad(this);
        //UserInfo.SceneManagment = this;
    }


    public void changeScene(Scenes scene)
    {
        if (sceneNotLoaded(scene))
        {
            switch (scene)
            {
                case Scenes.LOGIN_SCREEN:
                    SceneManager.LoadScene("Scenes/LoginScreen/LoginScreen");
                    break;
                case Scenes.MAIN_MENU:
                    SceneManager.LoadScene("Scenes/MainMenu/MenuScreen");
                    break;
                case Scenes.CARD_COLLECTION:
                    SceneManager.LoadScene("Scenes/CardCollection/CardCollection");
                    break;
                case Scenes.GAME_MENU:
                    SceneManager.LoadScene("Scenes/GameMenu/GameMenu");
                    break;
                case Scenes.PLAY_FIELD:
                    SceneManager.LoadScene("PlayFieldSolo");
                    break;
            }

            currentScene = scene;
        }
    }

    private bool sceneNotLoaded(Scenes scene)
    {
        return !scene.Equals(currentScene) &&
               !SceneManager.GetActiveScene().name.Contains(SceneNames.getSceneName(scene));
    }
}