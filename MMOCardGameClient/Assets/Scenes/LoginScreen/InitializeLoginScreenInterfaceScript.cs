﻿using UnityEngine;

public class InitializeLoginScreenInterfaceScript : MonoBehaviour
{
    public LoginScreenComponentsDataStructure LoginScreenComponents;
    public SignUpFormComponentsDataStructure SignUpFormComponents;

    private void Start()
    {
        initializeLoginPanel();
        initializeSignUpPanel();
    }

    private void initializeLoginPanel()
    {
        LoginScreenComponents.LoadingCircleCanvas.enabled = false;
    }

    private void initializeSignUpPanel()
    {
        SignUpFormComponents.LoadingCircleCanvas.enabled = false;

        SignUpFormComponents.SuccessMessagePanel.SetActive(false);
        SignUpFormComponents.SignUpFormPanel.SetActive(false);

        SignUpFormComponents.SignUpButton.interactable = false;
        SignUpFormComponents.SuccessMessagePanel.SetActive(false);
    }
}