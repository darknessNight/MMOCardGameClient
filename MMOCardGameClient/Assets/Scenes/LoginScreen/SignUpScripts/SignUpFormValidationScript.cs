﻿using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class SignUpFormValidationScript : MonoBehaviour
{
    private readonly Color errorColor = Color.red;
    private readonly Color okColor = Color.green;

    private bool emailIsValid;

    private Regex EmailValidationRegex;
    private bool loginIsValid;
    private Regex LoginValidationRegex;
    private bool passwordIsValid;
    private bool passwordsMatch;
    private Regex PasswordValidationRegex;

    public SignUpFormComponentsDataStructure SignUpFormComponents;

    private void Start()
    {
        resetValues();
        LoginValidationRegex = new Regex(@"^[A-Za-z0-9]{3,20}$");
        PasswordValidationRegex = new Regex(@"^(?=\S+$).[A-Za-z0-9@#$%^&+=]{7,35}$");
        EmailValidationRegex = new Regex(@"^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}$");
    }

    public bool validateFormData()
    {
        validateEmail();
        validateLogin();
        validatePassword();
        checkIfPasswordsMatch();

        return emailIsValid && loginIsValid && passwordIsValid && passwordsMatch;
    }

    private void resetValues()
    {
        emailIsValid = false;
        loginIsValid = false;
        passwordIsValid = false;
        passwordsMatch = false;
    }

    public void validateEmail()
    {
        if (!EmailValidationRegex.IsMatch(SignUpFormComponents.EmailInputField.text))
        {
            setText(SignUpFormComponents.EmailInfoText,
                UserInfo.getLocalizedString("InvalidEmail"), errorColor);
            emailIsValid = false;
        }
        else
        {
            setText(SignUpFormComponents.EmailInfoText,
                UserInfo.getLocalizedString("ValidInformation"), okColor);
            emailIsValid = true;
        }
    }

    public void validateLogin()
    {
        if (!LoginValidationRegex.IsMatch(SignUpFormComponents.UserNameInputfield.text))
        {
            setText(SignUpFormComponents.UserNameInfoText,
                UserInfo.getLocalizedString("InvalidLogin"), errorColor);
            loginIsValid = false;
        }
        else
        {
            setText(SignUpFormComponents.UserNameInfoText,
                UserInfo.getLocalizedString("ValidInformation"), okColor);
            loginIsValid = true;
        }
    }

    public void validatePassword()
    {
        if (!PasswordValidationRegex.IsMatch(SignUpFormComponents.PasswordInputField.text))
        {
            setText(SignUpFormComponents.PasswordInfoText,
                UserInfo.getLocalizedString("InvalidPassword"), errorColor);
            passwordIsValid = false;
        }
        else
        {
            setText(SignUpFormComponents.PasswordInfoText,
                UserInfo.getLocalizedString("ValidInformation"), okColor);
            passwordIsValid = true;
        }
    }

    public void checkIfPasswordsMatch()
    {
        if (!SignUpFormComponents.PasswordInputField.text.Equals(SignUpFormComponents.RepeatPasswordInputfield.text) ||
            SignUpFormComponents.PasswordInputField.text.Equals(string.Empty))
        {
            setText(SignUpFormComponents.RepeatedPasswordInfoText,
                UserInfo.getLocalizedString("PasswordsDontMatch"), errorColor);
            passwordsMatch = false;
        }
        else
        {
            setText(SignUpFormComponents.RepeatedPasswordInfoText,
                UserInfo.getLocalizedString("ValidInformation"), okColor);
            passwordsMatch = true;
        }
    }

    private void setText(Text textBox, string text, Color color)
    {
        textBox.text = text;
        textBox.color = color;
    }
}