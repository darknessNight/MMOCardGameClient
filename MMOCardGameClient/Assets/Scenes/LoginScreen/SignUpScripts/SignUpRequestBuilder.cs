﻿using System.Text;
using UnityEngine.Networking;

public class SignUpRequestBuilder : RestRequestBuilder
{
    public new UnityWebRequest WebRequest => base.WebRequest;

    private SignUpData data;

    public SignUpRequestBuilder(string email, string userName, string password, string repeatedPassword) :
        base(UnityWebRequest.kHttpVerbPOST)
    {
        data = new SignUpData()
        {
            email = email,
            password = password,
            repeatedPassword = repeatedPassword,
            login = userName
        };

        setUpRequest();
    }

    public SignUpRequestBuilder(SignUpData signUpData) : base(UnityWebRequest.kHttpVerbPOST)
    {
        data = signUpData;
        setUpRequest();
    }

    private void setUpRequest()
    {
        WebRequest.url = UserInfo.DataServerUrlUsers;
        addUploadHandler(new SignUpDataSerialization(data).SignUpDataJsonString);
        addDownloadHandler();
    }

    


}