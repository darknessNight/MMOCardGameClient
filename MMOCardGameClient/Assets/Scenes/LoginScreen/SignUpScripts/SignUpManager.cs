﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class SignUpManager : MonoBehaviour
{
    private bool buttonsLocked;
    public SignUpFormComponentsDataStructure SignUpFormComponents;
    public SignUpFormValidationScript SignUpFormValidation;

    private void Start()
    {
        buttonsLocked = false;
    }

    private void Update()
    {
        if(SignUpFormComponents.SignUpFormPanel.activeInHierarchy)
            checkIfSignUpButtonCanBeEnabled();
    }

    private void checkIfSignUpButtonCanBeEnabled()
    {
        var dataIsValid = SignUpFormValidation.validateFormData();
        if (dataIsValid && !SignUpFormComponents.SignUpButton.interactable && !buttonsLocked)
            SignUpFormComponents.SignUpButton.interactable = true;
        else if (!dataIsValid && SignUpFormComponents.SignUpButton.interactable)
            SignUpFormComponents.SignUpButton.interactable = false;
    }

    public void signUp()
    {
        disableButtons();
        SignUpFormComponents.LoadingCircleCanvas.enabled = true;
        if (SignUpFormValidation.validateFormData())
        {
            StartCoroutine(sendSignUpDataAsync());
        }
        else
        {
            SignUpFormComponents.TitleText.text = UserInfo.getLocalizedString("SignUpError");
            SignUpFormComponents.TitleText.color = Color.red;
            enableButtons();
        }
    }

    public void goBackToLoginScreen()
    {
        //UserInfo.SceneManagment.changeScene(Scenes.LOGIN_SCREEN);
        SceneManager.LoadSceneAsync("LoginScreen");
    }

    private void disableButtons()
    {
        SignUpFormComponents.CancelButton.interactable = false;
        SignUpFormComponents.SignUpButton.interactable = false;
        buttonsLocked = true;
    }

    private IEnumerator sendSignUpDataAsync()
    {
        var signUpRequestBuilder = new SignUpRequestBuilder(getSignUpData());
        yield return signUpRequestBuilder.WebRequest.SendWebRequest();

        var response = getResponse(signUpRequestBuilder.WebRequest);
        response.getMessageForUser(ref SignUpFormComponents.TitleText);
        SignUpFormComponents.ServerResponseText.text = removeJsonChars(response.DownloadedText);//Temporary solution
        enableButtons();
    }

    private DataServerResponse getResponse(UnityWebRequest webRequest)
    {
        return new DataServerResponse.DataServerResponseBuilder(webRequest, "SignUp Request")
            .addResponseCodeMessage(HttpCode.Created, UserInfo.getLocalizedString("SignUpSucceeded"), true)
            .addResponseCodeMessage(HttpCode.BadRequest, UserInfo.getLocalizedString("SignUpError"), false)
            .build();
    }

    private string removeJsonChars(string jsonString)//Temporary solution
    {
        return jsonString
            .Replace("\"", string.Empty)
            .Replace("[", string.Empty)
            .Replace("]", string.Empty)
            .Replace(",", string.Empty);
    }

    private SignUpData getSignUpData()
    {
        return new SignUpData()
        {
            email = SignUpFormComponents.EmailInputField.text,
            login = SignUpFormComponents.UserNameInputfield.text,
            password = SignUpFormComponents.PasswordInputField.text,
            repeatedPassword = SignUpFormComponents.RepeatPasswordInputfield.text
        };
    }

    private void enableButtons()
    {
        SignUpFormComponents.CancelButton.interactable = true;
        SignUpFormComponents.SignUpButton.interactable = true;
        buttonsLocked = false;
        SignUpFormComponents.LoadingCircleCanvas.enabled = false;
    }
}