﻿using Newtonsoft.Json;

public class SignUpDataSerialization
{
    public string SignUpDataJsonString { get; }

    public SignUpDataSerialization(string email, string userName, string password, string repeatedPassword)
    {
        SignUpDataJsonString = JsonConvert.SerializeObject(new SignUpData()
        {
            email = email,
            login = userName,
            password = password,
            repeatedPassword = repeatedPassword
        });
    }

    public SignUpDataSerialization(SignUpData data)
    {
        SignUpDataJsonString = JsonConvert.SerializeObject(data);
    }
}

public class SignUpData
{
    public string email;
    public string login;
    public string password;
    public string repeatedPassword;
}
