﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FormSelectableBehavior : MonoBehaviour
{
    private static bool TabIsPressed => Input.GetKeyDown(KeyCode.Tab);

    private static bool LeftOrRightShitfPressed => Input.GetKey(KeyCode.LeftShift) || 
                                                   Input.GetKey(KeyCode.RightShift);

    private EventSystem eventSystem;

    void Start()
    {
        this.eventSystem = EventSystem.current;
    }

    void Update()
    {
        if (!TabIsPressed) return;
            if (selectedObjectIsValidAndActive())
                selectNext();
    }

    private bool selectedObjectIsValidAndActive()
    {
        return eventSystem.currentSelectedGameObject != null &&
               eventSystem.currentSelectedGameObject.activeInHierarchy;
    }

    private void selectNext()
    {
        Selectable nextSelectable = null;
        var currentSelectable = eventSystem.currentSelectedGameObject.GetComponent<Selectable>();
        if (currentSelectable != null)
        {
            nextSelectable = LeftOrRightShitfPressed ? selectNextSelectable(currentSelectable) : 
                selectPreviousSelectable(currentSelectable);
        }
        else if (selectablesExist()) nextSelectable = Selectable.allSelectables[0];

        if (nextSelectable != null) nextSelectable.Select();
    }

    private static Selectable selectNextSelectable(Selectable currentSelectable)
    {
        var nextSelectable = currentSelectable.FindSelectableOnLeft();
        if (nextSelectable == null)
            nextSelectable = currentSelectable.FindSelectableOnUp();
        return nextSelectable;
    }

    private static Selectable selectPreviousSelectable(Selectable currentSelectable)
    {
        var nextSelectable = currentSelectable.FindSelectableOnRight();
        if (nextSelectable == null)
            nextSelectable = currentSelectable.FindSelectableOnDown();
        return nextSelectable;
    }

    private static bool selectablesExist()
    {
        return Selectable.allSelectables.Count > 0;
    }

   
}
