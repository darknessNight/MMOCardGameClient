﻿using UnityEngine;

public class BasicMethods : MonoBehaviour
{
    public LoginScreenComponentsDataStructure LoginScreenComponents;
    public ResetPasswordComponentsDataStructure ResetPasswordComponents;
    public SignUpFormComponentsDataStructure SignUpFormComponents;

    public void ExitGame()
    {
        Application.Quit();
    }

    public void displaySignUpForm()
    {
        if (signUpFormIsOpen())
        {
            SignUpFormComponents.SignUpFormPanel.SetActive(false);
            LoginScreenComponents.LoginPanel.SetActive(true);
        }
        else
        {
            SignUpFormComponents.SignUpFormPanel.SetActive(true);
            LoginScreenComponents.LoginPanel.SetActive(false);
            clearSignUpForm();
        }

        if (signUpSuccessMessageIsOpen())
        {
            SignUpFormComponents.SignUpFormPanel.SetActive(false);
            SignUpFormComponents.SuccessMessagePanel.SetActive(false);
        }
    }

    private bool signUpFormIsOpen()
    {
        return SignUpFormComponents.SignUpFormPanel.activeInHierarchy;
    }

    private bool signUpSuccessMessageIsOpen()
    {
        return SignUpFormComponents.SuccessMessagePanel.activeInHierarchy;
    }

    private void clearSignUpForm()
    {
        SignUpFormComponents.EmailInputField.text = string.Empty;
        SignUpFormComponents.UserNameInputfield.text = string.Empty;
        SignUpFormComponents.PasswordInputField.text = string.Empty;
        SignUpFormComponents.RepeatPasswordInputfield.text = string.Empty;

        SignUpFormComponents.EmailInfoText.text = string.Empty;
        SignUpFormComponents.UserNameInfoText.text = string.Empty;
        SignUpFormComponents.PasswordInfoText.text = string.Empty;
        SignUpFormComponents.RepeatedPasswordInfoText.text = string.Empty;

        SignUpFormComponents.ServerResponseText.text = string.Empty;

        SignUpFormComponents.TitleText.text =
            UserInfo.getLocalizedString("SignUpTitleText");
        SignUpFormComponents.TitleText.color = Color.white;
    }

    public void displayResetPasswordForm()
    {
        if (resetPasswordFormIsOpen())
        {
            ResetPasswordComponents.ResetPasswordWindowPanel.SetActive(false);
            LoginScreenComponents.LoginPanel.SetActive(true);
            clearResetPasswordForm();
        }
        else
        {
            ResetPasswordComponents.ResetPasswordWindowPanel.SetActive(true);
            LoginScreenComponents.LoginPanel.SetActive(false);
        }
    }

    private bool resetPasswordFormIsOpen()
    {
        return ResetPasswordComponents.ResetPasswordWindowPanel.activeInHierarchy;
    }

    private void clearResetPasswordForm()
    {
        ResetPasswordComponents.TitleText.text =
            UserInfo.getLocalizedString("ResetPasswordTitle");
        ResetPasswordComponents.TitleText.color = Color.white;

        ResetPasswordComponents.LoginInputField.text = LoginScreenComponents.LoginInputField.text;
        ResetPasswordComponents.LoginInputField.interactable = true;
        ResetPasswordComponents.ResetTokenInputField.text = string.Empty;
        ResetPasswordComponents.NewPasswordInputField.text = string.Empty;
        ResetPasswordComponents.RepeatNewPasswordInputField.text = string.Empty;

        ResetPasswordComponents.LoginInfoText.text = string.Empty;
        ResetPasswordComponents.ResetTokenInfoText.text = string.Empty;
        ResetPasswordComponents.NewPasswordInfoText.text = string.Empty;
        ResetPasswordComponents.RepeatNewPasswordInfoText.text = string.Empty;

        ResetPasswordComponents.resetTokenReceived = false;
        ResetPasswordComponents.resetSuccessful = false;
    }
}