﻿using UnityEngine;
using UnityEngine.UI;

public class LoginScreenComponentsDataStructure : MonoBehaviour
{
    public Canvas LoadingCircleCanvas;
    public InputField LoginInputField;

    public GameObject LoginPanel;
    public MenuButtonControlScript MenuButtons;
    public InputField PasswordInputField;
    public MenuInfoTextScript UserInfoText;
}