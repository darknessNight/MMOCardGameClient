﻿using UnityEngine;
using UnityEngine.UI;

public class LoginScreenLoadingCircleBehaviour : MonoBehaviour
{
    private readonly float rotateSpeed = 200f;
    private Image image;
    private RectTransform rectComponent;
    private float sinePos;
    public float sineSpeed = 1.6f;


    private void Start()
    {
        rectComponent = GetComponent<RectTransform>();
        image = GetComponent<Image>();
    }

    private void Update()
    {
        if (enabled)
        {
            rectComponent.Rotate(0f, 0f, rotateSpeed * Time.deltaTime);
            image.fillAmount = (Mathf.Sin(sinePos += sineSpeed * Time.deltaTime) + 1) / 2;
        }
    }
}