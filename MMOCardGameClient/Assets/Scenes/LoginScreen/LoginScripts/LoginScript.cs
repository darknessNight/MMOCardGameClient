﻿using System;
using System.Collections;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using ExtensionMethods;
using DataClient;

public class LoginScript : MonoBehaviour
{
    public LoginScreenComponentsDataStructure LoginScreenComponents;

    private void displayExceptionMessage(Exception e)
    {
        LoginScreenComponents.UserInfoText.menuInfoText.text = e.Message;
        LoginScreenComponents.UserInfoText.menuInfoText.color = Color.red;
    }

    public void login()
    {
        prepareScreenForLogin();
        getLoginData();
        if (areLoginCredentialsCorrect())
            startLoginRequest();
        else
            displayErrorAndRevertScreenState();
    }

    private void startLoginRequest()
    {
        var accessClient = new HttpAccessClient();
        var request = accessClient.login(UserInfo.UserName, UserInfo.Password);
        request.SuccessCallback += (DataClient.ILoggedClient obj) =>
        {
            LoginScreenComponents.UserInfoText.menuInfoText.text = "Success";
            safeTokenAndLoadMainMenuIfLoginSucceeded(obj);
            revertScreenState();
        };
        request.ErrorCallback += (obj) =>
        {
            bool loginError = false;
            foreach (var error in obj)
            {
                Debug.LogError(error);
                if (error.Equals("Unauthorized"))
                    loginError = true;
            }

            if(loginError)
                LoginScreenComponents.UserInfoText.menuInfoText.text = UserInfo.getLocalizedString("LoginError");
            else LoginScreenComponents.UserInfoText.menuInfoText.text = UserInfo.getLocalizedString("UnknownError");
            LoginScreenComponents.UserInfoText.menuInfoText.color = Color.red;
            revertScreenState();
        };
        request.Send();
    }

    private void displayErrorAndRevertScreenState()
    {
        LoginScreenComponents.UserInfoText.menuInfoText.text = LoginCredentialsValidator.getValidationError();
        LoginScreenComponents.UserInfoText.menuInfoText.color = Color.red;
        revertScreenState();
    }

    private void safeTokenAndLoadMainMenuIfLoginSucceeded(ILoggedClient loggedClient)
    {
        UserInfo.LoggedClient = loggedClient;
        SceneManager.LoadScene("MenuScreen");
    }


    private void prepareScreenForLogin()
    {
        disableMenuControls();
        LoginScreenComponents.LoadingCircleCanvas.enabled = true;
    }

    private void disableMenuControls()
    {
        LoginScreenComponents.LoginInputField.interactable = false;
        LoginScreenComponents.PasswordInputField.interactable = false;
        LoginScreenComponents.MenuButtons.disableLoginButton();
        LoginScreenComponents.MenuButtons.disableRegisterButton();
    }

    private bool areLoginCredentialsCorrect()
    {
        return LoginCredentialsValidator.validateLoginData(
            UserInfo.UserName,
            UserInfo.Password
        );
    }

    private void getLoginData()
    {
        UserInfo.UserName =
            LoginScreenComponents.LoginInputField.text;
        UserInfo.Password =
            LoginScreenComponents.PasswordInputField.text;
    }

    private void revertScreenState()
    {
        enableMenuControls();
        LoginScreenComponents.LoadingCircleCanvas.enabled = false;
    }

    private void enableMenuControls()
    {
        LoginScreenComponents.LoginInputField.interactable = true;
        LoginScreenComponents.PasswordInputField.interactable = true;
        LoginScreenComponents.MenuButtons.enableLoginButton();
        LoginScreenComponents.MenuButtons.enableRegisterButton();
    }
}
