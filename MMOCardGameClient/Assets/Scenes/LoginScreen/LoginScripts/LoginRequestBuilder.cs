﻿using UnityEngine.Networking;

public class LoginRequestBuilder : RestRequestBuilder
{
    public LoginRequestBuilder(string login, string password) : 
        base(UnityWebRequest.kHttpVerbPOST)
    {
        WebRequest.url = UserInfo.DataServerUrlAuthenticated;
        WebRequest.SetRequestHeader("Authorization",
            new LoginDataConverterScript(login, password).convertLoginDataToBase64LoginString());
        addDownloadHandler();
    }
}
