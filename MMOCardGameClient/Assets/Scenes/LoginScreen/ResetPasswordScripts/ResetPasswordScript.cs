﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class ResetPasswordScript : MonoBehaviour
{
    private bool buttonsLocked;
    public ResetPasswordComponentsDataStructure ResetPasswordComponents;

    private ResetPasswordFormValidator validator;

    private void Start()
    {
        validator = new ResetPasswordFormValidator(ResetPasswordComponents);
        buttonsLocked = false;
    }

    private void Update()
    {
        checkIfInputFieldsCanBeEnabled();
        checkIfResetButtonCenBeEnabled();
    }

    private void checkIfInputFieldsCanBeEnabled()
    {
        if (tokenReceivedAndButtonsDisabled())
            ResetPasswordComponents.enableNewPasswordInputFileds();
        else if (resetRequestSentAndButtonsActive())
            ResetPasswordComponents.disableNewPasswordInputFileds();
    }

    private bool tokenReceivedAndButtonsDisabled()
    {
        return !ResetPasswordComponents.newPasswordInputFieldsAreEnabled() &&
               ResetPasswordComponents.resetTokenReceived;
    }

    private bool resetRequestSentAndButtonsActive()
    {
        return ResetPasswordComponents.newPasswordInputFieldsAreEnabled() &&
               ResetPasswordComponents.resetSuccessful;
    }

    private void checkIfResetButtonCenBeEnabled()
    {
        if (resetPasswordButtonIsDisabledAndFormDataIsValid())
            ResetPasswordComponents.ResetPasswordButton.interactable = true;
        else if (ResetPasswordComponents.resetSuccessful || buttonsLocked)
            ResetPasswordComponents.ResetPasswordButton.interactable = false;
    }

    private bool resetPasswordButtonIsDisabledAndFormDataIsValid()
    {
        return validator.validateResetFormWithoutLogin() &&
               !ResetPasswordComponents.ResetPasswordButton.interactable &&
               !ResetPasswordComponents.resetSuccessful &&
               !buttonsLocked;
    }

    public void sendNewPasswordToServer()
    {
        disableButtons();
        if (validator.validateResetFormWithoutLogin())
            StartCoroutine(sendNewPasswordToServerAsync());
        else
            enableButtons();
    }

    private void disableButtons()
    {
        ResetPasswordComponents.ResetPasswordButton.interactable = false;
        ResetPasswordComponents.CancelButton.interactable = false;
        buttonsLocked = true;
    }

    private IEnumerator sendNewPasswordToServerAsync()
    {
        var resetPasswordRequestBuilder = new ResetPasswordRequestBuilder(
            ResetPasswordComponents.ResetTokenInputField.text,
            ResetPasswordComponents.NewPasswordInputField.text,
            ResetPasswordComponents.RepeatNewPasswordInputField.text);
        yield return resetPasswordRequestBuilder.WebRequest.SendWebRequest();

        setStatusToSucessAndDisplayMessage(getResponse(resetPasswordRequestBuilder.WebRequest));
        enableButtons();
    }

    private DataServerResponse getResponse(UnityWebRequest webRequest)
    {
        return new DataServerResponse.DataServerResponseBuilder(webRequest, "Reset Password Request")
            .addResponseCodeMessage(HttpCode.Ok, UserInfo.getLocalizedString("PasswordChanged"), true)
            .addResponseCodeMessage(HttpCode.NotFound, string.Empty, false)
            .addResponseCodeMessage(HttpCode.Forbidden, UserInfo.getLocalizedString("UserDoesNotExist"), false)
            .build();
    }

    private void setStatusToSucessAndDisplayMessage(DataServerResponse responseBase)
    {
        responseBase.getMessageForUser(ref ResetPasswordComponents.ResetProcedureStatusText);
        if (!responseBase.Success) return;
        ResetPasswordComponents.resetTokenReceived = false;
        ResetPasswordComponents.resetSuccessful = true;
    }

    private void enableButtons()
    {
        ResetPasswordComponents.ResetPasswordButton.interactable = true;
        ResetPasswordComponents.CancelButton.interactable = true;
        buttonsLocked = false;
    }
}