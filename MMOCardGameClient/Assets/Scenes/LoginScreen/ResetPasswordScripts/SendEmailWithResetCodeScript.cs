﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class SendEmailWithResetCodeScript : MonoBehaviour
{
    private bool buttonsLocked;
    public ResetPasswordComponentsDataStructure ResetPasswordComponents;

    private ResetPasswordFormValidator validator;

    private void Start()
    {
        validator = new ResetPasswordFormValidator(ResetPasswordComponents);
        buttonsLocked = false;
    }

    private void Update()
    {
        checkIfResetPasswordButtonCanBeAnabled();
    }

    private void checkIfResetPasswordButtonCanBeAnabled()
    {
        if (loginIsValidAndNotSentAndButtonIsntInteracable())
            ResetPasswordComponents.SendLoginForResetButton.interactable = true;
        else if (!validator.validateLogin() && ResetPasswordComponents.SendLoginForResetButton.interactable)
            ResetPasswordComponents.SendLoginForResetButton.interactable = false;
        else if (ResetPasswordComponents.resetTokenReceived)
            ResetPasswordComponents.SendLoginForResetButton.interactable = false;
    }

    private bool loginIsValidAndNotSentAndButtonIsntInteracable()
    {
        return validator.validateLogin() &&
               !ResetPasswordComponents.SendLoginForResetButton.interactable &&
               !buttonsLocked &&
               !ResetPasswordComponents.resetTokenReceived;
    }

    public void sendResetCodeRequest()
    {
        disableButtons();
        ResetPasswordComponents.LoadingCircle.SetActive(true);
        if (validator.validateLogin())
            StartCoroutine(sendGetResetCodeRequestAsync());
        else
            enableButtons();
    }

    private void disableButtons()
    {
        ResetPasswordComponents.CancelButton.interactable = false;
        ResetPasswordComponents.SendLoginForResetButton.interactable = false;
        buttonsLocked = true;
    }

    private IEnumerator sendGetResetCodeRequestAsync()
    {
        var resetTokenRequestBuilder = new ResetTokenRequestBuilder(
            ResetPasswordComponents.LoginInputField.text);
        yield return resetTokenRequestBuilder.WebRequest.SendWebRequest();

        setStatusToSuccessAndDisplayMessage(getResponse(resetTokenRequestBuilder.WebRequest));
        enableButtons();
    }

    private DataServerResponse getResponse(UnityWebRequest webRequest)
    {
        return new DataServerResponse.DataServerResponseBuilder(webRequest, "SendTokenRequest")
            .addResponseCodeMessage(HttpCode.Ok, UserInfo.getLocalizedString("ResetTokenSent"), true)
            .addResponseCodeMessage(HttpCode.Forbidden, UserInfo.getLocalizedString("UserDoesNotExist"), false)
            .addResponseCodeMessage(HttpCode.NotFound, UserInfo.getLocalizedString("UserDoesNotExist"), false)
            .build();
    }

    private void setStatusToSuccessAndDisplayMessage(DataServerResponse response)
    {
        response.getMessageForUser(ref ResetPasswordComponents.ResetProcedureStatusText);
        if (!response.Success) return;
        ResetPasswordComponents.resetTokenReceived = true;
        ResetPasswordComponents.resetSuccessful = false;
    }

    private void enableButtons()
    {
        ResetPasswordComponents.CancelButton.interactable = true;
        ResetPasswordComponents.SendLoginForResetButton.interactable = true;
        buttonsLocked = false;
        ResetPasswordComponents.LoadingCircle.SetActive(false);
    }
}