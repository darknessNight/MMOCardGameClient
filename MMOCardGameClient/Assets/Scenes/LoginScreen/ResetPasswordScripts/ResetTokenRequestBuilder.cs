﻿using System.Collections;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class ResetTokenRequestBuilder : RestRequestBuilder
{
    public new UnityWebRequest WebRequest => base.WebRequest;
 
    public ResetTokenRequestBuilder(string userName) : base(UnityWebRequest.kHttpVerbPOST)
    {
        var adressBuilder = buildDataServerUserUrl(userName);
        adressBuilder.Append(UserInfo.ResetPasswordUrlTail);
        WebRequest.url = adressBuilder.ToString();
    }
}