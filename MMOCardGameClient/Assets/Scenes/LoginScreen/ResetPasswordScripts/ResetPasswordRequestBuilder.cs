﻿using System.Text;
using UnityEngine.Networking;

public class ResetPasswordRequestBuilder : RestRequestBuilder
{
    public ResetPasswordRequestBuilder(string token, string newPassword, string repeatedNewPassword) : 
        base(UnityWebRequest.kHttpVerbPUT)
    {
        WebRequest.url = buildAdress();
        addUploadHandler(new ResetPasswordJsonSerialization(token, newPassword, repeatedNewPassword).serialize());
    }

    public ResetPasswordRequestBuilder(ResetJsonBody data, string requestMethod) : base(requestMethod)
    {
        WebRequest.url = buildAdress();
        addUploadHandler(new ResetPasswordJsonSerialization(data).serialize());
    }



    private string buildAdress()
    {
        var adressBuilder = buildDataServerUserUrl();
        adressBuilder.Append(UserInfo.ResetPasswordUrlTail);
        return adressBuilder.ToString();
    }
}