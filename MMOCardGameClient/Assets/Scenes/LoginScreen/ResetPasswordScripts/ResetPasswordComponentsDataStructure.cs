﻿using UnityEngine;
using UnityEngine.UI;

public class ResetPasswordComponentsDataStructure : MonoBehaviour
{
    public Button CancelButton;
    public GameObject LoadingCircle;
    public Text LoginInfoText;

    public InputField LoginInputField;
    public Text NewPasswordInfoText;
    public InputField NewPasswordInputField;
    public Text RepeatNewPasswordInfoText;
    public InputField RepeatNewPasswordInputField;
    public Button ResetPasswordButton;

    public GameObject ResetPasswordWindowPanel;
    public Text ResetProcedureStatusText;
    public bool resetSuccessful;
    public Text ResetTokenInfoText;
    public InputField ResetTokenInputField;


    public bool resetTokenReceived;

    public Button SendLoginForResetButton;

    public Text TitleText;

    private void Start()
    {
    }

    private void Update()
    {
    }


    public void enableNewPasswordInputFileds()
    {
        ResetTokenInputField.interactable = true;
        NewPasswordInputField.interactable = true;
        RepeatNewPasswordInputField.interactable = true;
    }

    public void disableNewPasswordInputFileds()
    {
        ResetTokenInputField.interactable = false;
        NewPasswordInputField.interactable = false;
        RepeatNewPasswordInputField.interactable = false;
    }

    public bool newPasswordInputFieldsAreEnabled()
    {
        return ResetTokenInputField.interactable &&
               NewPasswordInputField.interactable &&
               RepeatNewPasswordInputField.interactable;
    }
}