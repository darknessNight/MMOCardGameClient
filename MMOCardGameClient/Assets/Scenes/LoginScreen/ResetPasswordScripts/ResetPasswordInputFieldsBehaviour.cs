﻿using UnityEngine;
using UnityEngine.UI;

public class ResetPasswordInputFieldsBehaviour : MonoBehaviour
{
    public ResetPasswordComponentsDataStructure ResetPasswordComponents;

    private ResetPasswordFormValidator validator;


    private void Start()
    {
        validator = new ResetPasswordFormValidator(ResetPasswordComponents);
    }

    public void checkLoginInputField()
    {
        if (!validator.validateLogin())
            changeText(ResetPasswordComponents.LoginInfoText,
                UserInfo.getLocalizedString("InvalidLogin"), Color.red);
        else
            ResetPasswordComponents.LoginInfoText.text = string.Empty;
    }

    public void checkNewPasswordInputField()
    {
        if (!validator.validateNewPassword())
            changeText(ResetPasswordComponents.NewPasswordInfoText,
                UserInfo.getLocalizedString("InvalidPassword"), Color.red);
        else
            changeText(ResetPasswordComponents.NewPasswordInfoText, @"OK", Color.green);
    }

    public void checkRepeatedPasswordInputField()
    {
        if (!validator.validateRepeatedPassword())
            changeText(ResetPasswordComponents.RepeatNewPasswordInfoText,
                UserInfo.getLocalizedString("InvalidPassword"), Color.red);
        else
            changeText(ResetPasswordComponents.RepeatNewPasswordInfoText, @"OK", Color.green);
    }

    private void changeText(Text infoText, string text, Color color)
    {
        infoText.text = text;
        infoText.color = color;
    }
}