﻿using UnityEngine;
using UnityEngine.UI;

namespace Localization
{
    public class LocalizedText : MonoBehaviour
    {
        private void Start()
        {
            var text = GetComponent<Text>();
            text.text = UserInfo.getLocalizedString(text.text);
        }
    }
}