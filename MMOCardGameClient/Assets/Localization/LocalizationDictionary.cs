﻿using System.Collections.Generic;
using UnityEngine;

namespace Localization
{
    public class LocalizationDictionary
    {
        private readonly Dictionary<string, string> localizations;

        public LocalizationDictionary(Dictionary<string, string> dictionary)
        {
            localizations = dictionary;
        }

        public string getLocalizedTextIfExists(string textToLocalize)
        {
            if (localizations.ContainsKey(textToLocalize))
                return localizations[textToLocalize];
            Debug.Log("cannot find localization of <" + textToLocalize + ">");
            return textToLocalize;
        }
    }
}