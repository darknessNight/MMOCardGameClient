﻿using UnityEngine;
using UnityEngine.UI;
using Localization;

public class AccountSettingsButtonBehaviour : MonoBehaviour 
{
	public SettingsPanelComponentsDataStructure settingsPanelComponents;

	public void displayAccountSettingsPanel()
	{
		if(settingsPanelComponents.currentlyOpenSubCategoryPanel != null)
			settingsPanelComponents.currentlyOpenSubCategoryPanel.SetActive(false);
		settingsPanelComponents.AccountSettingsCategoryPanel.SetActive(true);
		settingsPanelComponents.currentlyOpenSettingPanel = 
			settingsPanelComponents.AccountSettingsCategoryPanel;
	}
	
}
