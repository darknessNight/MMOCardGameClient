﻿using System.Collections.Generic;
using Localization;
using NUnit.Framework;

namespace Tests.Localization
{
    public class LocalizationDictionaryTests
    {
        [Test]
        public void getLocalizedTextIfExists_emptyDictionary_returnsArgumentString()
        {
            var dictionary = new LocalizationDictionary(new Dictionary<string, string>());
            var result = dictionary.getLocalizedTextIfExists("test");
            Assert.AreEqual("test", result);
        }

        [Test]
        public void getLocalizedTextIfExists_oneElementDictionaryGetThisElement_returnsDictionaryElement()
        {
            var dictionary = new LocalizationDictionary(new Dictionary<string, string>
            {
                {"test", "result"}
            });
            var result = dictionary.getLocalizedTextIfExists("test");
            Assert.AreEqual("result", result);
        }

        [Test]
        public void getLocalizedTextIfExists_oneElementDictionaryGetNonExistingElement_returnsArgumentString()
        {
            var dictionary = new LocalizationDictionary(new Dictionary<string, string>
            {
                {"test", "result"}
            });
            var result = dictionary.getLocalizedTextIfExists("nonExisting");
            Assert.AreEqual("nonExisting", result);
        }
    }
}