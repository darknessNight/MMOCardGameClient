﻿using NUnit.Framework;
using UnityEngine;
using UnityEngine.UI;

public class LoginScriptTest
{
    [Test]
    [Ignore("Test in development")]
    public void LoginWithCorrectLoginAndPassword()
    {
        var LoginScriptMonoBehaviour = new GameObject().AddComponent<LoginScript>();
        LoginScriptMonoBehaviour.LoginScreenComponents =
            new GameObject().AddComponent<LoginScreenComponentsDataStructure>();
        LoginScriptMonoBehaviour.LoginScreenComponents.LoginInputField = new GameObject().AddComponent<InputField>();
        LoginScriptMonoBehaviour.LoginScreenComponents.PasswordInputField = new GameObject().AddComponent<InputField>();
        LoginScriptMonoBehaviour.LoginScreenComponents.MenuButtons.LoginButton =
            new GameObject().AddComponent<Button>();
        LoginScriptMonoBehaviour.LoginScreenComponents.MenuButtons.RegisterButton =
            new GameObject().AddComponent<Button>();
        LoginScriptMonoBehaviour.LoginScreenComponents.MenuButtons.ExitButton = new GameObject().AddComponent<Button>();

//		var SessionController = Substitute.For<ISessionManaging>();
        //TODO: Finish test
    }
}