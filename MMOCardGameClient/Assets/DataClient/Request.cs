﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataClient
{
    public abstract class Request<T>
    {
        /// <summary>
        /// Method invoked on request error. Each error returned by server and returned by request itself
        /// </summary>
        public Action<List<string>> ErrorCallback { get; set; }
        /// <summary>
        /// Method invoked on request success. Returns specified type
        /// </summary>
        public Action<T> SuccessCallback { get; set; }
        /// <summary>
        /// Invoked on forbidden request
        /// </summary>
        public Action ForbiddenAction { get; set; }
        /// <summary>
        /// Special deserializer from json structure. If not specified it used JsonUntil.FromJson(...)
        /// </summary>
        public Func<string, T> SpecialDeserializer { protected get; set; } = null;
        /// <summary>
        /// Start request in coroutine
        /// </summary>
        public abstract void Send();
    }
}
