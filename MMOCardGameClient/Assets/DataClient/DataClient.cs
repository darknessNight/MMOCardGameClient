﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace DataClient
{
    public struct RegisterData{
        string username; string password; string repeatedPassword; string mail;
    }

    public struct ResetPasswordData{
        string login; string password; string repeatedPassword; string resetCode;
    }

    public class GameEndData
    {
        public string login;
        public string result;
        public int award;
    }

    public class DeckName
    {
        public string name;
    }

    public class CardId
    {
        public int id;
    }

    public struct Void { }

    public interface ILoggedClient
    {
        Action UnauthorizedAction { get; set; }
        
        Request<Void> logout();
        Request<GameEndData> getLastGameResult();
        Request<UserCardCollection[]> getUserCardCollection();
        Request<UserDeckJSON[]> getUserDecks();
        Request<Void> addCardToDeck(string deckName, int id);
        Request<Void> createNewDeck(string name);
        Request<Void> deleteDeck(string deckName);
        Request<Void> addCardsToDeck(string deckName, IList<UserCardCollection> cards);
        Request<Void> deleteCardsFromDeck(string deckName, IList<UserCardCollection> cards);
        Request<UserCardCollection[]> getCardsInDeck(string deckName);
        //tutaj inne metody
    }

    public interface AccessClient{
        Request<ILoggedClient> login(string username, string password);
        Request<Void> registerNewClient(RegisterData data);
        Request<Void> requestResetPassword(string username);
        Request<Void> resetPassword(ResetPasswordData data);
    }
}
