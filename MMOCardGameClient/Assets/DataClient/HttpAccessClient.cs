﻿using System;
using UnityEngine;

namespace DataClient
{
    struct LoggonToken
    {
        public string token;
    }

    public class HttpAccessClient : AccessClient
    {
        public Request<ILoggedClient> login(string username, string password)
        {
            return new LoginHttpRequest<ILoggedClient>
            {
                SpecialDeserializer = DeserializeLoggedClientFromToken,
                Url = UserInfo.DataServerUrlAuthenticated,
                Username = username,
                Password = password
            };
        }

        ILoggedClient DeserializeLoggedClientFromToken(string arg)
        {
            var token = JsonUtility.FromJson<LoggonToken>(arg);
            return new HttpLoggedClient(token, UserInfo.UserName);
        }


        public Request<Void> registerNewClient(RegisterData data)
        {
            throw new NotImplementedException();
        }

        public Request<Void> requestResetPassword(string username)
        {
            throw new NotImplementedException();
        }

        public Request<Void> resetPassword(ResetPasswordData data)
        {
            throw new NotImplementedException();
        }
    }
}

