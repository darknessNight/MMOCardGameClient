@echo off
for /f "usebackq tokens=*" %%i in (`vswhere -latest -products * -requires Microsoft.Component.MSBuild -property installationPath`) do (
  set InstallDir=%%i
  echo %%i
)

nuget restore ../Backend/Backend.sln

if exist "%InstallDir%\MSBuild\15.0\Bin\MSBuild.exe" (
	"%InstallDir%\MSBuild\15.0\Bin\MSBuild.exe" /p:Configuration=Debug ../Backend\Backend.sln
)

for /r . %%g in (../Backend/bin/debug/*.dll) do ( 
copy "..\Backend\bin\Debug\%%~nxg" "..\MMOCardGameClient\Assets\Backend\%%~nxg"
)