﻿using Backend.Operations;

using NUnit.Framework;

namespace UnitTests.Tests {
    internal class ArrayOperationsTests {
        [Test]
        public void IntArraysEquals_CompareTwoEqualArrays_ReturnTrue() {
            int[] a1 = {1, 2, 3};
            int[] a2 = {1, 2, 3};

            var result = ArrayOperations.IntArraysEqual(a1, a2);

            Assert.AreEqual(result, true);
        }

        [Test]
        public void IntArraysEquals_CompareTwoNotEqualArrays_ReturnFalse() {
            int[] a1 = {1, 2, 4};
            int[] a2 = {1, 2, 3};

            var result = ArrayOperations.IntArraysEqual(a1, a2);

            Assert.AreEqual(result, false);
        }

        [Test]
        public void IntArraysEquals_CompareTwoNulls_ReturnTrue() {
            int[] a1 = null;
            int[] a2 = null;

            var result = ArrayOperations.IntArraysEqual(a1, a2);

            Assert.AreEqual(result, true);
        }

        [Test]
        public void IntArraysEquals_CompareOneNulls_ReturnFalse() {
            int[] a1 = {1, 2, 3};
            int[] a2 = null;

            var result = ArrayOperations.IntArraysEqual(a1, a2);

            Assert.AreEqual(result, false);
        }

        [Test]
        public void IntArraysEquals_CompareTwoArraysWithDifferentLengths_ReturnFalse() {
            int[] a1 = {1, 2, 3, 4};
            int[] a2 = {1, 2, 3};

            var result = ArrayOperations.IntArraysEqual(a1, a2);

            Assert.AreEqual(result, false);
        }

        [Test]
        public void StringArraysEquals_CompareTwoEqualArrays_ReturnTrue() {
            string[] a1 = {"abc", "bcd", "cde"};
            string[] a2 = {"abc", "bcd", "cde"};

            var result = ArrayOperations.StringArraysEqual(a1, a2);

            Assert.AreEqual(result, true);
        }

        [Test]
        public void StringArraysEquals_CompareTwoNotEqualArrays_ReturnFalse() {
            string[] a1 = {"abc", "bcd", "cde"};
            string[] a2 = {"abc", "bcde", "cde"};

            var result = ArrayOperations.StringArraysEqual(a1, a2);

            Assert.AreEqual(result, false);
        }

        [Test]
        public void StringArraysEquals_CompareTwoNulls_ReturnTrue() {
            string[] a1 = null;
            string[] a2 = null;

            var result = ArrayOperations.StringArraysEqual(a1, a2);

            Assert.AreEqual(result, true);
        }

        [Test]
        public void StringArraysEquals_CompareOneNulls_ReturnFalse() {
            string[] a1 = {"abc", "bcd", "cde"};
            string[] a2 = null;

            var result = ArrayOperations.StringArraysEqual(a1, a2);

            Assert.AreEqual(result, false);
        }

        [Test]
        public void StringArraysEquals_CompareTwoArraysWithDifferentLengths_ReturnFalse() {
            string[] a1 = {"abc", "bcd", "cde", "def"};
            string[] a2 = {"abc", "bcd", "cde"};

            var result = ArrayOperations.StringArraysEqual(a1, a2);

            Assert.AreEqual(result, false);
        }
    }
}