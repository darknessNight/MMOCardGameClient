﻿using Backend.JSON.Entities;
using Backend.JSON.Readers;

using NUnit.Framework;

using UnitTests.Tools;

namespace UnitTests.Tests {
    internal class NamedEffectsJSONReaderTests {
        [Test]
        public void GetNamedEffects_JSONWithOneNamedEffect_ReturnCorrectNamedEffect() {
            var namedEffect = GetOneNamedEffect();

            var loadedEffect =
                NamedEffectsJSONReader.GetNamedEffects(DirectoryManager.getTestDir() +
                                                       "/testJSONs/OneNamedEffect.json");

            CollectionAssert.AreEqual(namedEffect, loadedEffect);
        }

        public NamedEffectJSON[] GetOneNamedEffect() {
            NamedEffectJSON[] oneEffect = {
                new NamedEffectJSON(1, "Zawsze czujny",
                    "Jednostka może atakować i używać umiejętności kosztujących nałożenie punktów przeciążenia systemu w rundzie w której weszła na pole bitwy.")
            };
            return oneEffect;
        }

        [Test]
        public void GetNamedEffects_JSONWithTwoNamedEffects_ReturnCorrectNamedEffects() {
            var namedEffects = GetTwoNamedEffects();

            var loadedEffects =
                NamedEffectsJSONReader.GetNamedEffects(
                    DirectoryManager.getTestDir() + "/testJSONs/TwoNamedEffects.json");

            CollectionAssert.AreEqual(namedEffects, loadedEffects);
        }

        public NamedEffectJSON[] GetTwoNamedEffects() {
            var first = new NamedEffectJSON(1, "Zawsze czujny",
                "Jednostka może atakować i używać umiejętności kosztujących nałożenie punktów przeciążenia systemu w rundzie w której weszła na pole bitwy.");
            var second = new NamedEffectJSON(2, "nazwa test", "opis test");
            NamedEffectJSON[] oneEffect = {first, second};
            return oneEffect;
        }
    }
}