﻿using System.Collections.Generic;

using Backend.GameInformation;

using NUnit.Framework;

namespace UnitTests.Tests {
    internal class GameStateTests {
        [Test]
        public void GetMyUserName_MadeNewGameState_ValueIsCorrect() {
            var gameState = new GameState("Stefan");

            var result = gameState.GetMyUserName();

            Assert.AreEqual(result, "Stefan");
        }

        [Test]
        public void AddPlayer_MadeNewGameAndAddedPlayer_PlayerIsAddedCorrectly() {
            var gameState = new GameState("Stefan");
            var dictionaryWithOnePlayer = GetDictionaryWithOnePlayer();

            gameState.AddPlayer("Stefan", 2, 3, 20, 50);

            CollectionAssert.AreEqual(gameState.GetPlayers(), dictionaryWithOnePlayer);
        }

        private Dictionary<int, Player> GetDictionaryWithOnePlayer() {
            var dictionaryWithOnePlayer = new Dictionary<int, Player>();
            dictionaryWithOnePlayer.Add(2, new Player("Stefan", 2, 3, 20, 50));
            return dictionaryWithOnePlayer;
        }

        [Test]
        public void FindMyPlayerId_SearchForMyIdInGameStateWithThreePlayers_PlayerIdIsCorrect() {
            var gameState = GetGameStateWithThreePlayers();

            gameState.FindMyPlayerId();

            Assert.AreEqual(gameState.GetMyUserId(), 2);
        }

        private GameState GetGameStateWithThreePlayers() {
            var gameState = new GameState("Stefan");
            gameState.AddPlayer("Stefan", 2, 3, 20, 50);
            gameState.AddPlayer("Jan", 3, 4, 20, 50);
            gameState.AddPlayer("Marian", 4, 5, 20, 50);
            return gameState;
        }

        [Test]
        public void GetPlayer_GetPlayerByIdFromGameStateWithThreePlayers_CorrectPlayer() {
            var gameState = GetGameStateWithThreePlayers();
            var player = new Player("Marian", 4, 5, 20, 50);

            var result = gameState.GetPlayer(4);

            Assert.AreEqual(result, player);
        }

        [Test]
        public void SetPlayerLibrary_SetLibraryWithTwoKnownCards_EverythingIsCorrectInPlayer() {
            var gameState = GetGameStateWithThreePlayers();
            var player = GetPlayerWithTwoKnownCardsInLibrary();
            var library = GetLibraryWithTwoKnownCards();

            gameState.GetPlayer(4).Library = library;
            var result = gameState.GetPlayer(4);

            Assert.AreEqual(result, player);
        }

        private Player GetPlayerWithTwoKnownCardsInLibrary() {
            var player = new Player("Marian", 4, 5, 20, 50);
            player.Library.Add(1, new Card(3, 1, 1, 5, 5, 5, new CardCosts(2, 2, 2, 2, 2)));
            player.Library.Add(2, new Card(3, 2, 2, 5, 5, 5, new CardCosts(2, 2, 2, 2, 2)));
            return player;
        }

        private Dictionary<int, Card> GetLibraryWithTwoKnownCards() {
            var library = new Dictionary<int, Card>();
            library.Add(1, new Card(3, 1, 1, 5, 5, 5, new CardCosts(2, 2, 2, 2, 2)));
            library.Add(2, new Card(3, 2, 2, 5, 5, 5, new CardCosts(2, 2, 2, 2, 2)));
            return library;
        }
    }
}