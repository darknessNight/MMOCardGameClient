﻿using Backend.JSON.Entities;

using JSON.Entities.CardReader;
using JSON.Readers.CardReader;

using NUnit.Framework;

using UnitTests.Tools;

namespace UnitTests.Tests {
    public class CardJSONReaderTests {
        [Test]
        public void GetCards_JSONWithOneCardWithAllFieldsButSubtypeAndActivatedAbilities_ReturnCorrectCards() {
            var cardWithAllFieldsButSubtype = GetOneCardWithAllFieldsButSubtypeAndActivatedAbilities();

            var loadedCard =
                CardJSONReader.GetCards(
                    DirectoryManager.getTestDir() + "/testJSONs/OneCardWithAllFieldsButSubtype.json");

            CollectionAssert.AreEqual(cardWithAllFieldsButSubtype, loadedCard);
        }

        public CardJSON[] GetOneCardWithAllFieldsButSubtypeAndActivatedAbilities() {
            var cost = new CostJSON(1, 1, 0, 0, 0);
            var statistics = new StatisticsJSON(2, 1);
            int[] abilities = {1};
            CardJSON[] oneCard = {
                new CardJSON(5, statistics, abilities, cost, null, "Zawsze czujny", "Husarz", "Unit", null, "SI",
                    "Prześladowca", "flavor")
            };
            return oneCard;
        }

        [Test]
        public void GetCards_JSONWithOneBasicLinkCard_ReturnCorrectCards() {
            var basicLinkCard = GetOneBasicLinkCard();

            var loadedCard =
                CardJSONReader.GetCards(DirectoryManager.getTestDir() + "/testJSONs/OneBasicLinkCard.json");

            CollectionAssert.AreEqual(basicLinkCard, loadedCard);
        }

        public CardJSON[] GetOneBasicLinkCard() {
            var abilityCost = new AbilityCostJSON(0, 0, 0, 0, 0, 1);
            var activatedAblitiy =
                new ActivatedAbilityJSON(1, "Zdobądź punkt mocy obliczeniowej szkoły Raiderów.", abilityCost);
            ActivatedAbilityJSON[] activatedAbilities = {activatedAblitiy};
            CardJSON[] oneCard = {
                new CardJSON(1, null, null, null, activatedAbilities
                    , "",
                    "Łącze szkoły Raiderów", "Link", "Basic", null, null, "flavor")
            };
            return oneCard;
        }

        [Test]
        public void GetCards_JSONWithEightCards_ReturnEightCards() {
            var loadedCards = CardJSONReader.GetCards(DirectoryManager.getTestDir() + "/testJSONs/EightCards.json");

            Assert.AreEqual(loadedCards.Length, 8);
        }
    }
}