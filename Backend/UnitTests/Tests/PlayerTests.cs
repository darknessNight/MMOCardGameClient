﻿using Backend.GameInformation;

using NUnit.Framework;

namespace UnitTests.Tests {
    internal class PlayerTests {
        [Test]
        public void GetName_MadeNewPlayer_ValueIsCorrect() {
            var player = new Player("Stefan", 2, 3, 20, 50);

            var result = player.Name;

            Assert.AreEqual(result, "Stefan");
        }

        [Test]
        public void GetId_MadeNewPlayer_ValueIsCorrect() {
            var player = new Player("Stefan", 2, 3, 20, 50);

            var result = player.Id;

            Assert.AreEqual(result, 2);
        }

        [Test]
        public void GetHeroId_MadeNewPlayer_ValueIsCorrect() {
            var player = new Player("Stefan", 2, 3, 20, 50);

            var result = player.HeroId;

            Assert.AreEqual(result, 3);
        }

        [Test]
        public void GetDefence_MadeNewPlayer_ValueIsCorrect() {
            var player = new Player("Stefan", 2, 3, 20, 50);

            var result = player.Defence;

            Assert.AreEqual(result, 20);
        }

        [Test]
        public void GetCardsInLibraryCount_MadeNewPlayer_ValueIsCorrect() {
            var player = new Player("Stefan", 2, 3, 20, 50);

            var result = player.CardsInLibraryCount;

            Assert.AreEqual(result, 50);
        }

        [Test]
        public void SetDefence_MadeNewPlayerAndChangedHisDefence_ValueIsCorrect() {
            var player = new Player("Stefan", 2, 3, 20, 50);

            player.Defence = 10;
            var result = player.Defence;

            Assert.AreEqual(result, 10);
        }
    }
}