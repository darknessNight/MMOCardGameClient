﻿using Backend.JSON.Entities;
using Backend.JSON.Readers;

using NUnit.Framework;

using UnitTests.Tools;

namespace UnitTests.Tests {
    internal class HerosJSONReaderTests {
        [Test]
        public void GetHeros_JSONWithHero_ReturnCorrectHero() {
            var hero = GetOneHero();

            var loadedHero = HerosJSONReader.GetHeros(DirectoryManager.getTestDir() + "/testJSONs/OneHero.json");

            CollectionAssert.AreEqual(hero, loadedHero);
        }

        public HeroJSON[] GetOneHero() {
            int[] abilityCosts = {1, 2, 3, 4};
            int[] startingCooldowns = {1, 2, 3, 4};
            int[] cooldown = {1, 2, 3, 4};
            string[] abilityNames = {"name1", "name2", "name3", "name4"};
            HeroJSON[] oneHero =
                {new HeroJSON(1, "Nazwa", "opis", abilityCosts, startingCooldowns, cooldown, abilityNames)};
            return oneHero;
        }

        [Test]
        public void GetHeros_JSONWithTwoHeros_ReturnCorrectHeros() {
            var heros = GetTwoHeros();

            var loadedHeros = HerosJSONReader.GetHeros(DirectoryManager.getTestDir() + "/testJSONs/TwoHeros.json");

            CollectionAssert.AreEqual(heros, loadedHeros);
        }

        public HeroJSON[] GetTwoHeros() {
            int[] abilityCosts = {1, 2, 3, 4};
            int[] startingCooldowns = {1, 2, 3, 4};
            int[] cooldown = {1, 2, 3, 4};
            string[] abilityNames = {"name1", "name2", "name3", "name4"};
            var firstHero = new HeroJSON(1, "Nazwa", "opis", abilityCosts, startingCooldowns, cooldown, abilityNames);

            int[] abilityCosts2 = {4, 3, 2, 1};
            int[] startingCooldowns2 = {4, 3, 2, 1};
            int[] cooldown2 = {4, 3, 2, 1};
            string[] abilityNames2 = {"name12", "name22", "name32", "name42"};
            var secondHero = new HeroJSON(2, "Nazwa2", "opis2", abilityCosts2, startingCooldowns2, cooldown2,
                abilityNames2);

            HeroJSON[] twoHeros = {firstHero, secondHero};
            return twoHeros;
        }
    }
}