﻿using System;
using System.Collections.Generic;

using Backend.GameInformation;

using ProtoBuffers;

using CardCosts = ProtoBuffers.CardCosts;

namespace Backend.ClientCommunication {
    public interface IClientCallback {
        /*Client gets this callback after initial connection is made*/
        void ConnectionWasMade(GameState gameState);

        /*Client gets this callback when phase is changed*/
        void PhaseChanged(GamePhase oldPhase, int oldPlayerId, GamePhase newPhase, int newPlayerId);

        /*Client gets this callback after every change to any players hand
         this includes changes during choosing first hand*/
        void HandChanged(int playerId);

        /*Client gets this callback after statistics of card are revealed*/
        void CardRevealed(Card revealedCard, int cardRevealedOwner, CardPosition cardRevealedPosition);

        /*Clients gets this callback after position of card is changed
         includes dying of units etc.*/
        void CardMoved(Card movedCard, int oldOwner, CardPosition oldPosition, int newOnwer, CardPosition newPosition);

        /*Client gets this callback after his own effect is added to stack*/
        void YourElementAddedToStack(int stackId, StackElement stackElement);

        /*Client gets this callback after other player effect is added to stack*/
        void OtherPlayerElementAddedToStack(int stackId, StackElement stackElement, int playerId, CardCosts cardCosts);

        /*Client gets this callback after transaction needs player to choose options/targets/cards*/
        void PickOptions(int transactionId, TargetList targetList, int instanceId);

        /*Client gets this callback after transactio is finished and he can really play the card/ability
         or he can use it to choose attackers/defenders 
         then you need to remember data from all callbacks while choosing attackers/defenders 
         and send it while attacking/defending*/
        void YouFinishedChoosing(int transactionId, int instanceId);

        /*Client gets this callback when list of targets in result message of transaction is not null*/
        void GotTargetListInResultOfTransaction(int transactionId, int instanceId, TargetList targetList);

        /*Client gets this callback when stack needs his reaction*/
        void ReactToStackAction();

        /*Client gets this callback when he needs to wait for reaction of other player*/
        void WaitForReactionToStackAction(int playerId);

        /*Client gets this callback when statistics of some card change*/
        void CardStatisticsChanged(Card changedCard);

        /*Client gets this callback when some player defence change*/
        void PlayerDefenceChanged(int playerId, int newDefence);

        /*Client gets this callback when he won*/
        void YouWon();

        /*Client gets this callback when other player won
         I don't know if this callback needs implementation
         Client gets YouLost Callback afterwards anyway
         and it is possible to not get this callback at all
         But I send it anyway*/
        void PlayerWon(int playerId);

        /*Client gets this callback when he lost*/
        void YouLost();

        /*Client gets this callback when other player lose
         this doesn't necessarily mean that you won*/
        void PlayerLost(int playerId);

        /*Client gets this callback when attackers or defenders are seleceted*/
        void AttackerDefenderSelected(Dictionary<int, List<TargetList>> attackersDefenders);

        /*Client gets this callback when attack is executed*/
        void AttackExecuted(int attackerId, int defenderId, TargetType defenderType);

        /*Client gets this callback when card is created*/
        void CardCreated(Card createdCard, int cardCreatedOwner, CardPosition cardCreatedCardPosition);

        /*Cient gets this callback when stack action wasn't countered and finished succesfully*/
        void StackActionCompleted(int stackId, StackElement completedStackElement);

        /*Client gets this callback when stack action is countered*/
        void StackActionRemoved(int stackId, StackElement removedStackElement);

        /*Client gets this method when link points are added or substracted*/
        void LinkPointsUpdated(int playerId, LinkPoints playerLinkPoints);

        /*Client gets this callback when error in communication with server occured*/
        void ServerError(Exception ex);

        /*Client gets this callback when trigger is added to stack*/
        void TriggerAddedToStack(int stackId, StackElement stackElement);

        /*Client gets this callback when trigger that ignores stack is activated*/
        void TriggerWithoutStackActivated(int instanceId, TriggerType triggerType);
    }
}