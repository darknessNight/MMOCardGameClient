﻿using System.Linq;

using Backend.JSON.Entities;

namespace Backend.Operations {
    public class ArrayOperations {
        public static bool IntArraysEqual(int[] a1, int[] a2) {
            if (a1 == null && a2 == null)
                return true;
            if (a1 == null || a2 == null)
                return false;

            if (a1.Length == a2.Length) return a1.SequenceEqual(a2);
            return false;
        }

        public static bool StringArraysEqual(string[] a1, string[] a2) {
            if (a1 == null && a2 == null)
                return true;
            if (a1 == null || a2 == null)
                return false;

            if (a1.Length == a2.Length) return a1.SequenceEqual(a2);
            return false;
        }

        public static bool ActivatedAbilitiArraysEqual(ActivatedAbilityJSON[] a1, ActivatedAbilityJSON[] a2) {
            if (a1 == null && a2 == null)
                return true;
            if (a1 == null || a2 == null)
                return false;

            if (a1.Length == a2.Length) return a1.SequenceEqual(a2);
            return false;
        }
    }
}