﻿using System.IO;

namespace Backend.Constants {
    public class Directories {
        private static string logLocation = @"D:/Desktop/log.txt";
        private static string messagesLocation;
        public static string CardJSONLocation { get; set; }
        public static string CriticalLogLocation { get; set; } = @"%appdata%/Local/criticalLog/critical.log";

        public static string LogLocation {
            get => logLocation;
            set {
                var directoryPath = Path.GetDirectoryName(value);
                if (directoryPath != null) Directory.CreateDirectory(directoryPath);

                logLocation = value;
            }
        }

        public static string MessagesLocation {
            get => messagesLocation;
            set {
                var directoryPath = Path.GetDirectoryName(value);
                if (directoryPath != null) Directory.CreateDirectory(directoryPath);

                messagesLocation = value;
            }
        }
    }
}