﻿using System;

namespace Backend.Exceptions {
    internal class NotFoundException : Exception { }
}