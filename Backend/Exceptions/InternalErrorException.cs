﻿using System;

namespace Backend.Exceptions {
    internal class InternalErrorException : Exception { }
}