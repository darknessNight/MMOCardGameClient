﻿using System;

namespace Backend.Exceptions {
    internal class NoClassIdInLoadedCardsJSONException : Exception { }
}