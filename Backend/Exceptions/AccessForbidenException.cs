﻿using System;

namespace Backend.Exceptions {
    internal class AccessForbidenException : Exception { }
}