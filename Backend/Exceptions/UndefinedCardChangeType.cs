﻿using System;

namespace Backend.Exceptions {
    internal class UndefinedCardChangeType : Exception { }
}