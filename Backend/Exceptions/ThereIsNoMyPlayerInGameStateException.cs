﻿using System;

namespace Backend.Exceptions {
    internal class ThereIsNoMyPlayerInGameStateException : Exception { }
}