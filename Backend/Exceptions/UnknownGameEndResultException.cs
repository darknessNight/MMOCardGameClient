﻿using System;

namespace Backend.Exceptions {
    internal class UnknownGameEndResultException : Exception { }
}