﻿using System;

namespace Backend.Exceptions {
    internal class NotAcceptableException : Exception { }
}