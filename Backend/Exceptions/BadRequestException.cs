﻿using System;

namespace Backend.Exceptions {
    internal class BadRequestException : Exception { }
}