﻿using System;

namespace Backend.Exceptions {
    internal class InconsistentClientStateException : Exception { }
}