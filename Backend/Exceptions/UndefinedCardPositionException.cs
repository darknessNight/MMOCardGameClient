﻿using System;

namespace Backend.Exceptions {
    internal class UndefinedCardPositionException : Exception { }
}