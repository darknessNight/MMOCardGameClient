﻿using System.Collections.Generic;
using System.Linq;

using Backend.GameInformation.Enums;

namespace Backend.GameInformation {
    public class Card {
        public Card(int? cardId, int instanceId, int positionIndex, int? overloadedPoints, int? attack, int? defence,
            CardCosts cardCosts) {
            CardId = cardId;
            InstanceId = instanceId;
            PositionIndex = positionIndex;
            OverloadedPoints = overloadedPoints;
            Attack = attack;
            Defence = defence;
            CardCosts = cardCosts;
            Type = CardType.Unknown;
            Subtype = CardSubType.Unknown;
            ClassValue = null;
            SubClass = null;

            ActivatedAbilities = new Dictionary<int, ActivatedAbility>();
        }

        public Card(int? cardId, int instanceId, int positionIndex, int? overloadedPoints, int? attack, int? defence,
            CardCosts cardCosts, CardType type, CardSubType subType, string classValue, string subClass) {
            CardId = cardId;
            InstanceId = instanceId;
            PositionIndex = positionIndex;
            OverloadedPoints = overloadedPoints;
            Attack = attack;
            Defence = defence;
            CardCosts = cardCosts;
            Type = type;
            Subtype = subType;
            ClassValue = classValue;
            SubClass = subClass;

            ActivatedAbilities = new Dictionary<int, ActivatedAbility>();
        }

        public Card(int instanceId) {
            CardId = null;
            InstanceId = instanceId;
            PositionIndex = null;
            OverloadedPoints = null;
            Attack = null;
            Defence = null;
            CardCosts = new CardCosts(-1, -1, -1, -1, -1);
            Type = CardType.Unknown;
            Subtype = CardSubType.Unknown;
            ClassValue = null;
            SubClass = null;

            ActivatedAbilities = new Dictionary<int, ActivatedAbility>();
        }

        public int? CardId { get; set; }
        public int InstanceId { get; }
        public int? PositionIndex { get; set; }
        public int? OverloadedPoints { get; set; }
        public int? Attack { get; set; }
        public int? Defence { get; set; }
        public CardCosts CardCosts { get; set; }
        public CardType Type { get; set; }
        public CardSubType Subtype { get; set; }
        public string ClassValue { get; set; }
        public string SubClass { get; set; }
        public Dictionary<int, ActivatedAbility> ActivatedAbilities { get; set; }

        protected bool Equals(Card other) {
            return CardId == other.CardId && InstanceId == other.InstanceId && PositionIndex == other.PositionIndex &&
                   OverloadedPoints == other.OverloadedPoints && Attack == other.Attack && Defence == other.Defence &&
                   Equals(CardCosts, other.CardCosts) && Equals(Type, other.Type) &&
                   Equals(Subtype, other.Subtype) && string.Equals(ClassValue, other.ClassValue) &&
                   string.Equals(SubClass, other.SubClass) &&
                   ActivatedAbilities.SequenceEqual(other.ActivatedAbilities);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Card) obj);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = CardId.GetHashCode();
                hashCode = (hashCode * 397) ^ InstanceId;
                hashCode = (hashCode * 397) ^ (PositionIndex != null ? PositionIndex.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ OverloadedPoints.GetHashCode();
                hashCode = (hashCode * 397) ^ Attack.GetHashCode();
                hashCode = (hashCode * 397) ^ Defence.GetHashCode();
                hashCode = (hashCode * 397) ^ (CardCosts != null ? CardCosts.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Type.GetHashCode();
                hashCode = (hashCode * 397) ^ Subtype.GetHashCode();
                hashCode = (hashCode * 397) ^ (ClassValue != null ? ClassValue.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (SubClass != null ? SubClass.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (ActivatedAbilities != null ? ActivatedAbilities.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}