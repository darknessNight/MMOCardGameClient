﻿using System.Collections.Generic;

using ProtoBuffers;

namespace Backend.GameInformation {
    public class StackElement {
        public List<TargetList> TargetLists;

        public StackElement(int instanceId, int? abilityId, List<TargetList> targetLists, TriggerType? triggerType) {
            InstanceId = instanceId;
            AbilityId = abilityId;
            TargetLists = targetLists;
            TriggerType = triggerType;
        }

        public int InstanceId { get; set; }

        //AbilityId equal 0 means this is card played not ability
        public int? AbilityId { get; set; }
        public TriggerType? TriggerType { get; set; }
    }
}