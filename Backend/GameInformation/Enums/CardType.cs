﻿namespace Backend.GameInformation.Enums {
    public enum CardType {
        Link,
        Unit,
        Script,
        Operation,
        ServiceProgram,
        Unknown,
        CalcUnit
    }
}