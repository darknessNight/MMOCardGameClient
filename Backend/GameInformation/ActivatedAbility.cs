﻿namespace Backend.GameInformation {
    public class ActivatedAbility {
        public ActivatedAbility(int id, string description, ActivatedAbilityCosts activatedAbilityCosts) {
            Id = id;
            Description = description;
            ActivatedAbilityCosts = activatedAbilityCosts;
        }

        public int Id { get; }
        public string Description { get; set; }
        public ActivatedAbilityCosts ActivatedAbilityCosts { get; set; }

        protected bool Equals(ActivatedAbility other) {
            return Id == other.Id && string.Equals(Description, other.Description) &&
                   Equals(ActivatedAbilityCosts, other.ActivatedAbilityCosts);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ActivatedAbility) obj);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = Id;
                hashCode = (hashCode * 397) ^ (Description != null ? Description.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (ActivatedAbilityCosts != null ? ActivatedAbilityCosts.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}