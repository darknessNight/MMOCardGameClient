﻿namespace Backend.GameInformation {
    public class CalcUnit {
        public CalcUnit(int defence, int points, int instanceId) {
            Defence = defence;
            Points = points;
            InstanceId = instanceId;
        }

        public int Defence { get; set; }
        public int Points { get; set; }
        public int InstanceId { get; }

        protected bool Equals(CalcUnit other) {
            return Defence == other.Defence && Points == other.Points && InstanceId == other.InstanceId;
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((CalcUnit) obj);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = Defence;
                hashCode = (hashCode * 397) ^ Points;
                hashCode = (hashCode * 397) ^ InstanceId;
                return hashCode;
            }
        }
    }
}