﻿namespace Backend.GameInformation {
    public class CardCosts {
        public CardCosts(int neutral, int psychos, int raiders, int scanners, int whiteHats) {
            Neutral = neutral;
            Psychos = psychos;
            Raiders = raiders;
            Scanners = scanners;
            WhiteHats = whiteHats;
        }

        public int Neutral { get; set; }
        public int Psychos { get; set; }
        public int Raiders { get; set; }
        public int Scanners { get; set; }
        public int WhiteHats { get; set; }

        protected bool Equals(CardCosts other) {
            return Neutral == other.Neutral && Psychos == other.Psychos && Raiders == other.Raiders &&
                   Scanners == other.Scanners && WhiteHats == other.WhiteHats;
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((CardCosts) obj);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = Neutral;
                hashCode = (hashCode * 397) ^ Psychos;
                hashCode = (hashCode * 397) ^ Raiders;
                hashCode = (hashCode * 397) ^ Scanners;
                hashCode = (hashCode * 397) ^ WhiteHats;
                return hashCode;
            }
        }
    }
}