﻿using System.Collections.Generic;
using System.Linq;

using Backend.Exceptions;

using ProtoBuffers;

namespace Backend.GameInformation {
    public class GameState {
        private readonly string myUserName;
        private readonly Dictionary<int, Player> players;
        private int? myUserId;

        public GameState(string myUserName) {
            players = new Dictionary<int, Player>();
            Stack = new Dictionary<int, StackElement>();
            CurrentPhase = GamePhase.Waiting;
            CurrentPlayerId = -1;

            this.myUserName = myUserName;
        }

        public GamePhase CurrentPhase { get; set; }
        public int CurrentPlayerId { get; set; }
        public Dictionary<int, StackElement> Stack { get; set; }

        protected bool Equals(GameState other) {
            return myUserId == other.myUserId && string.Equals(myUserName, other.myUserName) &&
                   players.SequenceEqual(other.players) && CurrentPhase.Equals(other.CurrentPhase) &&
                   CurrentPlayerId.Equals(other.CurrentPlayerId) && Stack.SequenceEqual(other.Stack);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((GameState) obj);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = myUserId.GetHashCode();
                hashCode = (hashCode * 397) ^ (myUserName != null ? myUserName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (players != null ? players.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ CurrentPhase.GetHashCode();
                hashCode = (hashCode * 397) ^ CurrentPlayerId.GetHashCode();
                hashCode = (hashCode * 397) ^ (Stack != null ? Stack.GetHashCode() : 0);
                return hashCode;
            }
        }

        public void AddPlayer(string name, int id, int heroId, int defence, int cardsInLibraryCount) {
            players.Add(id, new Player(name, id, heroId, defence, cardsInLibraryCount));
        }

        public void FindMyPlayerId() {
            foreach (var player in players.Values)
                if (player.Name == myUserName)
                    myUserId = player.Id;
            if (myUserId == null)
                throw new ThereIsNoMyPlayerInGameStateException();
        }

        public string GetMyUserName() {
            return myUserName;
        }

        public int? GetMyUserId() {
            return myUserId;
        }

        public Player GetPlayer(int id) {
            return players[id];
        }

        public Dictionary<int, Player> GetPlayers() {
            return players;
        }
    }
}