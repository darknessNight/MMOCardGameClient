﻿using System.Collections.Generic;

using ProtoBuffers;

namespace Backend.GameInformation {
    public class TargetList {
        public TargetList() {
            Targets = new List<Target>();
        }

        public string Description { get; set; }
        public TargetSelectingTransaction.Types.OptionsType OptionsType { get; set; }
        public List<Target> Targets { get; set; }
    }
}