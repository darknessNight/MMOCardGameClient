﻿namespace Backend.GameInformation {
    public class ActivatedAbilityCosts : CardCosts {
        public ActivatedAbilityCosts(int neutral, int psychos, int raiders, int scanners, int whiteHats, int overLoad) :
            base(neutral, psychos, raiders, scanners, whiteHats) {
            OverLoad = overLoad;
        }

        public int OverLoad { get; set; }

        protected bool Equals(ActivatedAbilityCosts other) {
            return base.Equals(other) && OverLoad == other.OverLoad;
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ActivatedAbilityCosts) obj);
        }

        public override int GetHashCode() {
            unchecked {
                return (base.GetHashCode() * 397) ^ OverLoad;
            }
        }
    }
}