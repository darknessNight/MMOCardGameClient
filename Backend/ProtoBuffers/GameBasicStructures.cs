// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: GameBasicStructures.proto

#pragma warning disable 1591, 0612, 3021

#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;

namespace ProtoBuffers {
    /// <summary>Holder for reflection information generated from GameBasicStructures.proto</summary>
    public static partial class GameBasicStructuresReflection {
        #region Descriptor

        /// <summary>File descriptor for GameBasicStructures.proto</summary>
        public static pbr::FileDescriptor Descriptor {
            get { return descriptor; }
        }

        private static pbr::FileDescriptor descriptor;

        static GameBasicStructuresReflection() {
            byte[] descriptorData = global::System.Convert.FromBase64String(
                string.Concat(
                    "ChlHYW1lQmFzaWNTdHJ1Y3R1cmVzLnByb3RvEgxQcm90b0J1ZmZlcnMaFFNp",
                    "bXBsZU1lc3NhZ2VzLnByb3RvImMKCUNhcmRDb3N0cxIPCgduZXV0cmFsGAUg",
                    "ASgFEg8KB3JhaWRlcnMYASABKAUSDwoHcHN5Y2hvcxgCIAEoBRIQCghzY2Fu",
                    "bmVycxgDIAEoBRIRCgl3aGl0ZUhhdHMYBCABKAUqZgoMQ2FyZFBvc2l0aW9u",
                    "EhUKEVVuZGVmaW5lZFBvc2l0aW9uEAASCwoHTGlicmFyeRABEggKBEhhbmQQ",
                    "AhIPCgtCYXR0bGVGaWVsZBADEg0KCUdyYXZleWFyZBAEEggKBFZvaWQQBSo1",
                    "CgpUYXJnZXRUeXBlEhEKDVVuZGVmaW5lZFR5cGUQABIKCgZQbGF5ZXIQARII",
                    "CgRDYXJkEAMq8AEKCUdhbWVQaGFzZRISCg5VbmRlZmluZWRQaGFzZRAAEgsK",
                    "B1dhaXRpbmcQARIRCg1GaXJzdEhhbmREcmF3EAISGgoWUGxheWVyUm91bmRQ",
                    "aGFzZXNTdGFydBADEgkKBVVudGFwEAQSCAoETWFpbhAFEgoKBkF0dGFjaxAG",
                    "EgsKB0RlZmVuY2UQBxIMCghFbmRSb3VuZBAIEhgKFFBsYXllclJvdW5kUGhh",
                    "c2VzRW5kEAkSEQoNUmVhY3Rpb25QaGFzZRAKEhAKDFRyaWdnZXJQaGFzZRAL",
                    "EhgKFEF0dGFja0V4ZWN1dGlvblBoYXNlEAwqgAIKC1RyaWdnZXJUeXBlEhkK",
                    "FVVuZGVmaW5lZFRyaWdnZXJUeXBlXxAAEhEKDUNhcmRBdHRhY2tlZF8QARIV",
                    "ChFDYXJkQ2hhbmdlZE93bmVyXxACEg4KCkNhcmRNb3ZlZF8QAxIRCg1DYXJk",
                    "UmV2ZWFsZWRfEAQSDQoJRHJhd0NhcmRfEAUSDwoLUGhhc2VFbmRlZF8QBhIT",
                    "Cg9NYWluUGhhc2VFbmRlZF8QBxITCg9QbGF5ZXJEZWZlYXRlZF8QCBIWChJQ",
                    "bGF5ZXJMaWZlQ2hhbmdlZF8QCRIVChFUcmlnZ2VyQWN0aXZhdGVkXxAKEhAK",
                    "DENhcmRDcmVhdGVkXxALQkgKL2NvbS5NTU9DYXJkR2FtZS5HYW1lU2VydmVy",
                    "LlNlcnZlcnMuUHJvdG9CdWZmZXJzQhNHYW1lQmFzaWNTdHJ1Y3R1cmVzUAFQ",
                    "AGIGcHJvdG8z"));
            descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
                new pbr::FileDescriptor[] {global::ProtoBuffers.SimpleMessagesReflection.Descriptor,},
                new pbr::GeneratedClrTypeInfo(
                    new[] {
                        typeof(global::ProtoBuffers.CardPosition), typeof(global::ProtoBuffers.TargetType),
                        typeof(global::ProtoBuffers.GamePhase), typeof(global::ProtoBuffers.TriggerType),
                    }, new pbr::GeneratedClrTypeInfo[] {
                        new pbr::GeneratedClrTypeInfo(typeof(global::ProtoBuffers.CardCosts),
                            global::ProtoBuffers.CardCosts.Parser,
                            new[] {"Neutral", "Raiders", "Psychos", "Scanners", "WhiteHats"}, null, null, null)
                    }));
        }

        #endregion
    }

    #region Enums

    public enum CardPosition {
        [pbr::OriginalName("UndefinedPosition")]
        UndefinedPosition = 0,
        [pbr::OriginalName("Library")] Library = 1,
        [pbr::OriginalName("Hand")] Hand = 2,
        [pbr::OriginalName("BattleField")] BattleField = 3,
        [pbr::OriginalName("Graveyard")] Graveyard = 4,

        /// <summary>
        ///     noone will return from void
        /// </summary>
        [pbr::OriginalName("Void")] Void = 5,
    }

    public enum TargetType {
        [pbr::OriginalName("UndefinedType")] UndefinedType = 0,
        [pbr::OriginalName("Player")] Player = 1,
        [pbr::OriginalName("Card")] Card = 3,
    }

    public enum GamePhase {
        [pbr::OriginalName("UndefinedPhase")] UndefinedPhase = 0,
        [pbr::OriginalName("Waiting")] Waiting = 1,
        [pbr::OriginalName("FirstHandDraw")] FirstHandDraw = 2,

        [pbr::OriginalName("PlayerRoundPhasesStart")]
        PlayerRoundPhasesStart = 3,
        [pbr::OriginalName("Untap")] Untap = 4,
        [pbr::OriginalName("Main")] Main = 5,
        [pbr::OriginalName("Attack")] Attack = 6,
        [pbr::OriginalName("Defence")] Defence = 7,
        [pbr::OriginalName("EndRound")] EndRound = 8,

        [pbr::OriginalName("PlayerRoundPhasesEnd")]
        PlayerRoundPhasesEnd = 9,
        [pbr::OriginalName("ReactionPhase")] ReactionPhase = 10,
        [pbr::OriginalName("TriggerPhase")] TriggerPhase = 11,

        [pbr::OriginalName("AttackExecutionPhase")]
        AttackExecutionPhase = 12,
    }

    /// <summary>
    ///     added suffix "_" because names conflicts... C sucks
    /// </summary>
    public enum TriggerType {
        [pbr::OriginalName("UndefinedTriggerType_")]
        UndefinedTriggerType = 0,
        [pbr::OriginalName("CardAttacked_")] CardAttacked = 1,

        [pbr::OriginalName("CardChangedOwner_")]
        CardChangedOwner = 2,
        [pbr::OriginalName("CardMoved_")] CardMoved = 3,
        [pbr::OriginalName("CardRevealed_")] CardRevealed = 4,
        [pbr::OriginalName("DrawCard_")] DrawCard = 5,
        [pbr::OriginalName("PhaseEnded_")] PhaseEnded = 6,
        [pbr::OriginalName("MainPhaseEnded_")] MainPhaseEnded = 7,
        [pbr::OriginalName("PlayerDefeated_")] PlayerDefeated = 8,

        [pbr::OriginalName("PlayerLifeChanged_")]
        PlayerLifeChanged = 9,

        [pbr::OriginalName("TriggerActivated_")]
        TriggerActivated = 10,
        [pbr::OriginalName("CardCreated_")] CardCreated = 11,
    }

    #endregion

    #region Messages

    public sealed partial class CardCosts : pb::IMessage<CardCosts> {
        /// <summary>Field number for the "neutral" field.</summary>
        public const int NeutralFieldNumber = 5;

        /// <summary>Field number for the "raiders" field.</summary>
        public const int RaidersFieldNumber = 1;

        /// <summary>Field number for the "psychos" field.</summary>
        public const int PsychosFieldNumber = 2;

        /// <summary>Field number for the "scanners" field.</summary>
        public const int ScannersFieldNumber = 3;

        /// <summary>Field number for the "whiteHats" field.</summary>
        public const int WhiteHatsFieldNumber = 4;

        private static readonly pb::MessageParser<CardCosts> _parser =
            new pb::MessageParser<CardCosts>(() => new CardCosts());

        private int neutral_;
        private int psychos_;
        private int raiders_;
        private int scanners_;
        private int whiteHats_;

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public CardCosts() {
            OnConstruction();
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public CardCosts(CardCosts other) : this() {
            neutral_ = other.neutral_;
            raiders_ = other.raiders_;
            psychos_ = other.psychos_;
            scanners_ = other.scanners_;
            whiteHats_ = other.whiteHats_;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public static pb::MessageParser<CardCosts> Parser {
            get { return _parser; }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public static pbr::MessageDescriptor Descriptor {
            get { return global::ProtoBuffers.GameBasicStructuresReflection.Descriptor.MessageTypes[0]; }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public int Neutral {
            get { return neutral_; }
            set { neutral_ = value; }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public int Raiders {
            get { return raiders_; }
            set { raiders_ = value; }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public int Psychos {
            get { return psychos_; }
            set { psychos_ = value; }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public int Scanners {
            get { return scanners_; }
            set { scanners_ = value; }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public int WhiteHats {
            get { return whiteHats_; }
            set { whiteHats_ = value; }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        pbr::MessageDescriptor pb::IMessage.Descriptor {
            get { return Descriptor; }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public CardCosts Clone() {
            return new CardCosts(this);
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public bool Equals(CardCosts other) {
            if (ReferenceEquals(other, null)) {
                return false;
            }

            if (ReferenceEquals(other, this)) {
                return true;
            }

            if (Neutral != other.Neutral) return false;
            if (Raiders != other.Raiders) return false;
            if (Psychos != other.Psychos) return false;
            if (Scanners != other.Scanners) return false;
            if (WhiteHats != other.WhiteHats) return false;
            return true;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public void WriteTo(pb::CodedOutputStream output) {
            if (Raiders != 0) {
                output.WriteRawTag(8);
                output.WriteInt32(Raiders);
            }

            if (Psychos != 0) {
                output.WriteRawTag(16);
                output.WriteInt32(Psychos);
            }

            if (Scanners != 0) {
                output.WriteRawTag(24);
                output.WriteInt32(Scanners);
            }

            if (WhiteHats != 0) {
                output.WriteRawTag(32);
                output.WriteInt32(WhiteHats);
            }

            if (Neutral != 0) {
                output.WriteRawTag(40);
                output.WriteInt32(Neutral);
            }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public int CalculateSize() {
            int size = 0;
            if (Neutral != 0) {
                size += 1 + pb::CodedOutputStream.ComputeInt32Size(Neutral);
            }

            if (Raiders != 0) {
                size += 1 + pb::CodedOutputStream.ComputeInt32Size(Raiders);
            }

            if (Psychos != 0) {
                size += 1 + pb::CodedOutputStream.ComputeInt32Size(Psychos);
            }

            if (Scanners != 0) {
                size += 1 + pb::CodedOutputStream.ComputeInt32Size(Scanners);
            }

            if (WhiteHats != 0) {
                size += 1 + pb::CodedOutputStream.ComputeInt32Size(WhiteHats);
            }

            return size;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public void MergeFrom(CardCosts other) {
            if (other == null) {
                return;
            }

            if (other.Neutral != 0) {
                Neutral = other.Neutral;
            }

            if (other.Raiders != 0) {
                Raiders = other.Raiders;
            }

            if (other.Psychos != 0) {
                Psychos = other.Psychos;
            }

            if (other.Scanners != 0) {
                Scanners = other.Scanners;
            }

            if (other.WhiteHats != 0) {
                WhiteHats = other.WhiteHats;
            }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public void MergeFrom(pb::CodedInputStream input) {
            uint tag;
            while ((tag = input.ReadTag()) != 0) {
                switch (tag) {
                    default:
                        input.SkipLastField();
                        break;
                    case 8: {
                        Raiders = input.ReadInt32();
                        break;
                    }
                    case 16: {
                        Psychos = input.ReadInt32();
                        break;
                    }
                    case 24: {
                        Scanners = input.ReadInt32();
                        break;
                    }
                    case 32: {
                        WhiteHats = input.ReadInt32();
                        break;
                    }
                    case 40: {
                        Neutral = input.ReadInt32();
                        break;
                    }
                }
            }
        }

        partial void OnConstruction();

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public override bool Equals(object other) {
            return Equals(other as CardCosts);
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public override int GetHashCode() {
            int hash = 1;
            if (Neutral != 0) hash ^= Neutral.GetHashCode();
            if (Raiders != 0) hash ^= Raiders.GetHashCode();
            if (Psychos != 0) hash ^= Psychos.GetHashCode();
            if (Scanners != 0) hash ^= Scanners.GetHashCode();
            if (WhiteHats != 0) hash ^= WhiteHats.GetHashCode();
            return hash;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public override string ToString() {
            return pb::JsonFormatter.ToDiagnosticString(this);
        }
    }

    #endregion
}

#endregion Designer generated code