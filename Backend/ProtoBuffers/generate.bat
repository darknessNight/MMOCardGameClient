set TOOL_DIR=../packages/Grpc.Tools.1.11.0/tools/windows_x64


for /r . %%g in (*.proto) do "%TOOL_DIR%/protoc.exe" -I./ --grpc_out ./ --csharp_out ./ --plugin=protoc-gen-grpc="%TOOL_DIR%/grpc_csharp_plugin.exe" %%~nxg
