﻿using System.IO;

using JSON.Entities.CardReader;

using Newtonsoft.Json;

namespace JSON.Readers.CardReader {
    public class CardJSONReader {
        public static CardJSON[] GetCards(string path) {
            var serializer = new JsonSerializer();

            using (var sw = new StreamReader(path))
            using (JsonReader reader = new JsonTextReader(sw)) {
                return serializer.Deserialize<CardJSON[]>(reader);
            }
        }
    }
}