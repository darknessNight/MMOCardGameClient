﻿using System.IO;

using Backend.JSON.Entities;

using Newtonsoft.Json;

namespace Backend.JSON.Readers {
    public class HerosJSONReader {
        public static HeroJSON[] GetHeros(string path) {
            var serializer = new JsonSerializer();

            using (var sw = new StreamReader(path))
            using (JsonReader reader = new JsonTextReader(sw)) {
                return serializer.Deserialize<HeroJSON[]>(reader);
            }
        }
    }
}