﻿using Newtonsoft.Json;

namespace Backend.JSON.Entities {
    public class NamedEffectJSON {
        [JsonConstructor]
        public NamedEffectJSON(int id, string name, string description) {
            this.id = id;
            this.name = name;
            this.description = description;
        }

        public int id { get; }
        public string name { get; }
        public string description { get; }

        public override bool Equals(object obj) {
            if (obj == null || GetType() != obj.GetType())
                return false;

            var p = (NamedEffectJSON) obj;
            return Equals(id, p.id) && Equals(name, p.name) && Equals(description, p.description);
        }

        public override int GetHashCode() {
            return id.GetHashCode() ^ name.GetHashCode() ^ description.GetHashCode();
        }
    }
}