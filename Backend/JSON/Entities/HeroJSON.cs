﻿using Backend.Operations;

using Newtonsoft.Json;

namespace Backend.JSON.Entities {
    public class HeroJSON {
        [JsonConstructor]
        public HeroJSON(int id, string name, string description, int[] abilityCosts, int[] startingCooldown,
            int[] cooldown, string[] abilityNames) {
            this.id = id;
            this.name = name;
            this.description = description;
            this.abilityCosts = abilityCosts;
            this.startingCooldown = startingCooldown;
            this.cooldown = cooldown;
            this.abilityNames = abilityNames;
        }

        public int id { get; }
        public string name { get; }
        public string description { get; }
        public int[] abilityCosts { get; }
        public int[] startingCooldown { get; }
        public int[] cooldown { get; }
        public string[] abilityNames { get; }

        public override bool Equals(object obj) {
            if (obj == null || GetType() != obj.GetType())
                return false;

            var p = (HeroJSON) obj;
            return Equals(id, p.id) && Equals(name, p.name) && Equals(description, p.description)
                   && ArrayOperations.IntArraysEqual(abilityCosts, p.abilityCosts)
                   && ArrayOperations.IntArraysEqual(startingCooldown, p.startingCooldown)
                   && ArrayOperations.IntArraysEqual(cooldown, p.cooldown)
                   && ArrayOperations.StringArraysEqual(abilityNames, p.abilityNames);
        }

        public override int GetHashCode() {
            return id.GetHashCode() ^ name.GetHashCode() ^ description.GetHashCode() ^ abilityCosts.GetHashCode()
                   ^ startingCooldown.GetHashCode() ^ cooldown.GetHashCode() ^ abilityNames.GetHashCode();
        }
    }
}