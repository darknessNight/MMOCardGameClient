﻿using Newtonsoft.Json;

namespace JSON.Entities.CardReader {
    public class CostJSON {
        [JsonConstructor]
        public CostJSON(int neutral, int raiders, int scanners, int whiteHats, int psychotronnics) {
            this.neutral = neutral;
            this.raiders = raiders;
            this.scanners = scanners;
            this.whiteHats = whiteHats;
            this.psychotronnics = psychotronnics;
        }

        public int neutral { get; }
        public int raiders { get; }
        public int scanners { get; }
        public int whiteHats { get; }
        public int psychotronnics { get; }

        public override bool Equals(object obj) {
            if (obj == null || GetType() != obj.GetType())
                return false;

            var p = (CostJSON) obj;
            return Equals(neutral, p.neutral) && Equals(raiders, p.raiders) && Equals(scanners, p.scanners)
                   && Equals(whiteHats, p.whiteHats) && Equals(psychotronnics, p.psychotronnics);
        }

        public override int GetHashCode() {
            return neutral ^ raiders ^ scanners ^ whiteHats ^ psychotronnics;
        }
    }
}