﻿using JSON.Entities.CardReader;

using Newtonsoft.Json;

namespace Backend.JSON.Entities {
    public class AbilityCostJSON : CostJSON {
        [JsonConstructor]
        public AbilityCostJSON(int neutral, int raiders, int scanners, int whiteHats, int psychotronnics, int overLoad)
            : base(neutral, raiders, scanners, whiteHats, psychotronnics) {
            this.overLoad = overLoad;
        }

        public int overLoad { get; }

        protected bool Equals(AbilityCostJSON other) {
            return base.Equals(other) && overLoad == other.overLoad;
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((AbilityCostJSON) obj);
        }

        public override int GetHashCode() {
            unchecked {
                return (base.GetHashCode() * 397) ^ overLoad;
            }
        }
    }
}