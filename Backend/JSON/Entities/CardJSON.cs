﻿using Backend.JSON.Entities;
using Backend.Operations;

using Newtonsoft.Json;

namespace JSON.Entities.CardReader {
    public class CardJSON {
        [JsonConstructor]
        public CardJSON(int id, StatisticsJSON statistics, int[] abilities, CostJSON cost,
            ActivatedAbilityJSON[] activatedAbilities, string description,
            string name,
            string type, string subtype, [JsonProperty(PropertyName = "class")] string classValue, string subClass,
            string flavor) {
            this.id = id;
            this.statistics = statistics;
            this.abilities = abilities;
            this.cost = cost;
            this.activatedAbilities = activatedAbilities;
            this.description = description;
            this.name = name;
            this.type = type;
            this.subtype = subtype;
            this.classValue = classValue;
            this.subClass = subClass;
            this.flavor = flavor;
        }

        public int id { get; }
        public StatisticsJSON statistics { get; }
        public int[] abilities { get; }
        public CostJSON cost { get; }
        public ActivatedAbilityJSON[] activatedAbilities { get; }
        public string description { get; }
        public string name { get; }
        public string type { get; }
        public string subtype { get; }
        public string classValue { get; }
        public string subClass { get; }
        public string flavor { get; }

        public override bool Equals(object obj) {
            if (obj == null || GetType() != obj.GetType())
                return false;

            var p = (CardJSON) obj;
            return Equals(id, p.id) && Equals(statistics, p.statistics) &&
                   ArrayOperations.IntArraysEqual(abilities, p.abilities)
                   && Equals(cost, p.cost) &&
                   ArrayOperations.ActivatedAbilitiArraysEqual(activatedAbilities, p.activatedAbilities) &&
                   Equals(description, p.description) && Equals(name, p.name)
                   && Equals(type, p.type) && Equals(subtype, p.subtype) && Equals(classValue, p.classValue) &&
                   Equals(subClass, p.subClass)
                   && Equals(flavor, p.flavor);
        }

        public override int GetHashCode() {
            return id ^ statistics.GetHashCode() ^ abilities.GetHashCode() ^ cost.GetHashCode() ^
                   activatedAbilities.GetHashCode()
                   ^ description.GetHashCode() ^ name.GetHashCode() ^ type.GetHashCode() ^ subtype.GetHashCode()
                   ^ classValue.GetHashCode() ^ subClass.GetHashCode() ^ flavor.GetHashCode();
        }
    }
}