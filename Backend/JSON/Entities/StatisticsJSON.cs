﻿using Newtonsoft.Json;

namespace JSON.Entities.CardReader {
    public class StatisticsJSON {
        [JsonConstructor]
        public StatisticsJSON(int attack, int defence) {
            this.attack = attack;
            this.defence = defence;
        }

        public int attack { get; }
        public int defence { get; }

        public override bool Equals(object obj) {
            if (obj == null || GetType() != obj.GetType())
                return false;

            var p = (StatisticsJSON) obj;
            return Equals(attack, p.attack) && Equals(defence, p.defence);
        }

        public override int GetHashCode() {
            return attack ^ defence;
        }
    }
}