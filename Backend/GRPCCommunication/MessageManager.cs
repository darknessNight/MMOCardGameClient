﻿using System;
using System.Collections.Generic;
using System.Threading;

using Backend.GameInformation;
using Backend.Utils;

using Grpc.Core;

using ProtoBuffers;

namespace Backend.GRPCCommunication {
    public class MessageManager {
        private readonly GameState gameState;
        private readonly MessagesHandler messagesHandler;

        private readonly string userName;
        //TODO: tu wsadzić fajne funkcje wysyłające wiadomości do pawłowego serwera zagłady

        private Channel channel;
        private UserServer.UserServerClient client;
        private bool gameInProgress = true;
        private string passwordToken;

        public MessageManager(string userName, MessagesHandler messageHandler, GameState gameState) {
            this.userName = userName;
            messagesHandler = messageHandler;
            messagesHandler.MessagesManager = this;
            this.gameState = gameState;
        }

        public void MakeConnection(string address, string token) {
            MakeClient(address);
            MakeConnectionWithBattleServer(token);
        }

        public void MakeClient(string address) {
            channel = new Channel(address, ChannelCredentials.Insecure);
            client = new UserServer.UserServerClient(channel);
        }

        public void CancelConnection() {
            gameInProgress = false;
            channel.ShutdownAsync();
        }

        public void MakeConnectionWithBattleServer(string token) {
            var response = client.Connect(new Hello {UserName = userName, UserPasswordToken = token});
            messagesHandler.HandleConnectResponse(response);
        }

        public void StartReceiveingThread() {
            new Thread(ReceiveThread).Start();
        }

        public void ReceiveThread() {
            while (gameInProgress)
                try {
                    var hello = GetHello();

                    using (var call = client.returningMessagesStream(hello)) {
                        while (WaitForAnotherMessage(call)) {
                            var currenMessage = call.ResponseStream.Current;
                            messagesHandler.HandleStreamingMessage(currenMessage);
                        }
                    }
                }
                catch (Exception e) {
                    Console.WriteLine(e.Message);
                    Logger.LogException(e);
                    if (gameInProgress) Reconnect();
                }
        }

        private static bool WaitForAnotherMessage(AsyncServerStreamingCall<StreamingMessage> call) {
            var task = call.ResponseStream.MoveNext();
            task.Wait();
            return task.Result;
        }

        public void Reconnect() {
            var response = client.Reconnect(new ReconnectHello {UserName = userName, InnerToken = passwordToken});
            messagesHandler.HandleReconnectResponse(response);
            //StartReceiveingThread();
        }

        public void SetPasswordToken(string passwordToken) {
            this.passwordToken = passwordToken;
        }

        public void SendEndCurrentPhaseMessage() {
            SendEndPhaseMessage(gameState.CurrentPlayerId, gameState.CurrentPhase);
        }

        public void SendEndPhaseMessage(int oldPlayer, GamePhase gamePhase) {
            var response = client.EndPhase(new PhaseChangedRequest {
                Header = GetHello(),
                Message = new PhaseChanged {
                    OldPhase = gamePhase,
                    OldPlayer = oldPlayer,
                    NewPhase = gamePhase,
                    NewPlayer = oldPlayer
                }
            });
            messagesHandler.HandleSimpleResponse(response);
        }

        public Hello GetHello() {
            return new Hello {
                UserName = userName,
                UserPasswordToken = passwordToken
            };
        }

        public void ReplaceCards(List<int> replacedCards) {
            var request = new HandChangedCardsRequest {
                Header = GetHello(),
                Message = new HandChanged {
                    Type = CardChangeType.Remove,
                    Player = (int) gameState.GetMyUserId(),
                    Count = replacedCards.Count
                }
            };

            foreach (var card in replacedCards) {
                gameState.GetPlayer((int) gameState.GetMyUserId()).Hand.Remove(card);
                request.Message.CardInstances.Add(card);
            }

            var response = client.ReplaceCards(request);

            messagesHandler.HandleSimpleResponse(response);
        }

        public void Mulligan() {
            gameState.GetPlayer((int) gameState.GetMyUserId()).Hand.Clear();

            var response = client.Muligan(GetHello());

            messagesHandler.HandleSimpleResponse(response);
        }

        public void PlayCard(int cardInstanceId) {
            var response = client.TargetSelectingTransaction(new TargetSelectingTransactionRequest {
                Header = GetHello(),
                Message = new TargetSelectingTransaction {
                    Type = TargetSelectingTransactionType.StartTransaction,
                    TransactionStartedFor = TargetSelectingTransaction.Types.StartTransactionFor.Play,
                    DestinationTargetId = cardInstanceId,
                    DestinationTargetType = TargetType.Card
                }
            });

            messagesHandler.HandleTargetSelectingTransactionResponse(response, cardInstanceId);
        }

        public void UseCardAbility(int cardInstanceId) {
            var response = client.TargetSelectingTransaction(new TargetSelectingTransactionRequest {
                Header = GetHello(),
                Message = new TargetSelectingTransaction {
                    Type = TargetSelectingTransactionType.StartTransaction,
                    TransactionStartedFor = TargetSelectingTransaction.Types.StartTransactionFor.Ability,
                    DestinationTargetId = cardInstanceId,
                    DestinationTargetType = TargetType.Card
                }
            });

            messagesHandler.HandleTargetSelectingTransactionResponse(response, cardInstanceId);
        }

        public void ChooseAttackersTargets(int cardInstanceId) {
            var response = client.TargetSelectingTransaction(new TargetSelectingTransactionRequest {
                Header = GetHello(),
                Message = new TargetSelectingTransaction {
                    Type = TargetSelectingTransactionType.StartTransaction,
                    TransactionStartedFor = TargetSelectingTransaction.Types.StartTransactionFor.Attack,
                    DestinationTargetId = cardInstanceId,
                    DestinationTargetType = TargetType.Card
                }
            });

            messagesHandler.HandleTargetSelectingTransactionResponse(response, cardInstanceId);
        }

        public void ChooseDefendersTargets(int cardInstanceId) {
            var response = client.TargetSelectingTransaction(new TargetSelectingTransactionRequest {
                Header = GetHello(),
                Message = new TargetSelectingTransaction {
                    Type = TargetSelectingTransactionType.StartTransaction,
                    TransactionStartedFor = TargetSelectingTransaction.Types.StartTransactionFor.Defend,
                    DestinationTargetId = cardInstanceId,
                    DestinationTargetType = TargetType.Card
                }
            });

            messagesHandler.HandleTargetSelectingTransactionResponse(response, cardInstanceId);
        }

        public void SelectOptions(int transactionId, int instanceId, List<Target> selectedOptions) {
            var request = new TargetSelectingTransactionRequest {
                Header = GetHello(),
                Message = new TargetSelectingTransaction {
                    Type = TargetSelectingTransactionType.SelectingOption,
                    TransactionId = transactionId,
                    DestinationTargetId = instanceId,
                    DestinationTargetType = TargetType.Card
                }
            };

            foreach (var target in selectedOptions) {
                var selectedOption = new TargetSelectingOption {
                    TargetType = target.TargetType,
                    Id = target.Id
                };
                request.Message.Options.Add(selectedOption);
            }

            var response = client.TargetSelectingTransaction(request);

            messagesHandler.HandleTargetSelectingTransactionResponse(response, instanceId);
        }

        public void AbortPlay(int transactionId, int instanceId) {
            var response = client.TargetSelectingTransaction(new TargetSelectingTransactionRequest {
                Header = GetHello(),
                Message = new TargetSelectingTransaction {
                    Type = TargetSelectingTransactionType.Abort,
                    TransactionId = transactionId,
                    DestinationTargetId = instanceId,
                    DestinationTargetType = TargetType.Card
                }
            });

            messagesHandler.HandleSimpleResponse(response.Response);
        }

        public void FinishPlay(int transactionId, int instanceId) {
            var response = client.UseCard(new CardUsedRequest {
                Header = GetHello(),
                Message = new CardUsed {
                    Player = (int) gameState.GetMyUserId(),
                    CardInstanceId = instanceId,
                    CardAbilityId = transactionId
                }
            });

            messagesHandler.HandleAddedToStackMessage(response, instanceId);
        }

        public void Surrender() {
            var response = client.Surrender(GetHello());

            messagesHandler.HandleSimpleResponse(response);
        }

        public void AttackDefend(Dictionary<int, int> selectedTargets) {
            var request = new AttackersDefendersSelectedRequest {
                Header = GetHello(),
                Message = new AttackersDefendersSelected()
            };

            foreach (var selectedTarget in selectedTargets) {
                var target = new AttackersDefendersSelected.Types.AttackDefenceTarget {
                    TransactionId = selectedTarget.Value,
                    AttackerId = selectedTarget.Key
                };
                request.Message.Targets.Add(target);
            }

            var response = client.SelectAttackersDefenders(request);

            messagesHandler.HandleSimpleResponse(response);
        }

        public void AcceptTrigger(int transactionId, int instanceId, TriggerType triggerType)
        {
            var response = client.DoTriggerReaction(new TriggerReactionRequest()
            {
                Header = GetHello(),
                Message = new TriggerActivated() {
                    TransactionId = transactionId,
                    CardInstanceId = instanceId,
                    TriggerType = triggerType
                }
            });

            messagesHandler.HandleSimpleResponse(response);
        }
    }
}