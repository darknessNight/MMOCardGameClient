﻿using System;
using System.Collections.Generic;

using Backend.ClientCommunication;
using Backend.Constants;
using Backend.Exceptions;
using Backend.GameInformation;
using Backend.GameInformation.Enums;
using Backend.Utils;

using Google.Protobuf.Collections;

using JSON.Entities.CardReader;
using JSON.Readers.CardReader;

using ProtoBuffers;

namespace Backend.GRPCCommunication {
    public class MessagesHandler {
        private readonly IClientCallback callback;
        private readonly GameState gameState;
        private readonly CardJSON[] loadedCards;

        public MessagesHandler(IClientCallback callback, GameState gameState) {
            this.callback = callback;
            this.gameState = gameState;
            loadedCards = CardJSONReader.GetCards(Directories.CardJSONLocation);
        }

        public MessageManager MessagesManager { get; set; }

        public void HandleConnectResponse(ConnectResponse message) {
            HandleResponseCode(message.Response.ReponseCode);

            MessagesManager.SetPasswordToken(message.Response.ConnectionToken);

            foreach (var player in message.GameState.Players) {
                var library = new Dictionary<int, Card>();
                gameState.AddPlayer(player.Name, player.Id, player.HeroId, player.Defence, player.CardsInLibraryCount);
            }

            gameState.FindMyPlayerId();
            callback.ConnectionWasMade(gameState);
        }

        public void HandleReconnectResponse(ConnectResponse message) {
            HandleResponseCode(message.Response.ReponseCode);
            //TODO: jak się zrąbało połączenie to trzeba przywrócić grę, na razie nie ma czego przywracać
        }

        public void HandleStreamingMessage(StreamingMessage message) {
            try {
                switch (message.Type - GrpcConstants.StreamMessagesOffset) {
                    case (int) StreamingMessage.MessageOneofCase.PhaseChanged:
                        HandlePhaseChanged(message.PhaseChanged);
                        break;
                    case (int) StreamingMessage.MessageOneofCase.HandChanged:
                        HandleHandChange(message.HandChanged);
                        break;
                    case (int) StreamingMessage.MessageOneofCase.CardRevealed:
                        HandleCardRevealed(message.CardRevealed);
                        break;
                    case (int) StreamingMessage.MessageOneofCase.CardMoved:
                        HandleCardMove(message.CardMoved);
                        break;
                    case (int) StreamingMessage.MessageOneofCase.CardUsed:
                        HandleCardUsed(message.CardUsed);
                        break;
                    case (int) StreamingMessage.MessageOneofCase.CardStatisticsChanged:
                        HandleCardStatisticsChanged(message.CardStatisticsChanged);
                        break;
                    case (int) StreamingMessage.MessageOneofCase.PlayerDefenceChanged:
                        HandlePlayerDefenceChanged(message.PlayerDefenceChanged);
                        break;
                    case (int) StreamingMessage.MessageOneofCase.PlayerGameEnd:
                        HandlePlayerGameEnd(message.PlayerGameEnd);
                        break;
                    case (int) StreamingMessage.MessageOneofCase.AttackersDefendersSelected:
                        HandleAttackersDefendersSelected(message.AttackersDefendersSelected);
                        break;
                    case (int) StreamingMessage.MessageOneofCase.AttackExecution:
                        HandleAttackExecution(message.AttackExecution);
                        break;
                    case (int) StreamingMessage.MessageOneofCase.CardCreated:
                        HandleCardCreated(message.CardCreated);
                        break;
                    case (int) StreamingMessage.MessageOneofCase.StackActionCompleted:
                        HandleStackActionCompleted(message.StackActionCompleted);
                        break;
                    case (int) StreamingMessage.MessageOneofCase.PlayerLinkPointsUpdated:
                        HandlePlayerLinkPointsUpdated(message.PlayerLinkPointsUpdated);
                        break;
                    case (int) StreamingMessage.MessageOneofCase.TriggerActivated:
                        HandleTriggerActivated(message.TriggerActivated);
                        break;
                    case (int) StreamingMessage.MessageOneofCase.TargetSelectingTransaction:
                        HandleTargetSelectingTransaction(message.TargetSelectingTransaction);
                        break;
                    default:
                        Console.WriteLine(message.ToString());
                        break;
                }
            }
            catch (Exception e) {
                Logger.LogException(e);
            }
        }

        public void HandlePhaseChanged(PhaseChanged phaseChanged) {
            gameState.CurrentPhase = phaseChanged.NewPhase;
            gameState.CurrentPlayerId = phaseChanged.NewPlayer;

            switch (phaseChanged.NewPhase) {
                case GamePhase.ReactionPhase:
                    if (phaseChanged.NewPlayer == gameState.GetMyUserId())
                        callback.ReactToStackAction();
                    else
                        callback.WaitForReactionToStackAction(phaseChanged.NewPlayer);
                    break;
                default:
                    callback.PhaseChanged(phaseChanged.OldPhase, phaseChanged.OldPlayer, phaseChanged.NewPhase,
                        phaseChanged.NewPlayer);
                    break;
            }
        }

        public void HandleHandChange(HandChanged handChanged) {
            if (handChanged.Player == gameState.GetMyUserId()) {
                if (handChanged.Type == CardChangeType.Draw) { }
                else if (handChanged.Type == CardChangeType.Remove) {
                    foreach (var card in handChanged.CardInstances)
                        gameState.GetPlayer(handChanged.Player).Hand.Remove(card);
                }
                else {
                    throw new UndefinedCardChangeType();
                }
            }

            callback.HandChanged(handChanged.Player);
        }

        public void HandleCardRevealed(CardRevealed cardRevealed) {
            var container = GetCardContainerOfPlayer(cardRevealed.Owner, cardRevealed.Position);
            Card card;

            if (container.ContainsKey(cardRevealed.InstanceId)) {
                card = container[cardRevealed.InstanceId];
            }
            else {
                card = new Card(cardRevealed.InstanceId);
                container.Add(cardRevealed.InstanceId, card);
            }

            SetCardPropertiesFromCardRevealedAndJSON(card, cardRevealed);
            callback.CardRevealed(card, cardRevealed.Owner, cardRevealed.Position);
        }

        public void HandleCardMove(CardMoved cardMoved) {
            ChangeCardPosition(cardMoved);
            ChangeCardsInLibraryCountIfNeeded(cardMoved);
            ChangeCardsInHandCountIfNeeded(cardMoved);
        }

        private void ChangeCardPosition(CardMoved cardMoved) {
            var previousContainer = GetCardContainerOfPlayer(cardMoved.OldOwner, cardMoved.OldPosition);
            var newContainer = GetCardContainerOfPlayer(cardMoved.NewOwner, cardMoved.NewPosition);

            Card card;
            if (!newContainer.ContainsKey(cardMoved.CardInstanceId)) {
                if (previousContainer.ContainsKey(cardMoved.CardInstanceId)) {
                    card = previousContainer[cardMoved.CardInstanceId];
                }
                else {
                    card = new Card(cardMoved.CardInstanceId);
                    previousContainer.Add(cardMoved.CardInstanceId, card);
                }

                newContainer.Add(card.InstanceId, card);
                previousContainer.Remove(card.InstanceId);
            }
            else {
                card = newContainer[cardMoved.CardInstanceId];
            }

            callback.CardMoved(card, cardMoved.OldOwner, cardMoved.OldPosition, cardMoved.NewOwner,
                cardMoved.NewPosition);
        }

        private void ChangeCardsInLibraryCountIfNeeded(CardMoved cardMoved) {
            if (cardMoved.OldPosition == CardPosition.Library)
                gameState.GetPlayer(cardMoved.OldOwner).CardsInLibraryCount--;

            if (cardMoved.NewPosition == CardPosition.Library)
                gameState.GetPlayer(cardMoved.NewOwner).CardsInLibraryCount++;
        }

        private void ChangeCardsInHandCountIfNeeded(CardMoved cardMoved) {
            if (cardMoved.OldPosition == CardPosition.Hand) gameState.GetPlayer(cardMoved.OldOwner).CardsInHandCount--;

            if (cardMoved.NewPosition == CardPosition.Hand) gameState.GetPlayer(cardMoved.NewOwner).CardsInHandCount++;
        }

        public void HandleCardUsed(CardUsed cardUsed) {
            var addedElement = new StackElement(cardUsed.CardInstanceId, cardUsed.CardAbilityId,
                GetListOfTargetListFromTransactionSelects(cardUsed.TransactionSelects), null);
            gameState.Stack.Add(cardUsed.StackId, addedElement);
            callback.OtherPlayerElementAddedToStack(cardUsed.StackId, addedElement, cardUsed.Player,
                cardUsed.CardCosts);
        }

        public void HandleCardStatisticsChanged(CardStatisticsChanged cardStatisticsChanged) {
            var container = GetCardContainerOfPlayer(cardStatisticsChanged.Owner, cardStatisticsChanged.Position);
            var card = container[cardStatisticsChanged.CardInstanceId];
            SetCardPropertiesFromCardStatisticsChanged(card, cardStatisticsChanged);
            callback.CardStatisticsChanged(card);
        }

        public void HandlePlayerDefenceChanged(PlayerDefenceChanged playerDefenceChanged) {
            gameState.GetPlayer(playerDefenceChanged.Player).Defence = playerDefenceChanged.NewDefenceValue;
            callback.PlayerDefenceChanged(playerDefenceChanged.Player, playerDefenceChanged.NewDefenceValue);
        }

        public void HandlePlayerGameEnd(PlayerGameEnd playerGameEnd) {
            switch (playerGameEnd.Result) {
                case GameResult.Defeated:
                    HandleLostGame(playerGameEnd);
                    break;
                case GameResult.Won:
                    HandleWonGame(playerGameEnd);
                    break;
                default:
                    throw new UnknownGameEndResultException();
            }
        }

        public void HandleAttackersDefendersSelected(AttackersDefendersSelected attackersDefendersSelected) {
            var attackersDefenders = new Dictionary<int, List<TargetList>>();

            foreach (var attackerDefender in attackersDefendersSelected.Targets)
                attackersDefenders.Add(attackerDefender.AttackerId,
                    GetListOfTargetListFromTransactionSelects(attackerDefender.Transaction));

            callback.AttackerDefenderSelected(attackersDefenders);
        }

        public void HandleAttackExecution(AttackExecution attackExecution) {
            callback.AttackExecuted(attackExecution.AttackerId, attackExecution.DefenderId,
                attackExecution.DefenderType);
        }

        public void HandleCardCreated(CardCreated cardCreated) {
            if (cardCreated.CalcUnit)
                throw new NotImplementedException("Add creating calc units if hero abilities got support");

            var card = new Card(cardCreated.InstanceId);
            card.CardId = cardCreated.CardId;
            var container = GetCardContainerOfPlayer(cardCreated.Owner, cardCreated.CardPosition);
            container.Add(cardCreated.InstanceId, card);
            callback.CardCreated(card, cardCreated.Owner, cardCreated.CardPosition);
        }

        public void HandleStackActionCompleted(StackActionCompleted stackActionCompleted) {
            callback.StackActionCompleted(stackActionCompleted.StackId, gameState.Stack[stackActionCompleted.StackId]);
            gameState.Stack.Remove(stackActionCompleted.StackId);
            foreach (var deletedStackAction in stackActionCompleted.DeletedStackActions) {
                callback.StackActionRemoved(deletedStackAction, gameState.Stack[deletedStackAction]);
                gameState.Stack.Remove(deletedStackAction);
            }
        }

        public void HandlePlayerLinkPointsUpdated(PlayerLinkPointsUpdated linkPointsUpdated) {
            var player = gameState.GetPlayer(linkPointsUpdated.Player);
            player.LinkPoints.Psychos = linkPointsUpdated.Points.Psychos;
            player.LinkPoints.Raiders = linkPointsUpdated.Points.Raiders;
            player.LinkPoints.Scanners = linkPointsUpdated.Points.Scanners;
            player.LinkPoints.WhiteHats = linkPointsUpdated.Points.WhiteHats;
            player.LinkPoints.Neutral = linkPointsUpdated.Points.Neutral;
            callback.LinkPointsUpdated(player.Id, player.LinkPoints);
        }

        public void HandleTriggerActivated(TriggerActivated triggerActivated) {
            if (triggerActivated.StackId == -1) {
                callback.TriggerWithoutStackActivated(triggerActivated.CardInstanceId, triggerActivated.TriggerType);
            }
            else {
                var addedElement = new StackElement(triggerActivated.CardInstanceId, null,
                    GetListOfTargetListFromTransactionSelects(triggerActivated.Transaction), triggerActivated.TriggerType);
                gameState.Stack.Add(triggerActivated.StackId, addedElement);
                callback.TriggerAddedToStack(triggerActivated.StackId, addedElement);
            }
            
        }

        public void HandleTargetSelectingTransaction(TargetSelectingTransaction targetSelectingTransaction) {
            ProcessTargetSelectingTransaction(targetSelectingTransaction,
                targetSelectingTransaction.DestinationTargetId);
        }

        private void HandleLostGame(PlayerGameEnd playerGameEnd) {
            if (playerGameEnd.Player == gameState.GetMyUserId()) {
                MessagesManager.CancelConnection();
                callback.YouLost();
            }
            else {
                callback.PlayerLost(playerGameEnd.Player);
            }
        }

        private void HandleWonGame(PlayerGameEnd playerGameEnd) {
            if (playerGameEnd.Player == gameState.GetMyUserId()) {
                MessagesManager.CancelConnection();
                callback.YouWon();
            }
            else {
                callback.PlayerWon(playerGameEnd.Player);
            }
        }

        private Dictionary<int, Card> GetCardContainerOfPlayer(int playerId, CardPosition cardPosition) {
            var player = gameState.GetPlayer(playerId);
            Dictionary<int, Card> container;

            switch (cardPosition) {
                case CardPosition.Library:
                    container = player.Library;
                    break;
                case CardPosition.BattleField:
                    container = player.BattleField;
                    break;
                case CardPosition.Graveyard:
                    container = player.Cemetery;
                    break;
                case CardPosition.Hand:
                    container = player.Hand;
                    break;
                case CardPosition.Void:
                    container = player.RemovedCards;
                    break;
                default:
                    throw new UndefinedCardPositionException();
            }

            return container;
        }

        private CardJSON GetCardFromLoadedCards(int cardId) {
            foreach (var card in loadedCards)
                if (card.id == cardId)
                    return card;

            throw new NoClassIdInLoadedCardsJSONException();
        }

        private void SetCardPropertiesFromCardRevealedAndJSON(Card card, CardRevealed cardRevealed) {
            card.Attack = cardRevealed.Attack;
            card.Defence = cardRevealed.Defence;
            card.PositionIndex = cardRevealed.PositionIndex;
            card.CardCosts.Neutral = cardRevealed.CardCosts.Neutral;
            card.CardCosts.Psychos = cardRevealed.CardCosts.Psychos;
            card.CardCosts.Raiders = cardRevealed.CardCosts.Raiders;
            card.CardCosts.Scanners = cardRevealed.CardCosts.Scanners;
            card.CardCosts.WhiteHats = cardRevealed.CardCosts.WhiteHats;
            card.CardId = cardRevealed.CardId;
            card.OverloadedPoints = cardRevealed.OverloadedPoints;

            var cardJSON = GetCardFromLoadedCards((int) card.CardId);

            if (cardJSON.activatedAbilities != null) {
                card.ActivatedAbilities.Clear();
                foreach (var ability in cardJSON.activatedAbilities) {
                    var activatedAbilityCosts = new ActivatedAbilityCosts(ability.cost.neutral,
                        ability.cost.psychotronnics,
                        ability.cost.raiders, ability.cost.scanners, ability.cost.whiteHats, ability.cost.overLoad);
                    card.ActivatedAbilities.Add(ability.id,
                        new ActivatedAbility(ability.id, ability.description, activatedAbilityCosts));
                }
            }

            card.ClassValue = cardJSON.classValue;
            card.SubClass = cardJSON.subClass;
            try {
                if (cardJSON.subtype != null)
                    card.Subtype = (CardSubType) Enum.Parse(typeof(CardSubType), cardJSON.subtype);
                card.Type = (CardType) Enum.Parse(typeof(CardType), cardJSON.type);
            }
            catch (ArgumentException ex) {
                Logger.LogException(ex);
                throw new WrongTypeNameException();
            }
        }

        private void
            SetCardPropertiesFromCardStatisticsChanged(Card card, CardStatisticsChanged cardStatisticsChanged) {
            card.CardCosts.Neutral = cardStatisticsChanged.CardCosts.Neutral;
            card.CardCosts.Psychos = cardStatisticsChanged.CardCosts.Psychos;
            card.CardCosts.Raiders = cardStatisticsChanged.CardCosts.Raiders;
            card.CardCosts.Scanners = cardStatisticsChanged.CardCosts.Scanners;
            card.CardCosts.WhiteHats = cardStatisticsChanged.CardCosts.WhiteHats;
            card.Attack = cardStatisticsChanged.AttackValue;
            card.Defence = cardStatisticsChanged.DefenceValue;
            card.ClassValue = cardStatisticsChanged.ClassName;
            card.SubClass = cardStatisticsChanged.SubClassName;
            card.OverloadedPoints = cardStatisticsChanged.OverloadPoints;

            //TODO add changing activated abilities if time allows
        }

        private List<TargetList> GetListOfTargetListFromTransactionSelects(
            RepeatedField<TargetSelectingTransaction> transactionSelects) {
            var targetLists = new List<TargetList>();

            foreach (var targetSelectingTransaction in transactionSelects)
                targetLists.Add(GetTargetListFromTargetSelectingTransaction(targetSelectingTransaction));

            return targetLists;
        }

        private TargetList GetTargetListFromTargetSelectingTransaction(
            TargetSelectingTransaction targetSelectingTransaction) {
            var targetList = new TargetList();
            targetList.Description = targetSelectingTransaction.Description;
            targetList.OptionsType = targetSelectingTransaction.OptionsType;
            foreach (var targetSelectingOption in targetSelectingTransaction.Options) {
                var target = new Target();
                target.TargetType = targetSelectingOption.TargetType;
                target.Description = targetSelectingOption.Description;
                target.Id = targetSelectingOption.Id;
                targetList.Targets.Add(target);
            }

            return targetList;
        }

        public void HandleSimpleResponse(SimpleResponse message) {
            HandleResponseCode(message.ResponseCode);
            //TODO:obsługa odpowiedzi
        }

        public void HandleHandChangeCardsResponse(HandChangedCardsResponse message) {
            HandleResponseCode(message.SimpleResponse.ResponseCode);

            if (message.Message.Type == CardChangeType.Draw)
                foreach (var card in message.Message.CardInstances)
                    gameState.GetPlayer(message.Message.Player).Hand.Add(card, new Card(card));
            else if (message.Message.Type == CardChangeType.Remove)
                foreach (var card in message.Message.CardInstances)
                    gameState.GetPlayer(message.Message.Player).Hand.Remove(card);
            else
                throw new UndefinedCardChangeType();

            callback.HandChanged(message.Message.Player);
        }

        public void HandleAddedToStackMessage(AddedToStackResponse message, int cardInstanceId) {
            if (message.CardUsed.StackId != -1) {
                HandleResponseCode(message.SimpleResponse.ResponseCode);
                var addedElement = new StackElement(cardInstanceId, message.CardUsed.CardAbilityId,
                    GetListOfTargetListFromTransactionSelects(message.CardUsed.TransactionSelects), null);
                gameState.Stack.Add(message.CardUsed.StackId, addedElement);
                callback.YourElementAddedToStack(message.CardUsed.StackId, addedElement);
            }
        }

        public void HandleTargetSelectingTransactionResponse(TargetSelectingTransactionResponse message,
            int instanceId) {
            HandleResponseCode(message.Response.ResponseCode);
            ProcessTargetSelectingTransaction(message.Message, instanceId);
        }

        private void ProcessTargetSelectingTransaction(TargetSelectingTransaction targetSelectingTransaction,
            int instanceId) {
            if (targetSelectingTransaction.Type == TargetSelectingTransactionType.AvailableOptions) {
                callback.PickOptions(targetSelectingTransaction.TransactionId,
                    GetTargetListFromTargetSelectingTransaction(targetSelectingTransaction), instanceId);
            }
            else if (targetSelectingTransaction.Type == TargetSelectingTransactionType.Result) {
                callback.YouFinishedChoosing(targetSelectingTransaction.TransactionId, instanceId);
                if (targetSelectingTransaction.Options != null)
                    callback.GotTargetListInResultOfTransaction(targetSelectingTransaction.TransactionId, instanceId,
                        GetTargetListFromTargetSelectingTransaction(targetSelectingTransaction));
            }
            else {
                throw new WrongTargetSelectingTransactionType();
            }
        }

        public void HandleResponseCode(int responseCode) {
            try {
                switch (responseCode) {
                    case 0:
                    case 200:
                    case 201:
                        return;
                    case 403:
                        throw new AccessForbidenException();
                    case 500:
                        throw new InternalErrorException();
                    case 400:
                        throw new BadRequestException();
                    case 404:
                        throw new NotFoundException();
                    case 406:
                        throw new NotAcceptableException();
                    case 601:
                        throw new InconsistentClientStateException();
                    default:
                        throw new UnknownResponseCodeException();
                }
            }
            catch (Exception ex) {
                callback.ServerError(ex);
                throw ex;
            }
        }
    }
}