﻿using System.Collections.Generic;

using Backend.GameInformation;
using ProtoBuffers;

namespace Backend.Main {
    public interface IMainController {
        void MakeConnection(string address, string token);
        void NotifyThatPreparedForGame();
        void AcceptHand();
        void ReplaceCards(List<int> replacedCards);
        void Mulligan();
        void EndPhase();
        void PlayCard(int cardInstanceId);
        void UseCardAbility(int cardInstanceId);
        void SelectOptions(int transactionId, int instanceId, List<Target> selectedOptions);
        void FinishPlay(int transactionId, int instanceId);
        void AbortPlay(int transactionId, int instanceId);
        void PassStackReaction();
        void Surrender();
        void ChooseAttackersTargets(int cardInstanceId);
        void ChooseDefendersTargets(int cardInstanceId);
        void AttackDefend(Dictionary<int, int> selectedTargets);
        void CancelConnection();
        void AcceptTrigger(int transactionId, int instanceId, TriggerType triggerType);
    }
}